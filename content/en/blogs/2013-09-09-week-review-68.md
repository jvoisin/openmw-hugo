{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-09-09T01:20:06+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-68/"
}
Hello once, again!

OpenMW is progressing as usual, and Zini&#8217;s hardware is operational again! This leads us to…

**OpenMW 0.26.0!**

This is going to be a pretty good version, with combat, werewolfs and other goodies. WeirdSexy already made a video presentation and it&#8217;s epic.

**Zini** already posted the <a href="https://forum.openmw.org/viewtopic.php?f=20&t=1806" title="It's here!" target="_blank">roadmap</a> for version 0.27.0 and it looks quite promising if you ask me!

Other interesting things:

  * **Zini** finished with OpenCS fiter feature for now. Certainly a good thing. We also have a startup window for OpenCS, containing some buttons.
  * **Chris** is doing things with physics code that only he can understand. It looks like a major reimplementation though.