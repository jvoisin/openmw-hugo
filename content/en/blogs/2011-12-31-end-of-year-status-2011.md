{
   "author": "Zini",
   "categories": [
      "Week's review",
      "Year's review"
   ],
   "date": "2011-12-31T18:38:49+00:00",
   "tags": [
      "Zini"
   ],
   "title": "End of Year Status 2011",
   "type": "post",
   "url": "/2011/end-of-year-status-2011/"
}
The first year of OpenMW development under new management is over and for the most part we have done good. Last year around this time OpenMW still looked dead. Now there is no doubt anymore that OpenMW 1.0 will see the light of day.

We had three releases which isn&#8217;t so bad, although slightly less than I had hoped for. We had a lot of problems: forced website migration, partial loss of the old wiki, some important developers gone inactive, people involved in the release process quitting at the most inappropriate time and the mwrender refactoring task which went through the hands of three developers and which will still have an aftermath in form of an to be investigated slowdown and a potential regression. But overall we did good.

We didn&#8217;t produce a huge load of user visible improvements in 2011. Most of our work was either of peripheral nature or focused on the backend or code maintenance. I suspect that this factor contributed greatly to the decreased public attention we received, especially in the second half of 2011.  
But the upside of this is that we are in a very good position now. We have a lot of groundwork covered. The only major issue we need to get out of the way is the configuration improvement and the rest of the packaging automation.  
Now we can implement several interesting features that wouldn&#8217;t have been possible a year ago. Namely I expect for the upcoming releases (after 0.12):

  * plugin and multiple master loading
  * implementation of most of the missing graphical features
  * completing our plan to make the tutorial part at the beginning of Morrowind fully playable.

Also it looks like we will see the beginning of the Editor.

Once we have 0.12 out of the door a much brighter future is ahead of us.

We did good in 2011. Let&#8217;s do better in 2012.