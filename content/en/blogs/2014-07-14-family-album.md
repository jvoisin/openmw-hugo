{
   "author": "Nekochan",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-07-14T18:57:46+00:00",
   "title": "Family Album",
   "type": "post",
   "url": "/2014/family-album/"
}
Well, as someone so eloquently said in the last comment section&#8230;

<img class="alignleft" src="https://i0.wp.com/i1.kym-cdn.com/photos/images/original/000/349/104/00d.jpg" alt="Point taken." data-recalc-dims="1" /> 

I guess I should post some screens from the latest version of OpenCS then.

[This][1] is what it looks like when you open the panel for a container. You can see that inside the penal another panel is nested which shows the contents of the container. You can also [open two container panels][2] side by side. [Or even three][3], all in one window.

And [this is rendering in action][4]. Just like the containers, OpenCS is ready to [render while you render][5].

We&#8217;ve been talking to modders and one of the major complaints they have about Morrowind&#8217;s Construction Set is that they are limited to only one rendering window. That problem is now solved in OpenCS. In addition to having as many _panels_ open as you want, you can also open as many _windows_ as you want which is a fantastic solution for those us who have multiple monitors. (As you can see by the stone age resolution of those screenshots, they weren&#8217;t exactly taken with the latest monitor hooked up and even then the experience isn&#8217;t that bad.)

Hopefully, these shots are enough. If not, don&#8217;t worry. You&#8217;ll be able to see the real thing for yourself soon enough.

 [1]: https://openmw.org/wp-content/uploads/2014/07/nestedtable.png
 [2]: https://openmw.org/wp-content/uploads/2014/07/nestedtable2.png
 [3]: https://openmw.org/wp-content/uploads/2014/07/nestedtable3.png
 [4]: https://openmw.org/wp-content/uploads/2014/07/rendering1.png
 [5]: https://openmw.org/wp-content/uploads/2014/07/rendering2.png "renrenderdering"