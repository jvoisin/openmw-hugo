{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2018-02-07T20:03:24+00:00",
   "title": "OpenMW-launcher just got an advanced settings menu",
   "type": "post",
   "url": "/2018/openmw-launcher-advanced-settings-menu/"
}
After the new year, development has been quiet and there hasn&#8217;t been much of interest going on in OpenMW&#8230; or so I thought until I checked out the latest feature on the bugtracker. We now have a settings menu in our launcher that allows easy access to various optional features that were previously hidden in some text file that most people didn&#8217;t even know existed. Thanks to Thunderforge, enabling these options will soon be a simple matter of checking a box. It&#8217;s not quite clear if this feature is to stay in its current form, however, as we may revise it to be part of ingame options after getting OpenMW to its post 1.0 version state.

[<img loading="lazy" class="wp-image-5083 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2018/02/35781519-c3c6c3ae-09b0-11e8-9ece-636d185ebf59-300x252.png?resize=713%2C599&#038;ssl=1" alt="" width="713" height="599" data-recalc-dims="1" />][1]

In other news, AnyOldName3&#8217;s work on bringing shadows back to the OpenMW engine is still ongoing and can be followed [here on github][2]. Those who have the know-how to build OpenMW&#8217;s shadow branch have reported that the results are night and day in terms of visual stability compared to earlier iterations.



##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=4942" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2018/02/35781519-c3c6c3ae-09b0-11e8-9ece-636d185ebf59.png?ssl=1
 [2]: https://github.com/OpenMW/openmw/pull/1547