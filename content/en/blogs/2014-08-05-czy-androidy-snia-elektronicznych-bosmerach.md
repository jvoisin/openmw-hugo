{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-08-05T14:47:50+00:00",
   "title": "Czy Androidy śnią o elektronicznych Bosmerach?",
   "type": "post",
   "url": "/2014/czy-androidy-snia-elektronicznych-bosmerach/"
}
Jak wspomniano w jednym z wcześniejszych postów ([link][1]), quest główny jest już możliwy do ukończenia w OpenMW. Wciąż jest jednak wiele do zrobienia. Scrawl przypomniał sobie Gwiezdne Wojny i stwierdził, że „ma co do tego złe przeczucia”. Okazało się jednak, że wszelkie napotkane błędy udało się łatwo usunąć&#8230; przynajmniej te dotychczasowe. Jeden z naszych aktywnych testerów zauważył, że niektóre stworzenia „w dziczy” pojawiają się zbyt licznie ([zrzut ekranu][2]).

Jedno z częstych pytań zadawanych w odniesieniu do OpenMW brzmi: „Co ma OpenMW, czego nie ma Morrowind?”. Można w odpowiedzi zacząć gadkę o tym, że OpenMW ma otwarte źródła, i że dzęki temu jest bardziej modyfikowalne, i tak dalej. Ale jest jeszcze coś innego: co z platformami innymi niż Windows? Nasz zespół pracuje oczywiście nad wsparciem dla Linuksa i OSX-a, ale nie tylko! Jeden z członków naszej społeczności, Sandstranger, zaczął pracować nad pewnym nieoficjalnym projektem&#8230; OpenMW na Androida! Obecna wersja już działa, konieczne są jednak pewne poprawki ([wątek na forum][3]). Powodzenia Sandstranger! Jesteśmy pewni, że wyjdzie znakomicie!

Morrowind jest pełen skryptów, niestety nie zawsze są one napisane tak dobrze, jak powinny. Zini podał przykład (jeśli ktoś kiedykolwiek używał konsoli w Morrowindzie, to powinien wiedzieć o co chodzi). Gracz może dodać sobie zaklęcie Poparzenie komendą: _player -> addspell &#8220;fire_bite&#8221;_. Czasami można się spotkać z interesującymi wariacjami na temat tej komendy, np.: _player -> addspell &#8220;fire_bite&#8221;, 654._ Co oznacza to 654? Nikt tego nie wie. Morrowind po prostu zignoruje ten zbędny argument. Jednakże OpenMW odczytuje go i zgłasza, że ze skryptem jest coś nie tak. Jeśli ktokolwiek z Was ma pomysł co ten dodatkowy argument może oznaczać, to prosimy o dołączenie do [dyskusji][4]. Na razie te „przybłędy” są obchodzone, ale nie jest to eleganckie rozwiązanie. Jeśli macie jakiś pomysł, prosimy, dajcie znać!

Jeszcze kilka ciekawostek na koniec: mamy nowe funkcje! Jedną z nich jest suwak trudności. Jeśli gra była zbyt łatwa lub zbyt trudna, to jest już na to remedium. Ponadto poprawiono mechanizm witania się NPC-ów. Stwierdzenie Fargotha, że bylibyście wspaniałymi przyjaciółmi musi być bardzo żenujące, gdy wszyscy wokół gapią się potem na ciebie bez zmrużenia oka. Tak, mechanizm mrugania też dodano ;)

To by było na tyle na ten tydzień. Do następnego razu!

Chcesz zostawić komentarz? Zapraszamy [tutaj][5]!

 [1]: https://openmw.org/2014/3679/ "Junk data"
 [2]: http://forum.openmw.org/download/file.php?id=328&mode=view
 [3]: https://forum.openmw.org/viewtopic.php?p=25795#p25795
 [4]: https://forum.openmw.org/viewtopic.php?f=6&t=2262
 [5]: https://forum.openmw.org/viewtopic.php?f=38&t=2288