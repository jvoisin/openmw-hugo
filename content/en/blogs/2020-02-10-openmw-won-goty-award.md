{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2020-02-10T10:19:19+00:00",
   "title": "OpenMW won GOTY award!",
   "type": "post",
   "url": "/2020/openmw-won-goty-award/"
}
Hello everyone

We are very proud to announce that the website <u><font color="#000117"><a aria-label=" (opens in a new tab)" rel="noreferrer noopener" href="http://www.gamingonlinux.com" target="_blank">GamingOnLinux</a></font></u> has revealed OpenMW as the winner of their [Game of the Year awards][1] in the _Favourite FOSS game engine reimplementation_ category.

Competing against other respected and important projects like [ScummVM][2] and [OpenRA][3], this means a lot, and we are very honoured. A BIG thank you to everyone who voted for OpenMW!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=6693" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://www.gamingonlinux.com/goty.php?module=category&category_id=7
 [2]: https://www.scummvm.org/
 [3]: https://www.openra.net/