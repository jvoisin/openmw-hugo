{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-03-31T19:07:03+00:00",
   "title": "Zapomniane wydania: 0.38.0",
   "type": "post",
   "url": "/2016/zapomniane-wydania-0-38-0/"
}
A oto ostatnia aktualizacja, którą jesteśmy Wam winni. Poniżej znajdziecie kolejne dwa filmy, pierwszy dotyczy zopenMW, natomiast drugi poświęcony jest OpenCS.





A teraz, kiedy mamy to już z głowy, możemy przejść do 0.39.0. Następna wersja wciąż się gotuje, ale jeśli chcecie ją sprawdzić, zawsze możecie przetestować jeden z nocnych buildów dla [Windowsa][1] i&nbsp;[Linuksa][2]. Zawierają już one wsparcie dla eleganckich, [nowiutkich shaderów][3]!

##### Zapraszamy do komentowania [tutaj][4].

 [1]: https://forum.openmw.org/viewtopic.php?f=20&t=1808
 [2]: https://launchpad.net/~openmw/+archive/ubuntu/openmw-daily
 [3]: https://wiki.openmw.org/index.php?title=TextureModding
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=3445