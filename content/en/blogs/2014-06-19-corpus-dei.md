{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-06-19T08:53:58+00:00",
   "title": "Corpus Dei",
   "type": "post",
   "url": "/2014/corpus-dei/"
}
Korci mnie by ograniczyć się do stwierdzenia, że w OpenMW bez zmian, ale to nie byłaby prawda. Zmiany są ogromne, jednakże ich natura nie odbiega od tych z poprzednich tygodni. Ot, kolejna góra błędów naprawiona.

Warto nadmienić, że jest to góra wyjątkowo znacznych rozmiarów. Nie jakiś tam Kasprowy Wierch czy Babia Góra, a przynajmniej K2. O ile, oczywiście, przyjmiemy tą analogię za na tyle użyteczną by ją nadal ciągnąć. Intuicja oraz doświadczenie podpowiada, że lepiej ją porzucić i przejść do faktów. 

Liczba rozwiązanych błędów sięga setek. Dosłownie. Możecie sprawdzić sobie na bugtrackerze jeśli (mi?!) nie wierzycie. Główna w tym zasługa Scrawla, ale również inni programiści wydatnie przyczynili się do tego wyniku. Nie można zapomnieć choćby o mrCheko.

Oprócz tego: obawiam się, że niezbyt wiele. Aktualnie pracuję nad obsługą zagnieżdżonych danych w OpenCS (przykładowo, lista przedmiotów będących w posiadaniu postaci niezależnej) co jest w sumie dość ciekawe, ale aktualnie wciąż niedokończone.