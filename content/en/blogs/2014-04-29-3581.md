{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-04-29T13:48:22+00:00",
   "title": "Wiosna, wiosna wszędzie.",
   "type": "post",
   "url": "/2014/3581/"
}
Mnie męczy grypa, niektórzy spośród nas; alergicy; klną na pyłki – ale przecież nie powstrzyma nas to: kochamy wiosnę mimo wszystko! Od razu jednak uspokajam: żadne sezonowe sentymenty nie mogą powstrzymać rasowych hackerów przed przesiadywaniem długich godzin nad kodem. Nasz zespół jest aktywny niczym rój pszczół, a efekty widoczne są gołym okiem.

Zacznijmy może od naprawionych błędów: są one liczne. Chciałbym na tym poprzestać, ale niestety czytelnicy zapewne zaczęli by wietrzyć podstęp, więc muszę wymienić przynajmniej część z naszych dokonań.

Przykładowo scrawl:

  * Możecie już śmiało rabować zabitych przeciwników. Nie jest to traktowane jako kradzież.
  * Czas jaki zajmuje utonięcie nie jest zakodowany na stałe w silniku gry.
  * Liczne, inne poprawki.
Ponadto:

  * Nie można już w trakcie walki naprawiać przedmiotów, regenerować ładunków w przedmiotach magicznych oraz tworzyć eliksirów.
  * Liczne, inne poprawki.
Nowych funkcji nie ma zbyt wiele, ale do tego była już pora się przyzwyczaić. OpenMW aktualnie wymaga nade wszystko poprawek w tym, co zostało już wcześniej zaimplementowane. Mimo to, do kategorii nowych funkcji można dodać kilka pozycji.

  * Bohaterowi niezależni mogą używać pochodni w pomieszczeniach (pod warunkiem, że jest dostatecznie ciemno).
  * Bohaterowie niezależni powracają do swojego domyślnego miejsca.
  * Komputerowi przeciwnicy są w stanie celować w osi pionowej (z pozdrowieniami dla wszystkich pastwiących się nad potworami z pozycji lewitacji).