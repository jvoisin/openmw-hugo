{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-01-27T15:51:05+00:00",
   "title": "Cotygodnik kontratakuje",
   "type": "post",
   "url": "/2014/cotygodnik-kontratakuje/"
}
Otwórzmy emulator terminala, przejdźmy do katalogu OpenMW, git pull, a teraz git log i zaraz zobaczymy co też nasi programiści w pocie czoła zakodowali. O nie! Nie ma commitów z tego tygodnia. Jak to możliwe? Czyżby dopadł nas jakiś kataklizm? Czy prace stanęły?

Oczywiście, że nie. Po prostu nie udało się nam ukończyć pracy nad żadną nowością w tym tygodniu, ale to nie oznacza jeszcze, że próżnowaliśmy.

Po pierwsze, a dla braci Słowian posługujących się cyrylicą zapewne i najważniejsze: greye pracował nad poprawionym wsparciem dla znaków z poza kodowania UTF-8; a więc głównie tych które nie są nudnymi wariacjami na temat liter alfabetu łacińskiego. Powinno to poprawić i ułatwić zabawę z OpenMW nie tylko Rosjanom, Ukraincom, Białorusinom ale też narodom azjatyckim ─ może nawet w końcu oderwiemy Japończyków od Final Fantasy.

Pvdk wprowadził do programu startowego możliwość szybkiego sprawdzenie wersji OpenMW ─ nie tylko samego numeru wersji, ale nawet skrótu git. Trudno o większą rozdzielczość. Niektórzy mogą mieć zastrzeżenia co do przydatności aż tak skrajnej szczegółowości. Pamiętajcie jednak, że o ile użytkownicy dystrybucji Linux nie mają najmniejszych trudności ze sklonowaniem i zbudowaniem OpenMW ─ a co za tym idzie są już zaznajomieni z tą informacją, o tyle użytkownicy Windows przeważnie ograniczają się do wersji już prekompilowanych; między innymi mini-wydań nightly ─ i zapewne ciepło przywitają bardziej drobiazgowe raporty.

A scrawl? Scrawl to jak powszechnie wiadomo gwóźdź programu każdego cotygodnika. Tym razem wysiłki tego programisty skupiły się na udoskonaleniach funkcji zapisu gry. Jak wspominałem w zeszłym tygodniu wymagane jest tu wciąż wiele pracy. Warunkuje to szczególną wagę efektywność ─ tym lepiej, że zajął się tym właśnie scrawl. Pozostaje mieć nadzieję, że wydanie 0.29.0 zostanie wzbogacone o już względnie kompletne wczytywanie i zapisywanie stanu gry.

OpenCS również się rozwija. Niedługo możliwym będzie klonowanie rekordów, a graffy wprowadził zaś znaczące zmiany do okna wyboru plików gry.