{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-11-27T18:04:07+00:00",
   "tags": [
      "animation",
      "clothing",
      "jhooks"
   ],
   "title": "Why I need title?",
   "type": "post",
   "url": "/2011/why-i-need-title/"
}
Mwrender refactoring is finished. That&#8217;s great, since it was required to add any other rendering features, like animations, terrain rendering or particles. And jhooks is working on integrating animations right now!

Well, since jhooks has not much time to work on it we can&#8217;t expect so much progress as recently. And animations in 0.12.0 won&#8217;t include clothing and armor since we have no proper inventory handling yet. But that does not mean that it won&#8217;t be putted on 0.13.0 road map just to save us from jhooks wrath.

Configuration & Packaging is going on too. The most interesting thing is probably data path detection, currently openmw reads it from config file and you need to put it there manually. That sucks and from time to time We have users with &#8220;omg! openmw does not work, no data file! HEEEEEEEEEELP!&#8221;. That sucks too. ;-)