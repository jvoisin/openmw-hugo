{
   "author": "psi29a",
   "categories": [
      "Uncategorized"
   ],
   "date": "2020-06-05T14:37:54+00:00",
   "title": "A rumble in the deep",
   "type": "post",
   "url": "/2020/rumble-deep/"
}
Let&#8217;s just jump right into it! We&#8217;ve recently forked off 0.46.0 into its own branch and started our release candidate (RC) process. This will be officially our biggest release yet! If you would like to help us test, feel free to join us on the <a rel="noreferrer noopener" href="https://forum.openmw.org/" target="_blank">forums</a>, <a rel="noreferrer noopener" href="https://webchat.freenode.net/?channels=openmw&uio=OT10cnVlde" target="_blank">IRC</a> or <a rel="noreferrer noopener" href="https://discord.gg/bWuqq2e" target="_blank">Discord</a>.  
  
  
<figure class="wp-block-image size-large">

<img loading="lazy" width="1119" height="629" src="https://i0.wp.com/openmw.org/wp-content/uploads/2020/06/screenshot038.png?resize=1119%2C629&#038;ssl=1" alt="" class="wp-image-5411" data-recalc-dims="1" /> <figcaption>Sunrise</figcaption></figure> <figure class="wp-block-image size-large"><img loading="lazy" width="1119" height="629" src="https://i0.wp.com/openmw.org/wp-content/uploads/2020/06/screenshot037.png?resize=1119%2C629&#038;ssl=1" alt="" class="wp-image-5412" data-recalc-dims="1" /><figcaption>Sunset</figcaption></figure> <figure class="wp-block-image size-large"><img /></figure>