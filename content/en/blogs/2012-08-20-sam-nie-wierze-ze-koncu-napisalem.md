{
   "author": "sir_herrbatka",
   "date": "2012-08-20T15:34:32+00:00",
   "title": "Sam nie wierzę, że w końcu to napisałem.",
   "type": "post",
   "url": "/2012/sam-nie-wierze-ze-koncu-napisalem/"
}
&#8230; i stało się tak, że Zini zakończył refaktoryzację mw-subsystems nawet pomimo przesiadki z Ubuntu na Mint. Spokój wobec wstrząsu jakie wywołać może tylko nagła zmiana codziennie widzianych kolorów (wyobraźcie sobie tylko, że trawa na stepie ciągnącym się po horyzont staje się pomarańczowa) najlepiej świadczy o poziomie kwalifikacji naszego lidera. Zakładam, że interesują was te szczegóły &#8212; bo jakże mogłoby być inaczej, ale najważniejsze, iż nowe wydanie jest już blisko. Bardzo blisko, bo w fazie RC.

Ostatnie zadanie przypadło w udziale Greye. Jak już wspominałem pracował nad zadaniem „Player Controls” które z wielu istotnych powodów jest wręcz fundamentalne dla przyszłych wydań. Ponadto już w 0.17.0 będziecie mogli zobaczyć postać bohatera &#8212; jest już renderowana, a ponadto pojawił się tryb widoku z za pleców oraz widok „postojowy”.

Nie przecinamy wstęgi i nie urządzamy imprezy z okazji nowej, niebywale wspaniałej, niewątpliwie najlepszej od czasów poprzedniej wersji OpenMW bo wciąż ogrom prac przed nami. Nie ma powodu by zatrzymywać się bo skutkować może to tylko utratą tempa.

Scrawl oczywiście to rozumie, a ponieważ input system łasił się już od jakiegoś czasu to poszedł na pierwszy ogień. Dzięki przepisaniu tej części kodu oraz dodatkowemu wysiłkowi włożonemu w GUI można już ustawić czułość myszy. 

Z pewnością wiecie, że w ustawieniach Morrowind można sprecyzować czułość myszy w osi pionowej i poziomej, mam jednak szczerą nadzieję, że rozstanie z dwoma suwakami nie będzie wymagało krzyków, płaczu oraz innych objawów Czarnej Rozpaczy bo sens tego rozdziału wymyka się naszemu pojmowaniu. W związku z powyższym: jeśli wystąpią okoliczności (których wyobrazić sobie nie potrafimy) wymagające dostosowania reakcji myszy w sposób aż tak zawiły, jedynym sposobem pozostaje edycja pliku z konfiguracją. 

Gdy teraz o tym myślę wydaje mi się, że ciekawie byłoby spróbować ustawić czułość w poziomie wyższą niż w pionie, bo w końcu łatwiej jest rozejrzeć się na boki niż zadrzeć głowę do góry.

Nowością są niezależne ustawienia czułości myszy dla GUI. Pamiętajcie tylko, że jeśli ustawicie ją na najniższą wartość możecie mieć „drobne” problemy z przesunięciem suwaka w prawo. Cierpliwość w takim wypadku będzie niezbędna.

Jhooks1 sprawił, że można już obrócić npc za pomocą instrukcji setangle. Dodatkowo naprawił animacje, które teraz są gładziutkie niczym futerko kota i w żadnym wypadku nie unoszą animowanej postaci w powietrze.

Eli2 spokojnie podąża swoją ścieżką. Edytor rozwija się doskonale, choć jak twierdzi sam programista jest to obecnie w większości jeden wielki hak (ale taki przez ck). Od wydania 1.0.0 oddziela nas jeszcze może 6 lub 8 numerków ale edytor byłby przydatny już teraz; CS trudno nazwać cudem techniki i wielu modderów z pewnością przesiadło się na coś wydajniejszego i bardziej ergonomicznego.

W gruncie rzeczy nas także można zaliczyć do grona modderów bo planujemy wypuszczenie dema prezentującego silnik, jednocześnie sprawiając trochę frajdy dla gracza ale to głupota i kto by właściwie chciał o tym czytać?