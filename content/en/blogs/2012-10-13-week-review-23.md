{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-10-13T13:09:03+00:00",
   "tags": [
      "alchemy",
      "drpaneas",
      "fast travel",
      "Greye",
      "Gus",
      "Mark76",
      "multiple esm",
      "record saving",
      "scrawl",
      "skill gain",
      "Zini"
   ],
   "title": "the week in review",
   "type": "post",
   "url": "/2012/week-review-23/"
}
Welcome!

If you did not visit our forum recently you could miss new theme. It&#8217;s done by jedd and necrod, looks very nice and integrates with wordpress theme. This is not everything, lgro &#8212; our admin installed a brand new anti-spam plugin that should reduce the daily cleaning for admins and moderators &#8212; it seems to work good.

We have also quite a lot of programming progress. Scrawl added skill gain for training books and also started to work on video playback feature so maybe finally you will be able to see the intro movie as well as create spells since scrawl also works on it. In addition, as usual some bug fixing, most visible for global map not updating when it&#8217;s pinned.

gus started to work on fast travel service: silt striders, boats and guild guides will be finally able to take you to the other towns. It seems that guild guides works just as silt striders and the only difference is that mages are located in interiors and will take you to the other interior so game is not able to calculate distance and in this case it just assumes that travel will cost 10 gold and be instant. 

drpaneas (a new developer) started to work on his first task that is implementing a failed action. It&#8217;s quite important!

Mark76 showed up on the forums recently. Nothing has changed for the multiple esm and esp support but zini thinks about splitting it so it won&#8217;t be so overwhelming.

Zini on his own works on proper alchemy implementation. Needless to say current solution was nothing else then a prototype and was never intended to go into the 1.0 so it was just too strongly tied to the GUI and was just not planned properly. This is about to change. 

greye is working on the old task of record saving. It&#8217;s very important task for both game and the editor.