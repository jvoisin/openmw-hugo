{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-12-12T21:37:06+00:00",
   "title": "Zapowiedź nowej wersji",
   "type": "post",
   "url": "/2016/zapowiedz-nowej-wersji/"
}
​OpenMW w wersji 0.41.0 zostanie opublikowane za tydzień lub dwa. W ramach przygotowań, nasz nowy release manager, raevol, kompletuje listę zmian, a Atahualpa pracuje nad filmem prezentującym nowe wydanie. W międzyczasie nasi deweloperzy rozpoczęli pracę nad OpenMW w wersji 0.42.0.

​Przy okazji chcielibyśmy poinformować, że jest już możliwa jazda próbna na wersji 0.41.0. Co prawda nie została ona jeszcze wydana, ale staramy się dać naszemu wydaniu RC (Release Candidate) okazję do paru szybkich testów, by zobaczyć, czy działa dobrze zaraz po instalacji. Wypowiedzi dotyczące efektów testowania są rozsiane na ostatnich stronach [wątku poświęconego OpenMW 0.41.0][1].

​Czasami nasze RC mają problem z OpenMW-CS, a czasem ktoś zapomni dołączyć jakiś ważny komponent. W przypadku napotkania jakichkolwiek problemów, prosimy o napisanie posta we [wspomnianym wątku][1]. Jeśli wszystko działa dobrze, to również prosimy o informację, byśmy wiedzieli, że kandydat do wydania został przetestowany.

##### ​Zapraszamy do komentowania [tutaj][2].

 [1]: https://forum.openmw.org/viewtopic.php?f=20&t=3768
 [2]: https://forum.openmw.org/viewtopic.php?f=38&t=3989