{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-10-29T11:13:02+00:00",
   "title": "O mój Boże! Post za burtą!",
   "type": "post",
   "url": "/2012/moj-boze-post-za-burta/"
}
Witajcie ponownie moi drodzy (nie sądzę by czytała mnie choćby nawet jedna niewiasta, ale któż to wie&#8230;).

Prace nad nową implementacją alchemii zostały niemal zamknięte ─ niestety dzięki ofiarnej pracy testerów na światło dzienne wyszły pewne niewygodne błędy których istnienia nie sposób pominąć. Można oczywiście potraktować to inaczej, podejść do kwestii z otwartymi ramionami; optymistycznie i z uśmiechem na twarzy. W takiej różowo-pluszowej wizji świata odnalezienie błędów w oprogramowaniu to nic innego jak tylko pierwszy, a więc naturalnie najważniejszy krok do ich usunięcia. 

Ponadto Zini wciąż pracuje w gałęzi „Death”. Niestety w tej gałęzi także mamy zdecydowany nadmiar ciekawych zagadnień do rozgryzienia, i to pomimo wysiłków Ziniego.

Gus tymczasem powrócił do nie dawno zakończonej funkcji transportu pomiędzy miastami. Do uprzątnięcia był kod dialogów.

Greye tymczasem zajął się pracą w gałęzi „Storedevel” ─ jak sama już nazwa sugeruje nie znajdziemy tam żadnych ciekawych, widowiskowych funkcji widocznych z perspektywy gracza, ale jednak jest to praca ważna; fundamentalna dla całego projektu.

Chris tymczasem wciąż zajmuje się animacjami. Końca tego zadania wciąż nie widać, za to jeśli obejrzeć się za siebie zobaczymy wiele już zostało dokonane. Jak już tłumaczyłem ten problem jest skomplikowany od strony technicznej i nie sposób objaśnić w przystępny sposób listę zadań do wykonania. 

Niestety uwaga to odnosi się do coraz to szerszego zakresu prac nad projektem.

Tymczasem Eli2 zajmuje się edytorem. Nudziło mu się więc stworzył port do Qt5 z czego możemy wysnuć wniosek o tym, że zadanie nie było trudne. W istocie jednak Qt5 nie ma dużego znaczenia dla OpenMW, jeszcze przez wiele lat będziemy musieli opierać się na starszych bibliotekach by wspierać dystrybucje LTS. Z ważniejszych rzeczy: serializer Stopgap został zastąpiony przez ten z Qt.

Pvdk powrócił do prac nad starterem, dzięki czemu powróciły profile!