{
   "author": "jvoisin",
   "categories": [
      "Week's review"
   ],
   "date": "2012-05-13T19:37:07+00:00",
   "tags": [
      "animation",
      "Gus",
      "jhooks",
      "scrawl"
   ],
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-6/"
}
Hello everyone! The killer-feature of the week (yup, we&#8217;ve got one!) is the _drag&#8217;n&#8217;drop_, thanks to **gus**. The code is not yet perfected, and not everything is fully functional, but **scrawl** is currently cleaning and improving the code with **gus**.

**jhooks** is still working on Ogre3d-powered animations. There are still issues with some creatures ([ancestor ghost][1]) but with the help of **Chris** everything is slowly moving forward and I believe that even this issue will eventually be solved.

**scrawl** worked on several things:

  * Introduction of a [new font][2] (not under GPL, unfortunately, but it&#8217;s better than nothing) for [daedric][3] alphabet.
  * Improving scrolls/books interaction and rendering
  * Bugfixes and improvement of the GUI : you can now scroll with the mouse wheel !
  * The quote of the week :  
    > OpenMW just got to a whole new level  
    > because you can now steal Fargoth&#8217;s ring !

Thanks to the propaganda of the PR team, we attracted several future new contributors: maybe they&#8217;ll be in the next _Week in review_ post, who knows ;)

 [1]: http://images.uesp.net/0/04/MWCreature-AncestorGhost.jpg
 [2]: http://www.uesp.net/wiki/File:Obliviontt.zip
 [3]: http://www.uesp.net/wiki/Lore:Daedric_Alphabet