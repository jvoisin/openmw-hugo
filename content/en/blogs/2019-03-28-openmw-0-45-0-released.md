{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2019-03-28T12:31:14+00:00",
   "title": "OpenMW 0.45.0 Released!",
   "type": "post",
   "url": "/2019/openmw-0-45-0-released/"
}
The OpenMW team is proud to announce the release of version 0.45.0! Grab it from our [Downloads Page][1] for all operating systems. This release brings yet another horde of bug fixes and several new features, including support for per-group KF animation replacers, 360° screenshots, and the ability to brew a whole stack of a potion at once.

If you haven&#8217;t tried OpenMW out already, now is the big chance! [Bethesda&#8217;s free giveaway of Morrowind][2] got extended, so you are still able to get Morrowind _for free_ until March 31st!

Check out the [release video][3] and the [OpenMW-CS release video][4] by the illustrious Atahualpa, and see below for the full list of changes.





**Known Issues:**

  * Shadows are not yet re-implemented
  * There&#8217;s currently no way to redirect the logging output to the command prompt on Windows Release builds &#8212; this will be resolved in version 0.46.0
  * To use generic Linux binaries, Qt4 and libpng12 must be installed on your system
  * On macOS, launching OpenMW from OpenMW-CS requires OpenMW.app and OpenMW-CS.app to be siblings

**New Features:**

  * Non-actor objects display visual effects during spellcasting (#1645)
  * Creatures without any collision box use auto-generated ones (#2787)
  * Launcher: File paths of selected content files can be copied via context menu (#2847)
  * Actors play casting animations during scripted spellcasting (#3083)
  * As an option, barter deals permanently modify merchant disposition (#3103)
  * Ranged weapons deal critical hits to unaware opponents (#3703)
  * 360° screenshots of various kinds can be made (#4222)
  * &#8216;ToggleBorders&#8217; debug instruction (&#8220;tb&#8221;) available in console (#4256)
  * Sound generator keys in activator animations are supported (#4285)
  * On macOS, function key shortcuts can be used (#4324)
  * Some of the debug settings previously available as command line arguments can be set up in the launcher (#4345)
  * Support for per-group KF animation replacers has been added (#4444)
  * Shader water is rougher during bad weather (#4488)
  * The number of enchanted items in a stack is shown in the Spells window (#4509)
  * The actual chance of hitting the target is used for AI weapon rating instead of just the skill (#4548)
  * The real potential damage of a weapon is used for its rating (#4549, #4697)
  * A bonus is applied to the rating of ranged weapons if the attacker is far enough from the target (#4550)
  * As an option, the Strength attribute affects Fatigue and Health damage in hand-to-hand combat (#4579)
  * The logging system has been heavily reworked (#4581)
  * AI no longer uses magic effects that affect hit chance if the enemy has not taken the appropriate stance (#4624)
  * AI uses the root mean square of melee-weapon damage for more precise weapon rating (#4625)
  * AI accounts for weapon speed when rating a weapon (#4626)
  * Various vanilla GMSTs regarding combat action rating are utilised (#4632)
  * &#8216;sTo&#8217; GMST (&#8220;to&#8221;) replaces hyphen in the Spellmaking menu (#4636)
  * New count field in the Alchemy window allows you to brew multiple potions at the same time (#4642)
  * To improve performance, actors outside of the AI processing range are no longer rendered but faded out (#4647)
  * AI processing range can be configured via an in-game slider and a configuration option (#4647)
  * If the animated creature model lacks a collision box, the non-animated model&#8217;s collision box is used as a fallback (#4682)
  * Tooltips of thrown weapons show the actual in-game damage (twice the base-record damage) the projectiles have (#4697)

**New Editor Features:**

  * All top-bar dropdown menu options and most context menu options have icons (#912, #4506)
  * Actors are rendered in Scene view (#1221)
  * Verifier functionality covers Enchantment records (#1617)
  * Added a check box to execute a case-sensitive global search (#2606)
  * Shift + C is default shortcut for viewing cells (#2845)
  * Shift + V is default shortcut for previewing records (#2845)
  * Global search has a status bar (#3276)
  * Frame rate in Preview window can be limited (#3641)
  * Log files are created in case of a crash (#4012)
  * Previously sorted-by-ID lists, such as magic-effect lists, are now alphabetically sorted (#4404)
  * As an option, base records are ignored in verifier runs (#4466)
  * Already existing marker models for light sources and creature levelled lists are now used (#4512)

**Bug Fixes:**

  * All actors (not only those in active cells) restore Health and Magicka during player character rest (#1875)
  * Precise times of sunrise and sunset are now recovered from the imported Morrowind INI file (#1990)
  * Actors now properly aim during scripted spellcasting (#2131)
  * Skills and attributes that affect trading no longer cause low selling prices if they&#8217;re too high (#2222)
  * Landing sound behaviour for NPCs and the player character has been reworked (#2256)
  * Scripted movement of an object now adjusts the position of actors standing on top of that object (#2274)
  * The last equipped item of a certain type is automatically re-equipped once a bound-item spell of the same type expires (#2326)
  * Restore effects can restore drained stats (#2446)
  * Attacks from unarmed, non-bipedal creatures no longer degrade armour (#2455)
  * For the time being, actors are no longer allowed to activate teleport doors so that their abrupt cell change doesn&#8217;t cause a crash (#2562)
  * Using &#8216;Resurrect&#8217; instruction on a dead player character fully resumes the game (#2626)
  * References to non-existent classes or factions no longer crash the game (#2772)
  * Weapons are visually unequipped before the start of a spellcasting stance transition (#2835, #4327)
  * The player character now gets a bounty when a player follower commits a murder (#2852)
  * To prevent issues on macOS, SDL is only initialised for the Graphics tab of the launcher when Qt5 is used (#2862, #3911)
  * Explicit reference calls no longer break [Tab] autocompletion in the console (#2872)
  * Naked expressions starting with a member operator are no longer allowed outside of the console (#2971)
  * The handling of Drain and Fortify effects for health, magicka, and fatigue has been reworked to match vanilla behaviour (#3049, #4231)
  * Enemies are no longer immune to ranged attacks at very close range (#3059)
  * Adding an item with a self-equipment script to an actor no longer causes a freeze (#3072)
  * Actors who are placed above the ground are now snapped down at a significantly larger distance when a cell is loaded, preventing them from falling to their death (#3219)
  * TrueType fonts are now properly rescaled (#3288)
  * Targets are now much easier to hit with on-touch spells (#3374)
  * The handling of scripted and death animations has been reworked to significantly improve mod compatibility (#3486, #4286, #4291, #4307)
  * &#8216;GetSpellEffects&#8217; scripting function can now detect zero-duration effects (#3533)
  * Enemies are much easier to hit with low-reach weapons, since the intended start position of the attack raycast is now replicated (#3591)
  * Scripted sleeping interruptions no longer prevent the spawning of random sleep encounters (#3629)
  * Calling &#8216;AddSoulGem&#8217; and &#8216;RemoveSpell&#8217; scripting instructions with an extra argument no longer breaks script compilation (#3762)
  * &#8216;GetPCInJail&#8217; scripting function has been reworked to more closely match vanilla behaviour (#3788)
  * Previous placeholder implementation of &#8216;GetPCTraveling&#8217; scripting function has been replaced with a proper solution (#3788)
  * Message boxes can now have newline characters in the text argument (#3836)
  * Alignment of terrain texture painting has been corrected to match the vanilla look (#3876)
  * Attenuation of magic light sources is now properly calculated (#3890)
  * &#8216;Goodbye&#8217; scripting function in dialogue results now makes any additional choices act like &#8216;Goodbye&#8217; (#3897)
  * &#8216;RemoveSpellEffects&#8217; scripting instruction now also removes permanent effects (#3920)
  * Movement prediction for spell aiming now uses the correct GMST to calculate the magic projectile&#8217;s speed (#3948)
  * Animated collision shapes are no longer erroneously optimised (#3950)
  * The terrain texture blending map is now upscaled to more closely match the vanilla look (#3993)
  * Issues with actors not returning to their initial position in various situations have been fixed (#3997, #4251, #4393)
  * AI packages with a non-unique target no longer choose a random target with the same ID (#4036)
  * On macOS, version numbers are now properly displayed in the application properties (#4047)
  * Text after the last end-of-line tag is no longer shown in books to match vanilla behaviour (#4215)
  * &#8216;FixMe&#8217; script instruction has been reworked to more closely match vanilla behaviour (#4217)
  * Jumping on slopes is now less restrictive, preventing the player character from getting stuck in V-shaped terrain (#4221)
  * Pathgrid nodes which lie between an actor and that actor&#8217;s destination are now ignored (#4230)
  * Actors with an AiTravel package now stop near their target position if that position is blocked by another actor (#4230)
  * [Activate] key can no longer be held down to spam Persuasion and other kinds of repeatable actions (#4260)
  * Turning animations no longer reset idle animations (#4271)
  * Death animations from pre-0.43.0 savegames are now forward-compatible (#4274)
  * &#8216;CenterOnCell&#8217; script instruction (&#8220;coc&#8221;) now teleports the player character to the accurate door marker or exterior position for interior and exterior cells respectively (#4292)
  * Faction members are now aware of faction ownership during barter (#4293)
  * AiWander packages which are placed before an AiFollow package in the AI package list no longer override vital functionality of that AiFollow package (#4304)
  * Key focus in containers is now automatically (re)set to the [Close] button to prevent players from accidentally stealing items (#4333)
  * Spellcasting stance transitions no longer interrupt movement animations (#4358)
  * OK button in the Settings window has key focus by default (#4368)
  * It&#8217;s no longer possible to cast an Absorb spell on oneself (#4378)
  * Non-audio files no longer crash the game when the engine tries to play them (#4416)
  * When there is a &#8216;NiStringExtraData&#8217; node with MRK value in a NIF model node, only &#8216;NiTriShape&#8217; meshes starting with &#8220;Tri EditorMarker&#8221; are ignored instead of the whole node (#4419)
  * &#8216;RotateWorld&#8217; scripting instruction now rotates objects around the correct axis, while keeping the objects&#8217; initial rotation (#4426)
  * Some issues with building OpenMW on Windows and macOS were fixed (#4429, #4613)
  * Applying the console command &#8220;Lock 0&#8221; to a door or a container now creates an unbreakable lock, like in vanilla Morrowind (#4431)
  * Actors without any AI packages now properly return to their initial position after combat or pursuit (#4432)
  * Guards with AiAlarm = 0 no longer attempt to arrest the player character when they observe a crime but no one reports it (#4433)
  * &#8216;Begin&#8217; and &#8216;End&#8217; script keywords can now be followed by a comma (#4451)
  * The default terrain texture no longer shines through terrain texture blending transitions (#4452)
  * Broken items are now ignored when trying to equip an item via quick key (#4453)
  * Actors now open doors more quickly (#4454)
  * Light sources which cannot be carried no longer prevent the AI from auto-equipping shields (#4457)
  * The handling of idle chances in AiWander packages has been reworked to more closely match vanilla behaviour (#4458)
  * &#8216;NotCell&#8217; dialogue condition now supports partial name matches (#4459)
  * Scripted equipment now bypasses most standard equipping restrictions, e.g., those for beast races (#4460)
  * When a non-player object casts an Open spell, a player crime event is triggered if the player character is detected to ensure vanilla compatibility (#4461)
  * General-number formatting (&#8220;%g&#8221;) now works like in vanilla Morrowind and properly handles the fractional part (#4463)
  * Cancelled AI packages are now removed from the package cache (#4464)
  * Non-ASCII strings are now properly handled in the content selector and in the ESM reader (#4467, #4653)
  * Silt striders in abot&#8217;s &#8220;Silt Striders&#8221; mod are no longer rotated by 90 degrees (#4469)
  * The behaviour of weapon-using non-bipedal creatures has been reworked to make them less helpless in certain situations (#4470)
  * Actors who are vampires now display their regular record head when no vampire head is found (#4474)
  * Scripted animations no longer move actors away from their initial position (#4475)
  * Potential crashes in the quick-keys menu when an item is no longer available in the inventory no longer occur (#4480)
  * &#8216;Goodbye&#8217; dialogue scripting instruction now disables hyperlinks in dialogue topics (#4489)
  * &#8216;PositionCell&#8217; scripting instruction no longer tries to add local scripts to scripted items in the teleported player character&#8217;s inventory twice (#4490)
  * Training cap is now limited by the trainer&#8217;s modified skill instead of their base skill (#4494)
  * Crossbow reloading animations now only apply to the upper body (#4495)
  * Crossbows now use animations for one-handed weapons as a fallback (#4495)
  * Animations for turning in spellcasting stance are now used (#4496)
  * Animated meshes without slashes in their full path are now properly classified as animations (#4497)
  * Scripted spellcasting no longer increases the Alteration skill (#4503)
  * A potential zero division in the Fatigue calculation has been removed (#4510)
  * A knocked-down player character can no longer move or be moved in first-person view (#4519)
  * Some issues with sun specularity in the water shader have been fixed (#4527)
  * Movement animations without idle-animation fallback no longer trigger idle animations (#4531)
  * The game now switches between distorted (underwater) and undistorted sound effects based on the camera position instead of the player character&#8217;s head position (#4532)
  * The inventory paper doll is now affected by GUI scaling (#4539)
  * Items with an &#8216;OnActivate&#8217; command present in their script (such as &#8220;cursed&#8221; items) no longer visually disappear when they are picked up in menu mode (#4543)
  * Creatures no longer incorrectly flee from werewolves (#4545)
  * Minimum and maximum sound ranges with zero value are now separately reset to their default ranges (#4551)
  * Non-actor objects can no longer have a dialogue window opened via script (#4553)
  * Dialogue topics with reserved names, e.g., &#8220;Barter&#8221; or &#8220;Repair&#8221;, now behave the same as in vanilla Morrowind (#4557)
  * Optimiser search for reserved node names is now case-insensitive (#4558)
  * Pinned windows are now properly updated (#4560)
  * Fast-travel logic now always depends on the type of the service NPC&#8217;s cell instead of the destination cell type (#4563)
  * The underwater viewing distance limit is no longer broken (#4565)
  * The player character no longer uses headtracking in first-person view (#4573, temporary workaround for another issue)
  * The handling of player character turning animations has been improved (#4574)
  * Several weapon animation inconsistencies during movement in first-person view have been fixed (#4575)
  * Idle animations are no longer reset in certain situations where they should be continuous (#4576)
  * If an innocent actor dies from the continuing effects of a player attack, the murder is now also reported if the player has already paid their fine (#4582)
  * Attack strength is now 0 if the player spams the [Attack] button (#4591)
  * The incorrect inequality operator &#8220;<>&#8221; is now interpreted as less-than operator &#8220;<&#8221; to ensure compatibility with certain mods (#4597)
  * Picking a stack of gold up in menu mode now grabs the whole item stack (#4604)
  * Scaling for animated collision shapes is no longer erroneously applied twice (#4607)
  * Fall damage is no longer erroneously applied twice (#4608)
  * The pseudo duration of instant magic effects has been adjusted in the spellmaking menu to match vanilla spell cost calculations (#4611)
  * A zero division occurring in &#8216;NiFlipController&#8217; nodes without textures has been removed (#4614)
  * The flickering frequency of light sources and some light-related calculations have been fixed (#4615)
  * The sneaking offset in first-person view is now also applied when switching to sneak mode in midair (#4617)
  * Sneaking is no longer possible while flying (#4618)
  * Recharging enchanted items now always increases the Enchant skill regardless of success or failure, like in vanilla Morrowind (#4622)
  * NPC record entries Reputation, Disposition and Faction Rank are now loaded as unsigned variables (#4628)
  * Sneaking stance no longer affects speed if the actor is not actually sneaking (#4633)
  * &#8216;GetPCJumping&#8217; scripting function has been reworked to match vanilla behaviour (#4641)
  * &#8220;%Name&#8221; string replacer can now be used in creature dialogue (#4644)
  * Force-equipping a weapon now resets ongoing attack animations (#4646)
  * HUD no longer displays an &#8220;item condition&#8221; for throwing weapons (#4648)
  * Levelling up no longer fully restores health but rather applies the gained health points (#4649)
  * Actors no longer fall from cliffs or run into obstacles when backing down from attackers (#4656)
  * Enabling collision with &#8216;ToggleCollision&#8217; scripting instruction (&#8220;tcl&#8221;) now snaps the player character down to the ground (#4669)
  * The number of known ingredient and potion effects is now determined by the modified rather than the base Alchemy skill (#4671)
  * Pitch factor handling for crossbow animations has been streamlined to more closely match vanilla behaviour (#4672)
  * Journal can no longer be opened while the Settings window is open (#4674)
  * NPC fast-travel destination records without any data no longer crash the game upon loading (#4677)
  * Declared but empty script variable lists no longer crash the game upon loading (#4678)
  * The chance of a successful spell absorption is now multiplicative rather than additive, which is how the magic effect is designed in vanilla Morrowind (#4684)
  * Calling &#8216;Say&#8217; scripting instruction with non-existent sound as argument no longer breaks script compilation (#4685)
  * Creatures that lack unique sounds now make use of fallback sound generator records (#4689)
  * Loading bars no longer obscure message boxes but are instead moved to the centre of the screen (#4691)

**Editor Bug Fixes:**

  * &#8216;Revert&#8217; action now properly updates subviews (#3249)
  * A native colour picker is now used on all operating systems, fixing major issues on macOS (#3681)
  * &#8216;Undo&#8217; and &#8216;Redo&#8217; actions now keep assigned shortcuts (#4110)
  * Lock status of newly opened subviews is now correctly initialised (#4520)
  * Instance dragging, which was broken in 0.44.0, has been fixed (#4593)
  * Skeletons of animated objects are now properly initialised (#4654)
  * The Colour entry in Light Source records is now displayed as a coloured box instead of an integer number (#4668)

**Other:**

  * Windows builds in &#8216;Release&#8217; mode no longer automatically open a command prompt window (#2490)
  * Race condition in terrain code has been fixed (#4584)
  * Skinning and AI optimisations (#4605, #4621)
  * Support for Rapture3D OpenAL driver has been added (#4606)
  * Record verifying functionality in OpenMW-CS has been revised (#4643)

##### <a title="Want to leave a comment?" href="https://forum.openmw.org/viewtopic.php?f=38&t=5830" target="_blank" rel="noopener noreferrer">Want to leave a comment?</a>

 [1]: https://openmw.org/downloads/
 [2]: https://elderscrolls.bethesda.net/en/tes25
 [3]: https://www.youtube.com/watch?v=v5vMfYmqOMo
 [4]: https://www.youtube.com/watch?v=WVztdWo-TNQ