{
   "author": "sir_herrbatka",
   "date": "2013-02-07T16:59:12+00:00",
   "title": "Powróciłem.",
   "type": "post",
   "url": "/2013/powrocilem/"
}
Stęskniliście się za mną? Na pewno się stęskniliście, a nawet jeśli nie to pozwólcie słodkim snom trwać choć odrobinę dłużej trwać; jak poranna rosa na letnim słońcu, jak dym ze zdmuchniętej świecy, jak bańka mydlana. Bo rzeczywistość w koło jest wroga i ma wielkie, ostre jak brzytwy kły.

Ale ponieważ mam pisać o openmw nie mogę zapomnieć o postępach w pracach nad animacjami. Chris zdziałał naprawdę wiele, mocno w to wierzę nawet jeśli nie rozumiem nawet skromnego ułamka z jego wyczynów (co równocześnie tłumaczy długość tego obowiązkowego fragmentu).

Scrawla lubię, a jego projekty szczerze uwielbiam, głównie z racji wspaniałej prezencji efektów na zrzutach, co wyraźnie oszczędza mi wysiłku opisywania ich rewelacyjności. Tym razem zajął się efektem rozchodzenia się fal w wodzie po przejściu postaci. A ponieważ jeden <a href="http://scrawl.bplaced.net/perm/water.png" title="Tak, kliknij! Już! Teraz! Na! Co?! Czekasz?!" target="_blank">obraz</a> znaczyć może więcej niż tysiąc słów&#8230; Są też słowa które znaczą więcej niż obrazów. Zwłaszcza jeśli ktoś je zapisze.

Blunted2night wciąż pracuje nad implementacją dziennika, a przy okazji dodał kilka brakujących do tej pory zmiennych w rodzaju OnPCEquip która ma swój udział w przykrej niespodziance po wypróbowania czerpaka.

Poza tym bugfixy. Nudy na pudy. 

Ciekawszą wiadomością jest fakt zbliżającej się premiery OpenMW w wersji 0.21.0. Większych nowości niestety w tym wydaniu nie uświadczymy, ale wypróbować przecież zawsze warto. Film na youtube od WeirdSexy zapewne również nas nie ominie, co jest oczywiście wiadomością przynajmniej dobrą.