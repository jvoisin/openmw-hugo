{
   "author": "Vedyimyn",
   "categories": [
      "Nowa wersja"
   ],
   "date": "2015-05-30T11:29:10+00:00",
   "title": "OpenMW 0.36.0 zostało wydane!",
   "type": "post",
   "url": "/2015/openmw-0-36-0-zostalo-wydane/"
}
Zespół OpenMW ma zaszczyt ogłosić wydanie wersji 0.36.0! To dobrania w dziale „[Do ściągnięcia][1]”, dla wszystkich systemów operacyjnych. To wydanie wprowadza stosunkowo niewiele zmian. Czekamy z&nbsp;niecierpliwością na port na nowy silnik (OSG), nad którym pracuje scrawl. Wprowadzono mnóstwo poprawek do OpenMW-CS, które rozwija się w niezłym tempie. Poniżej pełna lista zmian.

**Znane problemy:**

  * Przełączenie się z pełnego ekranu na tryb okienkowy, przy użyciu silnika renderującego D3D9, powoduje wysypanie się programu.
  * Wysypywanie się OpenCS przy próbie włączenia podglądu cella na systemie OS X.

**Lista zmian:**

  * Implemented hotkey for hand-to-hand
  * Implemented key bind for always sneaking
  * Implemented multiselection of entries in the data files list of the launcher
  * Implemented using application support directory as the user data path on OS X
  * Fixed Erene Lleni not wandering correctly
  * Fixed installer not detecting default directory correctly in 64-bit Windows registry
  * Fixed issue where cloning the player would prevent player movement
  * Fixed casting bound weapon spell not switching to “ready weapon” mode
  * Fixed enchanted item charges not updating in the spell list window if it is pinned open
  * Fixed behavior when fatigue falls below zero due to the drain effect
  * Fixed error on startup with Uvirith’s Legacy enabled
  * Fixed sex, race, and hair sliders not initializing properly in chargen
  * Fixed minor issues with terrain clipping through statics
  * Fixed invisible sound mark having collision in Sandus Ancestral Tomb
  * Fixed tooltip display on stolen gold stack
  * Fixed persuasion mechanics using player stats instead of NPC’s
  * Fixed not defaulting to world origin when an unknown cell is encountered
  * Fixed NPCs not reacting to theft when player has the inventory open
  * Fixed water z-fighting issue
  * Fixed GetSpellEffects to better emulate vanilla behavior
  * Fixed a dialog choice in mod Rise of House Telvanni not exiting out of dialog
  * Fixed Detect Spell returning false positives
  * Fixed the map icons used by Detect spells
  * Fixed a crash on first launch after choosing “Run installation wizard”
  * OpenMW-CS: Implemented global search & replace
  * OpenMW-CS: Implemented dialog mode only columns
  * OpenMW-CS: Implemented optionally showing a line number column in the script editor
  * OpenMW-CS: Implemented optionally using monospace fonts in the script editor
  * OpenMW-CS: Implemented focusing on ID input field on clone/add
  * OpenMW-CS: Implemented handling moved instances
  * OpenMW-CS: Implemented a start script table
  * OpenMW-CS: Implemented a start script record verifier
  * OpenMW-CS: Fixed changed entries in the objects window not*? showing as changed
  * OpenMW-CS: Fixed changing certain entries not being saved in the omwaddon file
  * OpenMW-CS: Fixed terrain information not saving
  * OpenMW-CS: Fixed files with accented characters not being listed
  * OpenMW-CS: Fixed cloning/creating new container class resulting in an invalid omwaddon file
  * OpenMW-CS: Fixed cloning omitting some values
  * OpenMW-CS: Fixed crash when issuing undo command after the table subview is closed
  * OpenMW-CS: Fixed being unable to undo a record deletion in the object table
  * OpenMW-CS: Fixed multithreading for operations
  * OpenMW-CS: Changed References/Referenceables terminology to Instance/Object

##### **Zapraszamy do komentowania [tutaj][2]!**

 [1]: https://openmw.org/downloads/
 [2]: https://forum.openmw.org/viewtopic.php?f=38&t=2910