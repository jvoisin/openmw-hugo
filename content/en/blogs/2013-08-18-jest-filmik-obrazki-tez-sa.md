{
   "author": "sir_herrbatka",
   "date": "2013-08-18T18:09:29+00:00",
   "title": "Jest filmik, i obrazki też są.",
   "type": "post",
   "url": "/2013/jest-filmik-obrazki-tez-sa/"
}
Witajcie po raz kolejny,

Prawdę powiedziawszy tydzień zaliczyć należy po raz kolejny do udanych. Chris zajął się udoskonalaniem implementacji fizyki (nie pytajcie o szczegóły, niczego z tego nie rozumiem), a poza tym poprawił obsługę wilkołactwa w grze. Poza tym od teraz pułapki są w stanie rzucić na gracza zaklęcia, okno alchemii posiada poprawiony układ i można zmienić jego rozmiar, oraz (rzecz jasna) kilka bugów mniej w OpenMW.

A mimo to nie chciałbym pisać o samym OpenMW, a raczej skupić się na długo pomijanym w tych wpisach aspekcie naszego projektu – edytorze OpenCS.

OpenCS wygląda aktualnie <a title="Yes, my desktop" href="http://wstaw.org/m/2013/08/17/zrzut_ekranu6.png" target="_blank">tak</a>. Zrzut pochodzi z mojego komputera na którym uruchomionym jest właśnie środowisko KDE. OpenCS jako aplikacja korzystająca z toolkitu Qt integruje się ze stylem, oraz schematem kolorystycznym skutkiem czego okno jest szare. Nie jest to właściwość samego OpenCS.

Tak jak widać na zrzucie, OpenCS to obecnie w dużej mierze nic innego jak po prostu okno z tabelami. Ilość tabel w oknie nie jest ograniczona w żaden sposób, można nawet dodać kilka tabel tego samego typu, jeżeli z jakiegoś powodu jest to potrzebne. W oknie swobodnie można zmienić rozmiar zadokowanych tabel, przemieszczać je, a nawet odłączyć. Osoby używające KDE lub jakiegoś programu napisanego w Qt (choćby smplayer) mogą już kojarzyć to zachowanie. Co prawda często aplikacje Qt pozwalają również na grupowanie docków w formie kart, ale OpenCS nie posiada tej funkcji.

Na zrzucie widocznym są dwa okna OpenCS obsługujące jedną instancję OpenCS. Można otworzyć dowolną liczbę okien (zawierających dowolną liczbę docków). Rozwiązanie takie z pewnością docenią użytkownicy komputerów z więcej niż jednym monitorem oraz środowisk graficznych posiadających funkcje ułatwiające pracę z wieloma oknami.

Każda tabela zawiera zestaw elementów z ich właściwościami, a jeżeli właściwości jest dużo tabela automatycznie staje się bardzo szeroka. Bez obaw, ten aspekt został uwzględniony przy projektowaniu i w żadnym stopniu nie utrudnia pracy.

Dzięki **nomadic1** mamy wiele ikon dostarczających graficzne wskazówki dla użytkownika. Z czasem (miejmy nadzieję) uda się wprowadzić ikony dla wszystkich elementów występujących w edytorze.

Oprócz tabel edytor posiada całkiem praktyczną <a title="Still my desktop." href="http://wstaw.org/m/2013/08/17/zrzut_ekranu7.png" target="_blank">mapę</a> obszaru zewnętrznego. Podobnie jak tabele jest to dock którym można bez trudu manipulować i dodać więcej niż jeden (choć sens tego jest raczej wątły).

**Aktualnie nie mamy okna renderującego scenę, ale to oczywiście ulegnie zmianie.**

**Scrawl** tymczasem eksperymentował z technikami renderowania LOD terenu, dzięki czemu mogę zaprezentować wam ten o to film wideo. Nie wpadajcie jednak w jakąś przesadną ekstazę: z powodu trudności z obsługą bufora głębi i oczywistego braku obiektów innych niż teren nie ma mowy by coś podobnego pojawiło się w najnowszym OpenMW.