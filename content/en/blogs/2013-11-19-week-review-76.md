{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-19T08:26:01+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-76/"
}
We are doing almost good. Scrawl is done with his touch based magic, and now he is looking toward the ranged magic, even though we don&#8217;t have ranged weapons like bows and arrows. This is may turn into a good decision, after all. It puts emphasis on some aspects that we would have to consider for ranged combat anyway. And not only this, but He also improved npc animations code, cleaned up npc inventory code and even more! Great job scrawl!

Zini is also doing amazing job on OpenCS, working on all that stuff I have mentioned last week and to addition to this he has made excellent progress in new areas as well.

Almost everything goes good now. But almost is not always enough. We still got the bullet blues here. Bluest blues. [https://code.google.com/p/bullet/issues/detail?id=371][1]

This little bastard stops us from solving multiple issues inside OpenMW, and introducing this few missing features. Dear readers: please star this issue! It is important for us!

 [1]: https://code.google.com/p/bullet/issues/detail?id=371 "click me"