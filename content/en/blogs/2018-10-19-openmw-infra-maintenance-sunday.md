{
   "author": "psi29a",
   "categories": [
      "Uncategorized"
   ],
   "date": "2018-10-19T12:00:12+00:00",
   "title": "OpenMW Infra Maintenance Sunday",
   "type": "post",
   "url": "/2018/openmw-infra-maintenance-sunday/"
}
Just a heads up to let everyone know that there might be some &#8216;downtime&#8217; as we transition to new infrastructure. This will happen Sunday around 17:00 GMT and hopefully no one will notice a thing. If they do, they will notice something like: [https://maintenance.openmw.org][1]{.postlink}

Feel free to join us on [IRC][2]{.postlink} or [Discord][3]{.postlink}.

Our longtime comrade, developer and infrastructure admin lgromanowski is stepping down due to time constraints and has asked me (psi29a) and pvdk to take over the reigns of keeping the lights on for OpenMW. Please join us in thanking him for all his hard work keeping things humming along, paying the bills for hosting and donating his time to the project. Thanks Lgro! <img loading="lazy" class="smilies" title="Smile" src="https://i0.wp.com/forum.openmw.org/images/smilies/icon_e_smile.gif?resize=15%2C17&#038;ssl=1" alt=":)" width="15" height="17" data-recalc-dims="1" />

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=5488" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

Update: Transition happened without a hitch. Thank you again Lgro!

 [1]: https://maintenance.openmw.org/
 [2]: https://webchat.freenode.net/?channels=openmw&uio=OT10cnVlde
 [3]: https://discord.gg/wd3eSas