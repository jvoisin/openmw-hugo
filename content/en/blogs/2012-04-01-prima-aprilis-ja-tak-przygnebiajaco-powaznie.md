{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-04-01T10:44:16+00:00",
   "title": "Prima Aprilis, a ja tak przygnębiająco poważnie…",
   "type": "post",
   "url": "/2012/prima-aprilis-ja-tak-przygnebiajaco-powaznie/"
}
OpenMW 0.13.0 został wydany. Co prawda nie w poniedziałek, bo opóźnił nas błąd dotykający OpenMW jedynie na OSX i na dodatek skompilowanego za pomocą clang (i nikt na razie nie wie o co tu dokładnie chodzi) oraz zepsuta binarka dla windows; wciąż jednak uważam, że opóźnienie jest do zaakceptowania.

Oczywiście deweloperzy nie spoczęli na laurach. ;-)

W opisie wydania napisałem, że w launcherze uruchomionym na OSX występuje błąd uniemożliwiający jego działanie o ile ścieżka do folderu zawarta w pliku konfiguracyjnym zawiera spacje. Corristo już to naprawił, podobnie jak automatyczne wykrywanie obecnej instalacji morrowinda na Mac. Dodatkowo udało mu się znaleźć i usunąć bugi związane z dźwiękiem. Oprócz walki z wszechobecnymi błędami, corristo zajęty jest też pracą nad nową funkcją: renderowaniem ścieżek AI.

Hircine, na dobrą sprawę dopiero teraz zaczął pracę nad GUI dla ekwipunku. Idzie to powoli, ale jednak do przodu.

Jhooks1 dokonał przełomu w kwestii fizyki, to jest, nie owijając w bawełnę; nowa implementacja wreszcie działa! Mniej więcej. Rzecz wymaga <a href="http://www.youtube.com/watch?v=0VGQYZwA2Fw&#038;list=PL14E8F94A73754549&#038;index=1&#038;feature=plpp_video" title="regulacji" target="_blank">regulacji</a>, ale mimo wszystko to wielki skok naprzód.

Co prawda minimapa nadal wymaga dopieszczenia, ale na pewno już niedługo będzie całkowicie gotowa, Scrawl może więc spokojnie zająć się także innymi zadaniami. Przykładowo occlusion lub naprawieniem ostatnich niedociągnięć dotyczących renderowaniem wody (np. takimi drobiazgami jak wyświetlanie jej również w interiorach). ;-)

Swick wytrwale pracuje nad stworzeniem konwertera dla pliku morrowind.ini. Można powiedzieć, że zadanie jest już na finiszu.

Zini, nasz umiłowany przywódca i jedyny, prawowity główny deweloper (wazelina mode off) co prawda jest zajęty walką z bombardowaniem pull requestami ale oprócz tego zdołał ukończyć podstawowy mechanizm automatycznego zakładania przedmiotów dla NPCów. Całość jest tak prosta, jak to tylko możliwe: wybiera pierwszy przedmiot który można założyć z ekwipunku, po czym go wykorzystuje. Mimo wszystko nareszcie umożliwi to zobaczenie odzianych npców &#8211; jhooks1 był zawiedziony brakiem widocznych efektów jego pracy nad tym aspektem renderowania i animowania npc.

ACE ostatnio stał się autorem licznych poprawek. Poprawki to dobra rzecz. ;-)

Chris dzielnie pracuje nad dźwiękiem i w końcu stworzył rozwiązanie dla problemu z opcją &#8211;nosound (kretyński wordpress uszczęśliwia mnie na siłę zamieniając dwa minusy na jeden myślnik ale wy, inteligentni, błyskotliwi, mądrzy czytelnicy na pewno wiecie, że chodzi mi o opcję z długą nazwą które to tradycyjnie w systemach Unix i podobnych do niego wpisuje się właśnie w ten sposób). Jego gałąź została niedawno wchłonięta przez main.

Gałąź zawierająca teren również została parę dni temu połączona z main, więc możliwość testowania tej spektakularnej funkcji została bardzo ułatwiona. 

Na koniec informacja z ostatniej chwili: dziś na forum pojawił się deweloper Crystal Scrolls (innego projektu, jednak o celach zbliżonych do naszych) i wyraził chęć przyłączenia się do nas. Oczywiście programista z takim doświadczeniem to dla nas skarb!