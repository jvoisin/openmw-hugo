{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-12-31T08:04:34+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-79/"
}
Welcome, let&#8217;s start with some videos that will show specular mapping, normal mapping in OpenMW. Adding those state-of-art (actually, those are not &#8220;state-of-art&#8221; anymore, but It sounds just cooler this way) techniques makes it look like they are actually… Oh just take a look on those!





As you can see there is a lot that engine can for content quality. Of course, there is a lot that has to be done to apply those effects for every object in game, but after seeing how amazing things were made by community I&#8217;m sure we can make Morrowind looking really great!

As you can also see, magic works just fine. I don&#8217;t have video to show this, but also propylon chambers effects are rendered correctly. Nifflip controller is also implemented, so we can support additional set of content. Everything thanks to scrawl.

Meanwhile, zini works on the saving/load feature. After cleaning up, it seems that He progresses quite well.

Pvdk, finally started working on the installation wizard that will aid you to install Morrowind to use with OpenMW.