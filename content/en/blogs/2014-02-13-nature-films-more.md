{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-02-13T11:09:43+00:00",
   "title": "Nature films, and more.",
   "type": "post",
   "url": "/2014/nature-films-more/"
}
In the past week, Scrawl was able to solve a lot of bugs. He also made significant contributions to the GUI of the game saving and loading feature. As if that were not enough, he also began implementing ranged weapons mechanics.

Meanwhile, Zini made improvements in the area of scripting. There is a new feature in the scripts compiler: &#8220;warning mode&#8221;, which should print a warning if the compiler finds any mistake in the script ─ an undoubtedly useful function.

OpenMW also supports scripts faithful to the original engine ─ which, unfortunately, means that we&#8217;re moving into the area of the absurd. To keep your minds clear and uncontaminated with Bethesda insanity from more than a decade ago, I&#8217;ll skip the gory details.

The latest changes to character animation look really interesting. At the moment of the writing of this post, they aren&#8217;t included in the main branch, but it&#8217;s evident that MrCheko improved many aspects of the gameplay related with animations.

For a long time there weren&#8217;t any funny videos featuring bugs on the blog ─ and yet it&#8217;s precisely what we all love. Anyway, here&#8217;s another day in the life of nature and the inhabitants of the province of Morrowind&#8230;



Here, we can see an extremely difficult to observe phenomenon, the netch&#8217;s mating dance. The excited male demonstrates its efficiency to the female partner. As this happens, the natives are rendered motionless to avoid the aggressive behavior of the animals.



Dark Elves are known for their peculiar, heretical religion. This worship consists of many complex rituals, and an integral and inalienable part of many among them are repeated prostrations.



Here, we see yet another, mysterious ritual whose meaning remains unknown to us.