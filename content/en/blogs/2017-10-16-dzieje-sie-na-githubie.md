{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-10-16T19:38:14+00:00",
   "title": "Dzieje się na GitHubie",
   "type": "post",
   "url": "/2017/dzieje-sie-na-githubie/"
}
Minął kolejny miesiąc od ostatniego newsa. Sporo się wydarzyło od ostatniego czasu, więc nie będziemy zwlekać z nowinkami.

Jak mogliście przeczytać w poprzednim poście, ostatnie wydanie (0.42) przyciągnęła nieco nowych deweloperów do pracy nad silnikiem i edytorem. Zaczynamy dostrzegać rezultaty.

### Oto funkcjonalności, nad którymi trwają obecnie prace:

**Akortunov**, wciąż względnie nowy deweloper, miażdzy bugi jak profesjonalista. Pracował także nad kilkoma nowymi funkcjami. Jedna z nich do przebudowa systemu kradzieży kieszonkowej. Stary (zgodny z oryginałem) system wciąż będzie dostępny dla tych, którzy chcą grać zgodnie z zasadami oryginału. Zmiany Akortunova sprawiają, że silnik nie będzie wyliczał szansy powodzenia na podstawie wartości, ale na podstawie wagi. Będzie także możliwe wkładanie przedmiotów do kieszeni. Funkcja jest jeszcze w fazie rozwoju, więc może ulec jeszcze zmianom.

**Crassel** (crussel187 na githubie), kolejny nowy deweloper, pracował nad implementacją widoku znad ramienia. **[Ma już działający prototyp][1]**, brakuje jedynie kilku detali. Pracuje także nad wsparciem animacji dla formatów natywnych dla naszego silnika graficznego, osgb/osgt. Czas pokaże, czy się uda, ale jeśli tak, to będzie to oznaczało wiele nowych możliwości dla OpenMW.

**Drummyfish**, również nowy deweloper, poprawił shader wody poprzez redukcję pewnych artefaktów (białych linii) widocznych wzdłuż linii brzegowej, oraz dodał **[kręgi na wodzie][2]** jako efekt uderzania kropel deszczu w taflę wody. Ta funkcja jest już dostępna w wydaniach nocnych, w gałęzi głównej. Jakby tego było mało, Drummyfish kontynuuje prace nad doskonaleniem cyfrowej wody, dążąc do przebudowy systemu cząsteczek deszczu i śniegu. Obecnie deszcz w OpenMW działa tak, że deszcz pada tylko nad graczem, jak gdyby z małej chmurki będącej nad jego głową. Jeśli Drummyfish zrobi to, co zamierza, gracz nie będzie już prześladowany przez niby-chmurkę, a opady będą generowane w sposób bardziej realistyczny. Chwilowo jest jednak kilka przeszkód natury technicznej.

**Thunderforge** już nie jest nowy. Gdy dołączył do forum przyszedł jak prorok, przynoszący niewygodne prawdy, które musiały zostać wypowiedziane. Wzywał <s>społeczność do pokuty</s>, by lancher był bardziej **[przyjazny użytkownikom][3]**. Rozpoczął pracę nad tym, i w niedalekiej przyszłości będzie w nim można zobaczyć linki kierujące do sklepów, gdzie można nabyć oryginalną grę Morrowind, gdybyśmy czasem jej nie mieli. Sposób i konieczność realizacji tej funkcjonalności są wciąż przedmiotem debat tak więc upłynie jeszcze trochę czasu, nim wejdzie ujrzy on światło dzienne.

**AnyOldName3** również nie jest nowy. Znacie go z ostatniego posta. Wciąż pracuje nad implementacją cieni, która wciąż nieco kuleje, ale jest coraz lepiej. Jak dotąd, wpływ na wydajność jest znikomy.

**[Oto film][4]** pokazujący nwoe cienie w mniej dziwacznej sytuacji. Przez „mniej dziwaczną” sytuację należy rozumieć taką, w której sprawują się nadzwyczaj dobrze. W innych przypadkach wyglądają nieco dziwnie, ale dobrze idzie, prawda?

**Chris/kcat** to weteran. Scrawl dał nam fundament w postaci funkcji _odległego lądu_, natomiast Chris/kcat wznosi na nim budynek. Planuje dodać funkcje pozwalające łatwiej modyfikować dystans mgły i sprawić, by ta wyglądała lepiej z odległym lądem. Jeśli się uda, to zmiany te mają szansę znaleźć się w następnym wydaniu.

Jak wspomniano w poprzednim poście, **Aesylwinn** wrócił do pracy nad CS-em. Połączył siły z **PlutonicOverkill**em, by pracować nad możliwością pełnej edycji siatki terenu. Cel nie został zrealizowany, jednakże ostatnio jego praca trafiła do gałęzi głównej, a jest to możliwość [**edycji tekstur terenu**][5]. Funkcja ta będzie dostępna w OpenMW 0.43!

Ostatnia sprawa. Wszechmocnego **scrawl**a tak zirytował stary system okienek (tych w grze), że kompletnie go przebudował, wzbogacając go o nowe funkcje. Jakie? Na przykład możliwość obsługi menu za pomocą samej klawiatury. Ta-dam! Można to przetestować w wydaniach nocnych lub zaczekać na OpenMW 0.43.

### Chcesz się przyczynić dla sprawy?

Mamy od niedawna nieco więcej deweloperów, ale nie zaszkodzi mieć więcej. Jeśli uważasz, że Twoje doświadczenie programistyczne może nam pomóc, ale nie wiesz, jak zacząć, zacznij lekturę od tej dokumentacji:

**[Developer reference][6]**

Może zainteresują Cię strony poświęcone **[architekturze silnika][7]**, [**GUI (MyGUI)**][8], albo [**silnikowi renderującemu (OpenSceneGraph)**][9].

A jeśli nie jesteś programistą? Zawsze przyda się pomoc w testowaniu. Uruchom grę i [**zgłaszaj błędy**][10].

Potrzebna jest także pomoc w sortowaniu zgłoszeń. Przydatna jest tu znajomość organizacji projektu, a więc trzeba mieć z nim pewne obycie. **[Opis tej posady][11]**.

To tyle na dzisiaj. Jeśli wszystko pójdzie dobrze, nasz przezacny edytor wideo powróci za tydzień, lub coś koło tego. To znaczy, że już niedługo wydanie 0.43. Zostańcie z nami!

##### Zapraszamy do komentowania [tutaj][12].

#####

 [1]: https://www.youtube.com/watch?v=_7xBR2rQL7s
 [2]: https://www.youtube.com/watch?v=vJMNW58Yhy0
 [3]: https://forum.openmw.org/viewtopic.php?f=20&t=4372
 [4]: https://www.youtube.com/watch?v=aDr7o-Ft4R4
 [5]: https://github.com/OpenMW/openmw/pull/1427
 [6]: https://wiki.openmw.org/index.php?title=Developer_Reference
 [7]: https://wiki.openmw.org/index.php?title=Architecture
 [8]: https://wiki.openmw.org/index.php?title=GUI_Architecture
 [9]: https://wiki.openmw.org/index.php?title=Rendering_Architecture
 [10]: https://wiki.openmw.org/index.php?title=Bug_Reporting_Guidelines
 [11]: https://wiki.openmw.org/index.php?title=Bug_Triaging_Guidelines
 [12]: https://forum.openmw.org/viewtopic.php?f=38&t=4620