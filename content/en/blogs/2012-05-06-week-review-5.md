{
   "author": "jvoisin",
   "date": "2012-05-06T17:29:35+00:00",
   "tags": [
      "Gus",
      "inventory",
      "jhooks",
      "physics",
      "scripts",
      "spells"
   ],
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-5/"
}
Hello everyone! No fancies news for this week, but a massive background improvement (the new physics system), and a lot of misc things.

**Scrawl** is a machine! He&#8217;s working on multiple fronts, and the results so far are amazing:

  * Improvements of the journal rendering
  * Single quotes are now working
  * Opening/taking/closing books/scrolls now work
  * Movement is no longer frame-rate dependent
  * You can now talk with creatures
  * More spell backend

**jhooks** managed to get terrain collisions working! You can test this feature by typing _tcl_ or _togglecollisions_ in the console. The new physics system will be part of our next release: better, faster, easier!

**gus** is still working on item drag&#8217;n&#8217;dropping from the inventory.

Ho, by the way, if you know some [Qt][1], we are looking for someone to develop the _OpenMW Editor_. Don&#8217;t be shy, apply!

 [1]: https://qt.nokia.com/