{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-03-06T18:00:00+00:00",
   "title": "Parę nowości",
   "type": "post",
   "url": "/2016/pare-nowosci-2/"
}
Witajcie! Fajnie, że wpadliście. Trochę czasu już zleciało od ostatniego wpisu. Usiądźcie, mamy dla Was coś naprawdę fajnego. Wiecie pewnie, że WeirdSexy był zapracowany i tak dalej, i że kilka z naszych ostatnich wydań nie doczekało się filmików? Więc&#8230; mamy dla Was niespodziankę. Nie, WeirdSexy jeszcze nie wrócił, ale mamy wideo. Jest retrospekcja jednego z poprzednich wydań. Tak więc pomimo, że WeirdSexy wciąż nie ma, zasiądźcie i pozwólcie Atahualpie, by przedstawił Wam poprzednie wydania. Oto pierwszy film z serii „Zapomniane wydania” (_The Forgotten Releases_):



To było dobrze spędzone szesnaście minut. A mówiąc o dobrze spędzonym czasie&#8230; Scrawl też ma niespodziankę! Ta funkcja już była w Ogre3D, a teraz &#8211; po pewnym czasie nieobecności &#8211; jest spowrorem: normalne mapy! Scrawl wciąż miał gdzieś parę starych map PeterBitta, więc popróbował tego i owego, i oto, co wyszło:

[<img src="https://i0.wp.com/i.imgur.com/PBMBaSu.jpg?w=300" alt="" data-recalc-dims="1" />][1]

Subtelne, ale przepiękne. Jako bonus, OpenMW może teraz automatycznie używać normalnych map, o ile odpowiadają schematowi nazewnictwa. Scrawl wyjaśnia to tak: „Jest to już zaimplementowane. Na przykład, jako koloru tekstury foo.dds, gra użyje automatycznie foo_n.dds jako normalnej mapy (jeśli istnieje). Korzyść z tego jest taka, że nie trzeba modyfikować (rozmieszczać) wszystkich plików .nif i plików meshy”.

Ale normalne mapy to nie wszystko. OpenMW wspiera też teraz mapy specular. Oba rodzaje map mogą być wykorzystywane wraz z shaderem terenu.

Na przykład lysol (który pracował nad reteksturowaniem Warcrafta III i Rome: Total War) wykorzystał swoje doświadczenie w pracy i utworzył kilka nowych tekstur dla Vivek, wykorzystując zarówno mapy normalne, jak i mapy typu specular, trzymając się możliwie najbliżej oryginału:

[<img src="https://i0.wp.com/i.imgur.com/xTyD5oV.jpg?h=200" data-recalc-dims="1" />][2] [<img src="https://i0.wp.com/i.imgur.com/fmdvmQg.jpg?h=200" data-recalc-dims="1" />][3]

Więcej informacji na ten temat można znaleźć na [stronie wiki o modowaniu tekstur][4]. Może to być dobry punkt wyjścia dla wszystkich myślących o tworzeniu modów graficznych dla Morrowinda.

Uczyniono także znaczący postęp od strony OpenMW-CS. Po pierwsze: znaczniki komórek (_cell markers_) i krawędzie komórek (_cell borders_) są teraz widoczne. Po drugie, można przeciągać rzeczy z tabeli obiektów do trójwymiarowego podglądu komórki metodą drag-and-drop. Jeśli zatem zrobisz jakiś fajny przedmiot, i chcesz go umiejscowić gdzieś w świecie gry, może to zrobić bardzo prosto. I jeszcze po trzecie &#8211; edycja instancji wewnątrz sceny. Super! Nie możemy się doczekać, by zobaczyć, co Zini z tym zrobi.

A jak już mówimy o Zinim&#8230; Zespól prowadzący kanał Morrowind Modding Showcases zrobił z nim wywiad. Zapraszam do podziwiania jego germańskiej chwały i obejrzenia poniższego wideo:



Zapraszamy więc ponownie, gdy będziemy mieli kolejne fajne rzeczy do pokazania!

##### Zapraszamy do komentowania [tutaj][5].

 [1]: https://i0.wp.com/i.imgur.com/PBMBaSu.jpg
 [2]: https://i0.wp.com/i.imgur.com/xTyD5oV.jpg
 [3]: https://i0.wp.com/i.imgur.com/fmdvmQg.jpg
 [4]: https://wiki.openmw.org/index.php?title=TextureModding
 [5]: https://forum.openmw.org/viewtopic.php?f=38&t=3413