{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-03T17:22:16+00:00",
   "openid_comments": [
      "a:1:{i:0;i:436;}"
   ],
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-74/"
}
It seems that we start to properly gain momentum again.

First of lgro fixed the critical bug that used to trigger instant crash with Polish locale of Morrowind. Also, It seems that lgro spotted another crash bug. Good hunting!

Secondly gus finished implementing LOS (line of sight) and now He is free to return to his combat AI task. At the moment it seems that he finished the major bulk of the task and only the few remaining issues remain to be solved.

Scrawl created a new interesting widget for OpenCS: a OGRE3D renderer. It should sound exciting, we finally will be able to have preview inside OpenCS just like vanilla CS has. It will boost the awesome level of OpenCS to the highest highs. There is no chance for it in the next version of OpenMW, but without doubt it will be ready to use in next month.

And as usual, zini with his inhuman, mechanic precision continues to work on the other aspects of the editor. At the moment he is correcting tables already implemented, as well as continues to work on dialogue table.

Graffy is still working on the new content selector that is used in both OpenCS and the launcher.