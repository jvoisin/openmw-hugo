{
   "author": "sir_herrbatka",
   "date": "2013-02-25T16:10:56+00:00",
   "title": "Rzeź bugów.",
   "type": "post",
   "url": "/2013/rzez-bugow/"
}
Co nowego? W sumie nic ciekawego, choć byliśmy świadkami holocaustu bugów. Celowo używam tu tego słowa, nie tylko po to by oddać skalę, ale także po to by uatrakcyjnić w sumie dość nudną listę poprawek.

• TCL ─ działa w końcu tak jak powinno. 

• Podgląd postaci ─ naprawiony, działa wraz ze zmianami wprowadzonymi przez Chrisa.

• Pozycje świateł ─ naprawione.

• Obsługa dużych znaków ─ kilka problemów rozwiązanych.

• Odbicie mgły na powierzchni wody ─ działa.

• Układ książek ─ poprawiony.

• Udoskonalona obsługa tłumaczeń ─ ładowane są wszystkie pliki, nie tylko te dla pierwszego pluginu.

I tak dalej. Poprawek jest więcej, po prostu nie mam cierpliwości by wymienić wszystkie. Jak widzicie daleko jest od tego by można uznać projekt OpenMW za martwy, ale postawcie się na moim miejscu: jak właściwie mam o tym pisać? Zupełnie nic rajcującego.

Scrawl chyba to przeczuwa bo próbuje wmówić mi, że pracuje nad czymś fajnym. Ponieważ jednak nie piśnie nawet słowa tłumaczącego naturę swoich wysiłków sądzę, że mnie po prostu trolluje.

Gus chciałby wrócić do pracy nad renderowaniem broni, ale trudno zrobić to poprawnie nim zostaną zaimplementowane warstwy animacji.

Zini i graffy dalej katują kod edytora, podobnie jak blunted2night dziennik.

Chris dalej pracuje nad animacjami. Niestety okazuje się, że movement solver jest jednak daleki od ideału, i możliwy jest scenariusz kolejnej zamiany, tym razem na kontroler pochodzący z bullet. Oczywiście kontroler bullet także nie jest szczególnie cudowny, a co gorsza chris musi wpierw opanować tajniki tej biblioteki. Niestety na dzień dzisiejszy nie jestem w stanie powiedzieć ile czasu może to zająć.