{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-08-26T18:58:11+00:00",
   "tags": [
      0.17,
      "editor",
      "Eli2",
      "example suite",
      "greendogo",
      "jenkins",
      "lgromanowski",
      "release",
      "trees",
      "vurt",
      "WeirdSexy"
   ],
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-15/"
}
The 0.17.0 release is set for next week, as we&#8217;re just waiting on just a few builds. Our superstar **WeirdSexy** gifted us with another excellent demonstration video. We&#8217;d be lost without that smooth voice&#8230; but our project is an international effort with **K1ll****&#8216;s** a juicy German accent, **ap0** has both French lover and English down, while there is the true Norwegian Black Metal growl of **Rebel-Rider**&#8230;

**Eli2** is making good progress on the editor. At some point the code will be mature enough for others to join the effort. Let me reiterate: we need the editor in order to add desirable post-1.0.0 features.

As for the Example Suite, **greendogo** had contacted many talented Morrowind modders asking to use their works. Westly has given us permission to use his custom races, vurt&#8217;s allowing us to use his nice trees, and in general we&#8217;re gathering a very nice set of art assets. If you are a Morrowind modder that has work that you are willing to let us use, please let us know on our forum. These assets may be very useful for future Total Conversion projects that don&#8217;t want to have anything in common with Morrowind (including actually owning the game). The Example Suite&#8217;s story thus far is deserving of a &#8230; **B** or perhaps even **C** grade.

**Lgro** is setting up a jenkins server. It will allow us to have automatic nightly builds of OpenMW, and allow you to test the newest features without compiling on your own.

I would love to write about the future plans but this has to wait until new version is released. That&#8217;s any moment so be patient like me!