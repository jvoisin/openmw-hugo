{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-02-16T11:43:54+00:00",
   "title": "scrawl is back and more",
   "type": "post",
   "url": "/2017/scrawl/"
}
It looks like the good old times are back, because according to [github][1] we have been getting several commits from scrawl every day for the past two weeks or so. Looking at the optimizations he has been making, we can expect a boost in fps in the next version of OpenMW.

One thing sticks out for me &#8211; small object culling is now configurable in settings.cfg. It was initially hardcoded that objects smaller than 2&#215;2 pixels on your screen will be culled, but now you can decide for yourself how many pixels you want to cull in exchange for better fps. If paired with a simplified land mesh, which scrawl was working on before he went on a break, we might just have distant land on the horizon.

In other news, Allofich has fixed a [bug][2] that had always irked me. If you were to go for a quick swim and ignored the fish, combat music would constantly be playing around that area even when you were on dry land. Now, those of us who can&#8217;t be bothered with the small fish can enjoy the main theme of Morrowind again.

Finally, I chanced upon some really interesting stuff on the [tes3mp project][3]. What they have essentially done is set up a server that saves the changes that visiting players imprint on the world. Player interactions are not just limited to dropping items, but also adding objects through the console. The tes3mp developers have made some screenshots of the results and I&#8217;ll leave the link [here][4] for you to enjoy.

[Want to comment?][5]

 [1]: https://github.com/OpenMW/openmw/commits/master
 [2]: https://bugs.openmw.org/issues/2678
 [3]: https://www.reddit.com/r/tes3mp/
 [4]: https://imgur.com/a/6aTIl
 [5]: https://forum.openmw.org/viewtopic.php?f=38&t=4134