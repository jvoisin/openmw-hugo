{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-05-13T18:13:59+00:00",
   "title": "Tak, to jest post – cotygodnik",
   "type": "post",
   "url": "/2014/tak-jest-post-cotygodnik/"
}
Nekochan odzywa się na forum, więc chyba nie mam już żadnej wymówki – po prostu muszę napisać cotygodnik!

Podobnie jak musimy w końcu naprawić błędy: dziesiątki mniejszych lub większych, a okazjonalnie wręcz kolosalnych bugów wciąż nas prześladują. To naturalny stan rzeczy: błędy są wprowadzane przez programistów, gdy ci starają się dodać nową funkcję do programu, a my dodawaliśmy naprawdę wiele funkcji. Jak powtarzam od kilku już tygodni, OpenMW jest niemal kompletne i, na szczęście, nie dotarliśmy jeszcze do etapu gdy próba rozwiązania błędu doprowadza do stworzenia dwóch nowych – tak więc wyznacznikiem naszej pracy z wolna staje się liczba rozwiązanych problemów. Nie jest to szczególnie ekscytujące, ale bez tego nie ma mowy o wydaniu 1.0.0. Lub, jeśli nie lubicie wybiegać aż tak daleko w przyszłość: 0.30.0.

Wersja 0.30.0 wciąż nie jest gotowa, ale jesteśmy już blisko. K1ll dodał wiele poprawek usprawniających nasze paczki dla linuxa, scrawl ustanawia życiówki w kategorii ilości bugfixów do OpenMW. Pozostali deweloperzy (zauważcie, że nie używam korpo-pisowni przez literę &#8220;v&#8221;) również nie zasypiają gruszek w popiele. To był naprawdę aktywny tydzień, jednak jeśli chcemy wskazać na coś poza poprawkami nie mamy wyboru: jest tylko zini: dokończył on wzmiankowane w zeszłym tygodniu; prace nad udoskonaloną obsługą ładowania plików gry do edytora.

I na dobrą sprawę to tyle. Czekajcie cierpliwie na wydanie 0.30.0 – jest już bliskie.