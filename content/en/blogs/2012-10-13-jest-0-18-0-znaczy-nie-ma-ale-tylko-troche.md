{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-10-13T13:09:03+00:00",
   "title": "Jest 0.18.0! To znaczy nie ma, ale tylko trochę… ;-)",
   "type": "post",
   "url": "/2012/jest-0-18-0-znaczy-nie-ma-ale-tylko-troche/"
}
I tak o to minął wrzesień ─ sam nawet nie wiem kiedy. Może ktoś go ukradł i zakopał? 

Nie, oczywiście, że nie ─ w końcu udało się ukończyć 0.18.0: wiele wnoszące, pachnące jeszcze nowością wydanie, które niezbicie dowodzi istnienie września. 

Możesz nie wierzyć ─ masz ku temu podstawy, choćby negując fakt ukończenia 0.18.0; bo wszak nie ma ogłoszenia na blogu, a i na google code brak binarek. Ale w istocie 0.18.0 jest ukończone, jedynie nie wydane. 

Problem polega na tym, że dostęp do google code ma jedynie raevol który to akurat gdzieś wyjechał na parę dni i jest nieosiągalny. Oczywiście może być to jedynie element spisku&#8230;

Wracając jednak do wydarzeń bieżących i przypuszczalnie najbardziej interesujących: w końcu mamy własny styl forum!

Dawno, dawno temu, nad stylem forum pracował Necrod (ten sam który jest autorem logo widocznego u góry strony) ale nie osiągnął zbyt wiele, a nasze forum wciąż miało wygląd co najmniej kontrowersyjny. Sytuację rozwiązał jedd który wypolerował całość na wysoki połysk.

Wejście na forum jest teraz o wiele przyjemniejsze: ciepłe kolory, zaokrąglone rogi, świetna integracja ze stroną główną&#8230; zresztą polecam odwiedzić forum i przekonać się na własne oczy.

Oprócz tego Lgro zainstalował nowy i, o dziwo, skuteczny plugin anty-spamowy. Do tej pory codziennie musieliśmy usuwać boty spamujące ofertami powiększania penisa oraz inną klasyką gatunku&#8230; ale nie tylko! Okazuje się, że do zapaskudzania forów internetowych posuwają się nawet biura podróży.

Efekt działania pluginu można określić jednym słowem: błogość.

Wiem, wiem&#8230; „Talk is cheap: show me the code!”. Właśnie przechodzę do sekcji o programistycznych wyczynach zespołu.

Scrawl dodał podnoszenie poziomu umiejętności po przeczytaniu książki, a ponadto zaczął pracować nad odtwarzaniem filmów obecnych w grze ─ choć nie jest ich wiele to jednak pozwalają lepiej wczuć się w bohatera gry. Poza tym, jak zwykle naprawianie błędów, jak choćby tego który uniemożliwiał uaktualnienie globalnej mapy jeśli ta była ustawiona jako widoczna również w trakcie gry.

Gus zaś zajął się szybką podróżą znaną z morrowinda: łaziki, łodzie, gondole, teleportacja w gildii magów; wszystkie działają na tej samej zasadzie. Część z was może wskazać na teleportację jako wyjątek: jako jedyna odbywa się natychmiast i niezależnie od dystansu jaki przyjdzie przebyć kosztuje zawsze tyle samo. W rzeczywistości silnik morrowinda nie jest w stanie określić odległości dzielącej dwa wnętrza i dla tego w takim przypadku zakłada minimalną cenę bazową oraz ustala czas podróży na zero godzin. Oczywiście jedynymi usługodawcami szybkiej podróży rezydującymi w pomieszczeniach są magowie w Gildiach, można więc odnieść wrażenie, że morrowind jest bardziej skomplikowany niż w rzeczywistości.

drpaneas to nowy programista który zajął się swoim pierwszym zadaniem: implementuje „failed action”. Zadanie nie jest bardzo trudne, ani też wyjątkowo duże ale mimo to ważne i wymagane przez wiele innych, wciąż otwartych zadań.

Mark76, osoba która swojego czasu tworzyła projekt alternatywny względem OpenMW (Crystal Scrolls) powróciła na nasze fora. Kilka miesięcy temu Mark76 przyłączył się do naszego projektu i zajął się ambitnym zadaniem wspierania architektury pluginów oraz ładowania wielu esm jednocześnie. Co prawda prototyp został ukończony ale dowiódł jedynie na jak wiele sposobów nie jesteśmy gotowi do wprowadzenia tej funkcji, i do dziś wciąż nie posunęliśmy się szczególnie daleko. Zini sądzi, że konieczne są zmiany, rozbicie tego kolosalnego wyzwania na mniejsze, dające się przełknąć kawałki. 

Oprócz tego, Zini zajmuje się pracą nad nową implementacją alchemii. Przepisanie było planowana już od dawna, faktycznie od samego początku, a tym samym scrawl nie przejmował się szczegółowym projektowaniem swojego rozwiązania. 

Greye ciągnie stare, nudne, ale ważne jak jasna cholera zadanie „record saving”. Ta funkcja jest kluczowa zarówno dla samej gry jak również dla edytora.