{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-09-05T19:13:09+00:00",
   "title": "Grosz do grosza…",
   "type": "post",
   "url": "/2014/grosz-grosza/"
}
Spodziewamy się wersji 0.32.0. To oznacza, że dla każdej platformy są budowane pakiety kandydujące (release candidate). Jeśli w takim kandydacie nie zostaną znalezione żadne istotne błędy, to stanie się on wersją stabilną.

Co się wydarzyło w tych ostatnich chwilach przed nowym wydaniem? Najistotniejszą informacją jest fakt, że zaklęcie Światło już działa, tworząc oświetlenie tak, jak należy. Może nie jest to porywające samo w sobie, ale warto o tym wspomnieć, bo to ostatnie zaklęcie, jakie wymagało implementacji! Tak jest, wszystkie zaklęcia już działają! To był drugi co do „starości” problem (numer 47, najnowszy błąd, żeby dać porównanie, ma numer 1857). Jest to kolejny wielki krok w rozwoju OpenMW.

Nie zostało wiele do zrobienia. Jeśli z listy funkcji do dodania odfiltruje się wszystko, co ma być dodane po wersji 1.0, to można zobaczyć, że zostało mniej niż trzydzieści funkcji do zaimplementowania ([link][1]).

Oczywiście to wszystko nie znaczy, że wersja 1.0 pojawi się teraz, zaraz. Jest ponad 200 błędów do przepracowania. Jeśli widzieliście nasze ostatnie wideo [link], to pewnie już wiecie, że nasza drużyna składa się z ludzi, którzy błędy gniotą na śniadanie. (Okulo jest przekonany, że Scrawl jest tak naprawdę robotem &#8211; [zdjęcie][2]).

Kiedy pojawi się wersja 0.32.0? Już niedługo. Drużyna jest już w ostatnim stadium wypuszczania nowej wersji, więc wytrzymajcie. Niedługo będziecie czarować jak Merlin.

Zapraszamy do komentowania [tutaj][3]!

 [1]: https://bugs.openmw.org/projects/openmw/issues?c[]=project&c[]=tracker&c[]=status&c[]=priority&c[]=subject&c[]=assigned_to&c[]=updated_on&f[]=status_id&f[]=tracker_id&f[]=fixed_version_id&f[]=subject&f[]=&group_by=&op[fixed_version_id]=!&op[status_id]=o&op[subject]=!~&op[tracker_id]=%3D&per_page=50&set_filter=1&utf8=%E2%9C%93&v[fixed_version_id][]=3&v[subject][]=editor&v[tracker_id][]=2
 [2]: http://www.impactlab.net/wp-content/uploads/2011/03/robot.jpg
 [3]: https://forum.openmw.org/viewtopic.php?f=38&t=2393