{
   "author": "jvoisin",
   "categories": [
      "Uncategorized"
   ],
   "date": "2012-05-03T05:33:58+00:00",
   "tags": [
      "morrowind",
      "overhaul",
      "rebuilt",
      "tamriel"
   ],
   "title": "Happy Birthday Morrowind!",
   "type": "post",
   "url": "/2012/happy-birthday-morrowind/"
}
Morrowind is 10 years old today! What are you going to do to celebrate this?

You can:

  * Try [Morrowind Overhaul][1], which is a stunning compilation of many graphical mods. Your Morrowind will never had looked so shiny and actual. Check the [screenshots][2] if you don&#8217;t believe me!
  * Try [Tamriel Rebuilt][3], which is older than MOrrowind itself, and that aims to offer the **whole** Morrowind continent (and not only the central island).
  * Try our lastest release of OpenMW: the [0.14.0][4] one, which brings up many exciting features.
  * Listen to the [amazing][5] original soundtrack
  * [Educate][6] yourself about Morrowind&#8217;s background

&nbsp;

 [1]: http://morrowindoverhaul.rpgitalia.net/
 [2]: http://morrowindoverhaul.rpgitalia.net/screenshots?show=gallery
 [3]: http://tamriel-rebuilt.org
 [4]: http://openmw.org/2012/04/30/openmw-0-14-0-released/
 [5]: http://kotaku.com/5905585/morrowind-really-did-have-the-best-elder-scrolls-music-didnt-it
 [6]: http://www.uesp.net/wiki/Morrowind:Morrowind