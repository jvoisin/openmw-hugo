{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-10-21T20:33:41+00:00",
   "title": "No i po co te tytuły…?",
   "type": "post",
   "url": "/2012/po-te-tytuly/"
}
Witajcie! 

Nasz wspaniały edytor o przygnębiająco przewidywalnej i mam (złudną) nadzieję, że tymczasowej nazwie OpenCS wciąż pomaleńku się rozwija. Aktualnie Eli2 koncentruje się na fundamentach: ładowaniu i zapisywaniu projektów. Oczywiście ważne jest by zaprojektować podstawowe elementy jak najlepiej, w szczególności dla tego, że co prawda na razie niektórzy spośród Was mogą odnieść wrażenie niemrawego postępu; ale w rzeczywistości edytor może wzbogacić się w funkcje szybko ─ o ile rzecz jasna nie będzie się to odbywać drogą budowy na prowizorce.

Scrawl, zaznaczcie te epokowe wydarzenie w kalendarzu, nie pracuje aktualnie nad shaderami, ani niczym związanym z grafiką. Zamiast tego przyłączył się do Ziniego i jego prac nad nową, w zamierzeniu już poprawną implementacją alchemii. Kwalifikacje scrawla są trudne do podważenia: to właśnie ten programista jest autorem pierwotnej implementacji, a ponadto w ramach swojego udziału w nasz wspólny projekt stał się prawdziwym znawcą MyGUI ─ co jest o tyle istotne, że nowa implementacja alchemii wciąż jest częściowo pozbawiona interfejsu graficznego, a poza tym posiada błędy które należy poprawić.

Z ciekawostek: kompilacja OpenMW na komputerze scrawla zajmuje w przybliżeniu dwie minuty. ;-)

Zini oprócz tego przejął zadanie „Death”. Spokojnie: jeśli z waszego komputera zaczyna się wydobywać trupi fetor, a powietrze przeszywa lodowaty powiew zimna to wiedzcie, że może i coś się dzieje ale nie z naszej winy ─ te funkcje są w planach dla dopiero OpenMW 6.6.6. Z pewnością WeirdSexy nie może się doczekać możliwości odkrycia konwencji thrillera, filmu kryminalnego, a może także i horroru ─ wiadomo, że bez trupów w takich pozycjach się nie obejdzie.

Gdyby tak jeszcze animacje zaczęły naprawdę działać&#8230; Chris pracuje nad tym niekoniecznie banalnym zadaniem. Co prawda OpenMW jest w stanie odtwarzać animacje, ale jednak brak jest jakiejkolwiek integracji z fizyką. Oczywiście oznacza to tyle, że bohater gry nie chodzi, a sunie po ziemi ─ i powinien się cieszyć bo pozostałe postacie w grze nie potrafią nawet i tego. 

Lgro który żadnego bota się nie boi i serwerami włada zajął się tymczasem książkami. Otóż jak do tej pory strony ksiąg znajdywanych w grze nie były wyświetlane poprawnie, zwłaszcza jeśli zawierały obrazki. Zadanie pozornie nie stanowiło wyzwania ─ jednak zaskoczyło swoją złożonością i tak o to możemy powiedzieć tylko tyle, że pierwsze niepowodzenie jest już za nami, a teraz bogatsi o nowe doświadczenie pokonamy i tą przeszkodzę.

Kolejną skomplikowaną historię pełną zaskakujących odkryć przedstawia stan prac nad optymalizacją szybkości wczytywania nowych komórek w grze. U niektórych testerów problem jest wyraźnie widoczny, u innych jednak nie ─ więc dopiero teraz zini postanowił wprowadzić zmiany mające na celu usprawnienie procesu. Niestety zmiany nie przyniosły oczekiwanych rezultatów. Aktualnie sytuacja jest więc bardzo niejasna, a dokładny plan działań wciąż dopiero musi powstać.