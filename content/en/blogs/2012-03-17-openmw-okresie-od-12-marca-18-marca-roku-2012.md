{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-03-17T18:31:27+00:00",
   "title": "OpenMW w okresie od 12 marca do 18 marca roku 2012",
   "type": "post",
   "url": "/2012/openmw-okresie-od-12-marca-18-marca-roku-2012/"
}
Witajcie wszyscy, mam nadzieję że za waszymi oknami jasne, wiosenne słońce rozświetla czyste, błękitne niebo równie pięknie jak u mnie.

Ale przecież nie będę pisał o pogodzie&#8230; ;-)

Zacznijmy od tego, że submoduły zostały wchłonięte przez główną gałąź. Po tym gdy werdanith zwrócił uwagę, że nie ma z nich ani krzty pożytku nagle okazało się, że każdy (nie wyłączając ziniego) dosłowni marzy o pozbyciu się ich. Corristo skorzystał z szansy na zostanie przodownikiem pracy i bohaterem klasy programującej ─ zmergował submoduły ku radości nas wszystkich.

Żeby było nawet śmieszniej: parę dni temu dołączył do nas Chris (nick KittyCat na kanale IRC) i jak sam przyznał, decyzję podjął po tym gdy dowiedział się, że skończyliśmy z submodułami&#8230;

Chris kiedyś co prawda był deweloperem OpenMW ale było to już naprawdę dawno temu ─ tak dawno, że już chyba nawet najstarsi górale zdążyli zapomnieć. Nowemu członkowi zespołu nie odpowiada stan w jakim jest nasz system dźwięku i doprawdy, trudno mu się dziwić. Docelowo, nowy projekt zakłada przeniesienie całości obsługi audio do mwsoundmanager (czyli do miejsca najbardziej oczywistego). To duże zadanie.

scrawl (nalegał by pisać jego nick z małej litery) nanosi kolejne poprawki na kod renderujący niebo (przypominam, że niebo pojawi się w 0.13.0) oraz podjął zupełnie nowe zadanie: minimapa. Minimapa działa i potrafi wyświetlić renderowane przez silnik gry obiekty ale ponieważ rozwój ma miejsce w nowo utworzonej gałęzi jeszcze nie uświadczymy terenu. Ponadto wciąż nie jest wyświetlana strzałka oznaczające pozycję bohatera i możliwe, że dodanie jej będzie trudne, a może nawet niewykonywalne przed wydaniem nowej wersji myGUI. Oczywiście gdy sprawa się wyjaśni nie omieszkam was poinformować, a póki co o to [screenshot][1] :-) 

Zini ukończył backend ekwipunku. Tylko backend więc nie ma mowy o tym, by npc byli w stanie założyć na siebie odzienie którym dysponują, nie wspominając już nawet o GUI dla gracza&#8230; i dla tego możliwości przetestowania nowego kodu są mocno ograniczone. Na razie widoczną i działającą funkcją jest wykrywanie klasy pancerza, wykorzystywane przez nową funkcję odtwarzającą dźwięki podnoszenia przedmiotów.

W zeszłym tygodniu pisałem, że miał się tym zająć werdanith; a dziś mogę napisać, że skończył. Od teraz podniesienie przedmiotu skutkuje nie tylko zniknięciem go z ekranu, ale także słyszalnym dźwiękiem. Dodatkowo nasz dzielny programista naprawił regresję z 0.12.0 która uniemożliwiała usłyszenie dźwięków 3d.

Corristo wciąż kontynuuje pracę nad wyświetlaniem ścieżek AI. Funkcja konieczna do debuggowania przyszłego AI i działania &#8220;nawet bardziej przyszłego&#8221; edytora. 

Jhooks1 nadal poprawia fizykę&#8230;

&#8230; gus zaś nadal walczy z dialogami. Do zaimplementowania wciąż czekają filtry.

PPA dla ubuntu jest gotowe. Pozwolicie że zacytuję post BrotherBrick:

> Quote:  
> With the help of K1ll we now have a few PPAs that people can use.
> 
> I&#8217;m proud to say that our &#8216;official&#8217; Ubuntu packages can be found here:  
> [https://launchpad.net/~openmw/+archive/openmw][2]  
> ^&#8211; includes 32 and 64bit packages from Lucid to Precise that is statically compiled and does not need users to worry about libbullet nor libogre.
> 
> For those wishing to use static (*.a) libraries:  
> [https://launchpad.net/~openmw/+archive/deps][3]
> 
> For those wishing to use dynamic (*.so) libraries instead:  
> [https://launchpad.net/~openmw/+archive/build][4]

Dzięki pomocy artorius mamy także 32 i 64 bitowe rpmy dla [Fedory][5].

Użytkownicy pozostałych dystrybucji nie mają powodów do zmartwienia bo na stronie, w sekcji &#8220;Do Ściągnięcia&#8221; znajdą archiwa działające (przynajmniej w teorii) na każdej dystrybucji i to bez konieczności instalowania Ogre3d czy też bullet. Osobiście testowałem 64 bitową binarkę na Debian Stable i działa bez zająknięcia.

 [1]: http://dl.dropbox.com/u/2899105/minimap.png
 [2]: https://launchpad.net/%7Eopenmw/+archive/openmw
 [3]: https://launchpad.net/%7Eopenmw/+archive/deps
 [4]: https://launchpad.net/%7Eopenmw/+archive/build
 [5]: http://openmw.org/forum/viewtopic.php?f=8&t=599