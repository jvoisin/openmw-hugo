{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-10-02T15:04:11+00:00",
   "tags": [
      "animation",
      "BrotherBrick",
      "bullet",
      "Gus",
      "jhooks",
      "K1ll",
      "physics",
      "Zini"
   ],
   "title": "We are here.",
   "type": "post",
   "url": "/2011/we-are-here/"
}
&#8230; and we have *.deb packages! Thank you BrotherBrick! At the moment I can&#8217;t see them under downloads section but this is probably a matter of time. This packages where build for 32 and 64 bits ubuntu.

And jhooks found the major problem behind poor performance of animations? Animations runs, faster reaching the openmw standard (read as &#8220;runs still slow but not so terrible slow as before&#8221;)

Zini finished both 0.12.0 tasks that was reserved for him. Doing 200% while other developers slips away makes him almost heroic. Well, now zini is doing code maintaining stuff in the engine class.

Bullet performance. Although k1ll problems where caused by bug in the bullet itself GUS found idea to increase physics performance? I&#8217;m not sure if we will get better FPS since this is probably not a bottleneck now. At least It may help in future to speed rendering up.