{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-26T17:51:00+00:00",
   "title": "TurboRozwój",
   "type": "post",
   "url": "/2013/turborozwoj/"
}
Nie zagłębiając się w zbędne szczegóły, mogę podsumować ostatni tydzień jako udany.

Wydanie 0.28.0 będzie rewelacyjne poza wszelkimi wątpliwościami. Choćby w tym tygodniu, sztuczna inteligencja obsługująca walkę została właśnie dołączona do głównej gałęzi OpenMW, a przecież prace nad nowymi funkcjami wcale nie ustają.

Zacznijmy może od zapisywania i wczytywania gry. Otóż praca nad tą jakże przydatną funkcją jest w rękach ziniego, a to oznacza, że bez wątpienia również i ta funkcja znajdzie się w nowym wydaniu.

Scrawl nie ustaje w wysiłkach nad zaimplementowaniem magii. Obecne są już efekty przywołania, wampiryzm, obrażenia od słońca, odbicie i inne. W między czasie znalazł też czas na poprawienie pewnych aspektów obsługi pojemników, a także wprowadził możliwość odnowy ładunków przedmiotów magicznych.