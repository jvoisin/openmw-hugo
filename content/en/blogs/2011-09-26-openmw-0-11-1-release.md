{
   "author": "raevol",
   "categories": [
      "Release"
   ],
   "date": "2011-09-26T16:16:00+00:00",
   "tags": [
      "0.11.0",
      "release"
   ],
   "title": "OpenMW 0.11.1 Release",
   "type": "post",
   "url": "/2011/openmw-0-11-1-release/"
}
I&#8217;m proud to announce that OpenMW 0.11.1 is now released! Release packages can be found on the [Google Code page][1].

Please note that with the loss of our Linux builder, no Linux package is available. Linux users can download the source and build the release themselves. If you have Debian packaging experience and would like to help out, please let us know!

Changelog:

&#8211; Launcher implemented  
&#8211; Misc. code cleanup  
&#8211; Some optimizations implemented, further optimizations are planned  
&#8211; Fix to allow resource loading outside of BSA files  
&#8211; Fix to search for openmw.cfg in the correct locations  
&#8211; Added the TCL alias for ToggleCollision  
&#8211; Added missing cfg file support for some command line options  
&#8211; Added reporting of resulting state to Toggle-type script instructions  
&#8211; Fix for some NPC IDs being interpreted as Topic IDs  
&#8211; Implemented back-end for player journal  
&#8211; Implemented MessageBox  
&#8211; Implemented tab-completion in console  
&#8211; Implemented handling multiple data directories  
&#8211; Fix for accessing objects in cells via ID with mixed or upper case IDs  
&#8211; Fixed unicode conversion issue to allow localized encoding of gui strings

 [1]: http://code.google.com/p/openmw/downloads/list