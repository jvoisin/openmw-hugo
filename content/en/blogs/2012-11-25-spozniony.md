{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-11-25T14:03:42+00:00",
   "title": "spóźniony :(",
   "type": "post",
   "url": "/2012/spozniony/"
}
Możecie już uratować Morrowind przed szaleńczymi majakami Dagoth Ura w OpenMW! A przynajmniej zacząć… ;-)

Dzięki zmianom w dialogach ziniego i scrawlowi który naprawił omyłkę działają już pierwsze zadania wątku głównego, do momentu misji w Vivec gdzie, niestety, okazuje się, że brakuje nam instrukcji AI, a Mehra stoi tylko w miejscu jak ta głupia krowa. Tak czy inaczej wiadomo już co należy zrobić by posunąć się odrobinę dalej.

Tak więc rpopovici rozpoczął prace nad wykorzystaniem domyślnej klasy AI w skryptach. Zobaczymy co z tego wyniknie. Tymczasem scrawl dodał już całą chmarę innych instrukcji skryptów.

Gus twierdzi, że uporał się z najtrudniejszym elementem renderowania broni i już wkrótce będziemy mogli obejrzeć sobie oręż w dłoniach postaci gry.

Na razie wciąż nie ma postępu odnośnie książek. Lgro właśnie przekonał się, że wyzwanie jest wymaga więcej wysiłku niż ktokolwiek mógłby przypuszczać. 

pvdk za to wrócił po krótkiej przerwie do prac nad naszym starterem. Pojawiło się nawet okienko pierwszego uruchomienia!