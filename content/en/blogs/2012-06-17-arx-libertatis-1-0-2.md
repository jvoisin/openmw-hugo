{
   "author": "jvoisin",
   "categories": [
      "Uncategorized"
   ],
   "date": "2012-06-17T11:05:04+00:00",
   "title": "Arx Libertatis 1.0.2",
   "type": "post",
   "url": "/2012/arx-libertatis-1-0-2/"
}
Our dear friends from [Arx Libertatis][1] just released their version 1.02. This fixes various crashes, disappearing items when sorting the inventory, and minor rendering and input bugs. This release also fixes a bug that left the Spanish version with no text.



See the [changelog][2] for more details. [Windows and Linux][3] packages are available : what are you waiting for ?

 [1]: http://arx-libertatis.org
 [2]: http://wiki.arx-libertatis.org/Changelog#Patch_1.0.2
 [3]: http://wiki.arx-libertatis.org/Download