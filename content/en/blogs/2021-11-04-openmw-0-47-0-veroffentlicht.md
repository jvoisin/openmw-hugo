{
   "author": "Atahualpa",
   "categories": [
      "Uncategorized"
   ],
   "date": "2021-11-04T17:48:52+00:00",
   "summary": "Ein weiteres Jahr geschäftigen Treibens und erfolgreicher Entwicklungsarbeit liegt hinter uns &#8211; und wir, das OpenMW-Team, sind stolz darauf, die Veröffentlichung der Version 0.47.0 unserer Open-Source-Engine verkünden zu können!",
   "title": "OpenMW 0.47.0 veröffentlicht!",
   "url": "/2021/openmw-0-47-0-veroffentlicht/"
}
Ladet Euch die aktuelle Version auf unserer (englischsprachigen) [Download-Seite][1] für Windows, Linux oder macOS herunter.

Mit über 180 bearbeiteten Aufgaben und einer Fülle an neuen Features muss sich dieses Release nicht vor dem gigantischen 0.46.0er Release im letzten Jahr verstecken: Macht Euch gefasst auf Objekt-Paging, das es OpenMW endlich erlaubt, entfernte Objekte effizient darzustellen; die korrekte Unterstützung von Gras-Mods; ein verbessertes Beleuchtungssystem; eine effizientere und robustere Spielphysik; die neue, optionale Über-die-Schulter-Ansicht; und vieles, vieles mehr!

Darüber hinaus haben unsere fleißigen Entwickler wieder zahllose Bugs behoben &#8211; sowohl für das Basisspiel als auch für diverse Mods, um eine noch bessere Kompatibilität mit für die Original-Engine erstellten Modifikationen zu garantieren.

Schaut Euch am besten das [englische Release-Video][2] an &#8211; oder werft einen Blick auf das [erste deutsche Release-Video][3], das jemals für OpenMW produziert wurde! Beide stammen aus der Feder unseres neuen Video-Zweiergespanns, dem Release-Veteran Atahualpa und dem 0.46.0-Frischling johnnyhostile, und fassen die wichtigsten Neuerungen zusammen.

Im Folgenden findet Ihr die ins Deutsche übertragene, vollständige Änderungsliste für OpenMW, Version 0.47.0. Viel Spaß beim Lesen!

<div class="wp-block-group">
  <div class="wp-block-group__inner-container">
    <figure class="wp-block-embed aligncenter is-type-video is-provider-youtube wp-block-embed-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">

    <div class="wp-block-embed__wrapper">
    </div></figure>
  </div>
</div>

<div class="wp-block-group">
  <div class="wp-block-group__inner-container">
    <figure class="wp-block-embed aligncenter is-type-video is-provider-youtube wp-block-embed-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">

    <div class="wp-block-embed__wrapper">
    </div></figure>
  </div>
</div>

**Bekannte Probleme:**

  * Auf macOS kann OpenMW nur dann über den Editor (OpenMW-CS) gestartet werden, wenn &#8216;OpenMW.app&#8217; und &#8216;OpenMW-CS.app&#8217; als &#8216;siblings&#8217; verbunden sind
  * Lichteffekte auf Zauberpartikeln sehen zu blass aus
  * Wegfindung während der &#8220;Treulosigkeiten&#8221;-Quest in der &#8220;Tribunal&#8221;-Erweiterung funktioniert nicht richtig, so dass Taren Andoren seinen Zielort möglicherweise nicht erreicht; Warten für eine Spielstunde erlaubt es Taren, sein Ziel zu erreichen, so dass das Tagebuch korrekt aktualisiert wird
  * Performance verzauberter Fernkampfprojektile (z.B. Wurfpfeile) ist schlechter als die anderer Projektile

**Neue Engine-Features:**

  * _von akortunov_
      * [[#5524][4]] Fehlgeschlagene Skripte werden als &#8216;inaktiv&#8217; markiert und ignoriert, anstatt sie komplett zu entfernen, &#8211; so lange, bis das Spiel erneut geladen oder gestartet wird
      * [[#5580][5]] Verweigerung spezifischer NPC-Dienstleistungen (z.B. Handel oder Reparatur) über Filter ermöglicht komplexere Reaktionen anstelle des ursprünglichen &#8220;Alles oder nichts&#8221;-Verhaltens in der Original-Engine
      * [[#5642][6]] Möglichkeit, Pfeile dem Skelett von Akteuren anstatt dem Bogen zuzuweisen, erlaubt z.B. das Erstellen von Bögen für Linkshänder oder personenabhängiger Schussanimationen
      * [[#5813][7]] Gesonderte Behandlung von Gras-Mods mittels so genannter Gras-Instanziierung (&#8220;grass instancing&#8221;) zur effizienten Darstellung von Bodenvegetation im Spiel
  * _von AnyOldName3_
      * [[#4899][8]] &#8220;Alpha-to-Coverage&#8221;-Antialiasing: verbessert das Aussehen alpha-getester Texturen, z.B. Laub-Texturen aus dem &#8220;Morrowind Optimization Patch&#8221;
      * [[#4977][9]] OpenMW zeigt ein Platzhalter-Symbol, falls das eigentliche Symbol eines Gegenstandes nicht gefunden wurde
  * _von Assumeru_
      * [[#2404][10]] Stufenabhängige Listen (&#8220;levelled lists&#8221;) können in Behältern platziert werden
      * [[#2798][11]] Basisdatensätze sind variabel (&#8220;mutable&#8221;), d.h. das Modifizieren einer Instanz eines Basisdatensatzes (z.B. einer Wache) wirkt sich nun auf alle anderen Instanzen desselben Typs aus (z.B. alle Klone dieser Wache)
      * [[#5730][12]] Option zur Unterstützung des sichtbaren &#8220;Aufsammelns&#8221; von Pflanzen (&#8220;graphic herbalism&#8221;) im &#8216;Advanced&#8217;-Reiter des Launchers
      * [[#5771][13]] Der &#8216;ori&#8217;-Konsolenbefehl zeigt nun auch die Herkunft eines Meshes an und gibt an, ob die mit dem Präfix &#8220;x&#8221; versehene Version verwendet wird
  * _von bzzt_
      * Korrekte Unterwasser-Schatten bei eingeschalteter Lichtbrechung (Refraktion); deaktiviert, falls Skalierungsfaktor (&#8216;refraction scale&#8217;) ungleich 1,0 _[Ergänzungen von AnyOldName3 und madsbuvi]_
      * [[#2386][14]] Optional: Darstellung entfernter statischer Objekte mittels Objekt-Paging, d.h. Zusammenfassen benachbarter Objekte; standardmäßig eingeschaltet, benötigt aber aktivierte &#8216;Distant land&#8217;-Option _[Korrekturen und Feinschliff von psi29a]_
      * [[#2386][14]] Optional: Objekt-Paging in aktiven Zellen (3&#215;3-Gitter um den Helden herum); standardmäßig eingeschaltet _[Korrekturen und Feinschliff von psi29a]_
  * _von Capostrophic_
      * [[#5362][15]] Dialogfenster für das Aufteilen von Gegenständen zeigt den Namen der in einem Stapel Seelensteine gefangenen Seele an
      * [[#5445][16]] Verarbeitung der &#8216;NiLines&#8217;-Eigenschaft in NIF-Modellen
      * [[#5545][17]] Optional: Zu Boden gegangene (bewusstlose) NPCs können bestohlen werden; standardmäßig ausgeschaltet
      * [[#5579][18]] Unterstützung der Rotation in umgekehrter Reihenfolge (z-, y-, dann x-Achse) in der &#8216;SetAngle&#8217;-Funktion
      * [[#5649][19]] Unterstützung des komprimierten BSA-Formats der &#8220;TES V: Skyrim Special Edition&#8221;
      * [[#5672][20]] Option für an die Bildschirmauflösung angepasste Menühintergründe ist nun auch im Launcher verfügbar
  * _von CedricMocquillon_
      * [[#1536][21]] Anzeige der momentan erreichten Stufenaufstieg-Multiplikatoren für Attribute im &#8216;Stufe&#8217;-Tooltip
      * [[#4486][22]] Behandlung und Aufzeichnung von Abstürzen auf Windows
      * [[#5297][23]] Suchleiste im &#8216;Data Files&#8217;-Reiter des Launchers
      * [[#5486][24]] Optional: Angebotene Fertigkeiten von Ausbildern und deren Höchstgrenze werden durch Basis- statt modifizierte Werte bestimmt; standardmäßig ausgeschaltet
      * [[#5519][25]] Neugeordneter und erweiterter &#8216;Advanced&#8217;-Reiter im Launcher, um analog zum MCP (&#8220;Morrowind Code Patch&#8221;) implementierte Einstellungen zu integrieren
      * [[#5814][26]] &#8216;bsatool&#8217; kann bestehende BSA-Archive erweitern oder neue erstellen (unterstützt alle BSA-Formate)
  * _von elsid_
      * [[#4917][27]] Objekte, die zu klein sind, um das Navigationsnetz (&#8220;navigation mesh&#8221;) zu beeinflussen, veranlassen nun keine Aktualisierung dieses Netzes mehr, was dessen Effizienz erhöht
      * [[#5500][28]] Das Laden einer Spielszene endet nun erst dann, wenn genug Teile des Navigationsnetzes um den Helden herum erstellt worden sind
      * [[#6033][29]] Existierende Wegpunkte-Gitter werden nun automatisch dem Navigationsnetz hinzugefügt, um die Wegfindung weiter zu verbessern
      * [[#6033][29]] Rückfall in den alten Wegfindungsalgorithmus teilweise entfernt, um z.B. zu verhindern, dass Akteure ihre Feinde durch Lavaströme verfolgen
      * [[#6034][30]] Berechnung optimaler Routen basierend auf der individuellen Geschwindigkeit eines Akteurs zu Land und zu Wasser
  * _von fr3dz10_
      * [[#2159][31]] Optional: Gesprächsthemen, die zum Zeitpunkt des Dialogs keine neuen Antworten brächten, werden ausgegraut; neue Themen werden farblich hervorgehoben; standardmäßig ausgeschaltet (die Farben sind einstellbar)
      * [[#4201][32]] Kollisionen zwischen Projektilen inkl. Zaubern (ein erfolgreicher Treffer neutralisiert beide Projektile); imitiert das Verhalten in der Original-Engine
      * [[#5563][33]] Optional: Physik-Berechnungen werden in einem oder mehreren Hintergrund-Threads abgearbeitet, um die Effizienz zu erhöhen; standardmäßig ausgeschaltet
  * _von glassmancody.info_
      * [[#5828][34]] Neues, Shader-basiertes, anpassbares Beleuchtungssystem, das das bisherige Maximum von acht Lichtquellen pro Objekt entfernt
  * _von jefetienne_
      * [[#5692][35]] Der Filter im Zauberspruch-Fenster zeigt nun auch Gegenstände und Zauber mit zutreffenden magischen Effekten an &#8211; nicht nur solche mit zutreffendem Namen
  * _von ptmikheev_
      * Verbesserte freie Heldenansicht in der Verfolgerperspektive
      * [[#390][36]] Optional: Kamera schaut in Verfolgerperspektive über die Schulter des Helden; standardmäßig ausgeschaltet
      * [[#390][36]] Optional: Automatisches Wechseln der Kamera zur anderen Schulter in engen Gängen; standardmäßig eingeschaltet, benötigt aber aktivierte &#8216;View over the shoulder&#8217;-Option
      * [[#2686][37]] Aufgezeichnete Informationen in der &#8216;openmw.log&#8217;-Datei werden mit Zeitstempeln versehen
      * [[#4894][38]] Optional: NPCs vermeiden Kollisionen beim Laufen; standardmäßig ausgeschaltet
      * [[#4894][38]] Optional: NPCs machen Platz für sich bewegende Akteure, falls sie gerade nichts Wichtiges zu tun haben; standardmäßig eingeschaltet, benötigt aber aktivierte &#8216;NPCs avoid collisions&#8217;-Option
      * [[#5043][39]] Optional: Kamera schwankt beim Laufen in der Egoperspektive (&#8220;head bobbing&#8221;); standardmäßig ausgeschaltet (Stärke des Effekts einstellbar)
      * [[#5457][40]] Optional: Verbesserte Charakteranimationen bei diagonaler Fortbewegung; standardmäßig ausgeschaltet
      * [[#5610][41]] Optional: Akteure laufen weicher und drehen sich weniger ruckartig um; standardmäßig ausgeschaltet
  * _von simonmeulenbeek_
      * [[#5511][42]] Audio-Einstellungen im &#8216;Advanced&#8217;-Reiter des Launchers hinzugefügt
  * _von TescoShoppah_
      * [[#3983][43]] Installationsassistent verlinkt nun den FAQ-Abschnitt über Möglichkeiten des (legalen!) Erwerbs einer Morrowind-Kopie
  * _von unelsson_
      * [[#5456][44]] Grundlegende Unterstützung von &#8216;Collada&#8217;-Animationen
  * _von wareya_
      * [[#5762][45]] Der Bewegungslöser (&#8220;movement solver&#8221;) ist nun deutlich robuster
      * [[#5910][46]] Spielphysik greift im Notfall auf so genannte harte Delta-Zeit (&#8220;true delta time&#8221;) zurück, um die Berechnungsdauer pro Frame zu begrenzen und so ein Einfrieren (und Abstürzen) des Spiels zu vermeiden

**Neue Editor-Features:**

  * _von Atahualpa_
      * [[#6024][47]] Einstellbare Aktionen für primäre und sekundäre Auswahltasten beim Editieren von Terrain (&#8216;Terrain land editing&#8217;, siehe [#3171][48])
  * _von olcoal_
      * [[#5199][49]] Bessere und einstellbare Hintergrundfarben in der 3D-Ansicht
  * _von unelsson_
      * [[#832][50]] Korrekte Behandlung gelöschter Instanzen
      * [[#3171][48]] Objektauswahl in der 3D-Ansicht: zentrierter Würfel (&#8216;Centred cube&#8217;), an Ecke aufgezogener Würfel (&#8216;Cube corner to corner&#8217;), zentrierte Kugel (&#8216;Centred sphere&#8217;)
      * [[#3171][48]] Konfigurierbare Aktionen für die Objektauswahl: auswählen (&#8216;Select only&#8217;), zur Auswahl hinzufügen (&#8216;Add to selection&#8217;), von Auswahl abziehen (&#8216;Remove from selection&#8217;), Auswahl umkehren (&#8216;Invert selection&#8217;)

**Behobene Engine-Bugs:**

  * _von akortunov_
      * [[#3714][51]] Konflikte zwischen &#8216;SpellState&#8217;- und &#8216;MagicEffects&#8217;-Funktionalität der Engine behoben
      * [[#3789][52]] Das Aktualisieren der aktiven magischen Effekte sollte nun nicht mehr zu Abstürzen führen
      * [[#4021][53]] Werte von Attributen und Fertigkeiten werden nun als Fließkommazahlen gespeichert
      * [[#4623][54]] Implementierung der Corprus-Krankheit wurde verbessert
      * [[#5108][55]] Die Speichergröße von Spielständen wird nicht länger durch ein ungeeignetes Nebel-Texturformat aufgebläht
      * [[#5165][56]] Aktive Zaubereffekte werden nun in Echtzeit aktualisiert, anstatt Zeitstempel zu verwenden, was Probleme mit der Zeitskalierung im Spiel und mit geskripteten Tageszeitänderungen behebt
      * [[#5387][57]] &#8216;Move&#8217;- und &#8216;MoveWorld&#8217;-Befehle aktualisieren nun die Zelle des verschobenen Objektes in korrekter Weise
      * [[#5499][58]] Die Spiellogik für Beförderungen innerhalb einer Fraktion berücksichtigt nun auch die zweite der beiden bevorzugten Fertigkeiten der jeweiligen Fraktion
      * [[#5502][59]] Die Totzone für Analog-Sticks kann nun in der &#8216;openmw.cfg&#8217;-Datei eingestellt werden
      * [[#5619][60]] Tastatureingaben während des Ladens von Spielständen werden nun ignoriert
      * [[#5975][61]] Kontrollpunkte so genannter &#8220;Sheath&#8221;-Modelle (die in Mods mit wegsteckbaren Waffen genutzt werden) werden nun deaktiviert, um z.B. das versehentliche Abspielen von Schussanimationen für Bögen der &#8220;HQ Arsenal&#8221;-Mod zu verhindern
      * [[#6043][62]] Schild-Animationen von NPCs werden nun abgebrochen, sobald eine Lichtquelle an Stelle des Schildes ausgerüstet wird
      * [[#6047][63]] Tastenbelegungen für Maustasten können nun nicht mehr ausgelöst werden, wenn die Steuerung im Spiel deaktiviert ist, z.B. während des Ladens eines Spielstandes
  * _von AnyOldName3_
      * [[#2069][64]] &#8216;NiFlipController&#8217; in NIF-Modellen werden nun nur noch auf die Basistextur angewendet, was z.B. Darstellungsprobleme mit den Glühwürmchen aus der Mod &#8220;Fireflies Invade Morrowind&#8221; behebt
      * [[#2976][65]] Probleme mit der Priorität der lokalen und globalen Konfigurationsdateien behoben
      * [[#4631][66]] Grafikkarten, die einen Antialiasing-Faktor von 16 nicht unterstützen, verwenden nun automatisch einen geringeren Wert, wenn in den Optionen &#8217;16&#8217; eingestellt wird
      * [[#5391][67]] Charaktermodelle aus der Mod &#8220;Races Redone&#8221; werden nun im Inventar korrekt angezeigt
      * [[#5688][68]] Der Wasser-Shader wird nun in Innenräumen auch dann korrekt ausgeführt, wenn Schatteneffekte in Innenräumen deaktiviert worden sind
      * [[#5906][69]] Sonnenblendeffekt (&#8220;sun glare&#8221;) funktioniert nun auch mit &#8216;Mesa&#8217;-Treibern und AMD-Grafikkarten
  * _von Assumeru_
      * [[#2311][70]] NPCs, die nicht einzigartig sind (z.B. Wachen), können nun Ziel von Skripten sein
      * [[#2473][71]] Wird einem Händler eine Ware verkauft, deren Bestand immer sofort aufgefüllt wird, so erhöht sich nun dauerhaft die angebotene Menge dieser Ware
      * [[#3862][72]] Zufällige Inhalte von Behältern werden nun ähnlich wie in der Original-Engine bestimmt
      * [[#3929][73]] Behälter mit zufällig generiertem Inhalt werden nun nicht wieder aufgefüllt, wenn der Held sie plündert und anschließend mit dem Besitzer des Behälters handelt
      * [[#4039][74]] Gefolgsleute folgen ihrem Anführer nun nicht mehr im Gänsemarsch, sondern halten alle den gleichen Abstand zu ihm ein
      * [[#4055][75]] Falls eine lokale Instanz eines Skriptes erzeugt wird und bereits eine globale Instanz desselben Skripts aktiv ist, so werden die Variablen des lokalen Skripts nun mit den Werten des globalen initialisiert
      * [[#5300][76]] NPCs wechseln von Fackel zu Schild, wenn sie in einen Kampf geraten _[inspiriert von Capostrophics Vorarbeit]_
      * [[#5423][77]] Größere Kreaturen, z.B. Guars, die einem Akteur folgen, rempeln diesen nun nicht mehr an
      * [[#5469][78]] Geskriptete Drehungen oder Verschiebungen von großen Objekten setzen nicht länger den Nebel des Krieges der Umgebungskarte zurück
      * [[#5661][79]] Das Soundsystem nutzt nun Rückfallwerte, um die minimale und maximale Zeit zwischen regionsabhängigen Umgebungsgeräuschen zu bestimmen
      * [[#5661][79]] Es besteht nun die Möglichkeit, dass kein Umgebungsgeräusch abgespielt wird, falls die Summe der Wahrscheinlichkeiten aller Geräusche kleiner als 100 % ist
      * [[#5687][80]] Gleichzeitig ausklingende Beschwörungszauber für Gegenstände, die denselben Inventarplatz belegen, lassen das Spiel nicht länger einfrieren
      * [[#5835][81]] Skripte können nun KI-Werte (&#8216;AI Hello&#8217;, &#8216;AI Alarm&#8217;, &#8216;AI Fight&#8217;, &#8216;AI Flee&#8217;) auf negative Werte setzen
      * [[#5836][82]] Voraussetzungen für Gesprächsantworten, die negative KI-Werte als Bedingung enthalten, funktionieren nun korrekt
      * [[#5838][83]] Teleport-Türen, deren Zielort nichtexistierende, namenlose Zellen sind, korrumpieren nun nicht mehr die Umgebungskarte
      * [[#5840][84]] Angreifende NPCs, die von einem Feuer-/Frost-/Schockschildeffekt des Feindes getroffen werden, lösen nun den entsprechenden Sound-Effekt aus
      * [[#5841][85]] Kostenlose Zauber können nun auch dann gewirkt werden, wenn der zaubernde Akteur keine Magiepunkte mehr hat
      * [[#5871][86]] Benutzer mit russischem Tastatur-Layout können nun das &#8216;Ё&#8217;-Zeichen in Eingabefeldern verwenden, ohne dass sich die Konsole öffnet
      * [[#5912][87]] Effekte von Beschwörungszaubern werden bei Fehlschlagen der Beschwörung nicht mehr sofort entfernt
      * [[#5923][88]] Mausklicks auf textfreie Flächen im Tagebuch können nun nicht mehr zum spontanen Öffnen von Themen auf der nächsten Seite führen
      * [[#5934][89]] Kompatibilität zur Original-Engine: Die angegebene Anzahl der hinzuzufügenden Gegenstände in &#8216;AddItem&#8217;-Skriptbefehlen wird nun in einen vorzeichenlosen Wert umgerechnet, so dass negative Werte &#8220;überlaufen&#8221; und zum Hinzufügen einer positiven Menge von Gegenständen führen
      * [[#5991][90]] Skripte können nun Bücher und Schriftrollen aktivieren, wenn sich das Spiel in der Inventaransicht befindet
      * [[#6007][91]] Fehlerhafte Videodateien können nicht länger zu Spielabstürzen führen
      * [[#6016][92]] Schleichende oder springende NPCs halten nun nicht mehr an und drehen sich auch nicht zur Begrüßung des Helden um
  * _von Capostrophic_
      * [[#4774][93]] Wachen ignorieren nicht länger Angriffe eines unsichtbaren Helden, sondern sprechen diesen an und fliehen, falls sich der Held der Verhaftung widersetzt
      * [[#5358][94]] Das Ansprechen eines anderen Akteurs, ohne das Dialogfenster vorher zu schließen, löscht nun nicht mehr den Gesprächsverlauf, was z.B. Gespräche mit mehreren Teilnehmern mittels &#8216;ForceGreeting&#8217;-Befehl ermöglicht
      * [[#5363][95]] Das &#8216;Auto Calc&#8217;-Flag für Verzauberungen wird nun von OpenMW-CS und OpenMWs &#8216;esmtool&#8217; auch als solches behandelt
      * [[#5364][96]] Skripte, die ein nichtexistierendes globales Skript aufrufen wollen, überspringen diesen Schritt nun einfach, statt ihre Ausführung komplett abzubrechen
      * [[#5367][97]] Bei Auswahl eines bereits ausgerüsteten Zaubers oder magischen Gegenstandes über eine Schnelltaste wird nun nicht mehr das Ausrüsten-Geräusch abgespielt
      * [[#5369][98]] Der Skalierungswert in stufenabhängigen Kreaturenlisten wird nun auf die Größe der erzeugten Kreaturen angewendet
      * [[#5370][99]] Ach, Bethesda: Das Benutzen eines Schlüssels zum Öffnen einer fallenbewehrten Tür oder eines fallenbewehrten Behälters entschärft nur noch dann die Falle, wenn die Tür bzw. der Behälter auch tatsächlich abgeschlossen ist
      * [[#5397][100]] NPC-Begrüßungszeilen werden nun korrekt zurückgesetzt, wenn ein Bereich verlassen und erneut betreten wird
      * [[#5403][101]] Visuelle Zaubereffekte, die auf einen Akteur wirken, werden nun während dessen Sterbeanimation weiter abgespielt
      * [[#5415][102]] Modelle ausgerüsteter Gegenstände mit Environment-Maps, z.B. die Rüstungen der &#8220;HiRez Armors&#8221;-Mod, zeigen diesen Effekt nun auch
      * [[#5416][103]] In NIF-Modellen werden unbrauchbare Wurzeln, die keine Knoten sind, (&#8220;non-node root records&#8221;) nun weniger streng behandelt, damit Ressourcen bestimmter Mods geladen werden können
      * [[#5424][104]] Kreaturen wenden ihre Köpfe nun dem Helden zu
      * [[#5425][105]] Magische Effekte, deren Wirkung nicht einmalig ist, haben nun eine Mindestlaufzeit von einer Sekunde
      * [[#5427][106]] Die &#8216;GetDistance&#8217;-Funktion stoppt nun nicht mehr die Ausführung von Skripten, falls kein Objekt mit der übergebenen ID existiert
      * [[#5427][106]] Die &#8216;GetDistance&#8217;-Funktion und die interne Objektsuche zeichnen nun bessere Warnmeldungen auf, falls Objekte für eine gegebene ID fehlen
      * [[#5435][107]] Feindliche Angriffe können den Helden nun auch dann treffen, wenn die Kollisionsberechnung ausgeschaltet ist (Konsole: &#8216;tcl&#8217;)
      * [[#5441][108]] Die Priorität bestimmter Animationen wurde korrigiert, so dass Feinde den Helden nun wegstoßen können, wenn Letzterer in der Egoperspektive zum Schlag ausholt und diese Position einfach hält
      * [[#5451][109]] Magische Projektile verschwinden nun sofort, wenn ihre Quelle verschwindet oder stirbt
      * [[#5452][110]] Automatisches Laufen wird nicht mehr in Spielständen gespeichert, so dass der Held in diesem Fall nach dem Laden wieder stillsteht
      * [[#5484][111]] Gegenstände mit einem Basiswert von 0 können nun nicht mehr für ein Goldstück verkauft werden
      * [[#5485][112]] Erfolgreiche Einschüchterungsversuche erhöhen nun die Stimmung des Gesprächspartners wenigstens um einen Minimalwert
      * [[#5490][113]] Treffer auf den linken Waffenslot werden nun auf den linken Schulter- bzw. den Brustplatten-Slot umgeleitet, falls der Akteur keinen Schild ausgerüstet hat
      * [[#5525][114]] Das Suchfenster im Inventar ignoriert Groß- und Kleinschreibung nun auch für Nicht-ASCII-Zeichen
      * [[#5603][115]] Beim Wechsel zu konstanten Zaubereffekten im Verzauberungsmenü werden nun alle Effekte auf &#8216;Sich selbst&#8217; gesetzt, oder aber entfernt, falls sie nicht auf den Zaubernden selbst gewirkt werden können
      * [[#5604][116]] OpenMWs Ladekomponenten für Meshes können nun NIF-Dateien mit mehreren Wurzeln (&#8220;root nodes&#8221;) richtig interpretieren
      * [[#5611][117]] Reparaturhämmer mit 0 Anwendungen können nun nur noch ein Mal benutzt werden, bevor sie kaputtgehen; Dietriche und Sonden mit 0 Anwendungen bewirken hingegen gar nichts &#8211; danke, Bethesda!
      * [[#5622][118]] Die Priorität des Hauptmenü-Fensters wurde verringert, um zu verhindern, dass die Konsole den Mausfokus verliert und nicht mehr erreichbar ist
      * [[#5627][119]] Der Syntaxanalysierer (Parser) für Schriftstücke berücksichtigt nun Bilder und Formatierungs-Tags, die auf das letzte Zeilenende-Tag (&#8220;
        &#8220;) folgen, damit Bücher bestimmter Mods korrekt dargestellt werden können
      * [[#5633][120]] Negative Zaubereffekte, die vor dem Wechsel in den Gottmodus (Konsole: &#8216;tgm&#8217;) gewirkt wurden, schädigen den Helden nicht weiter
      * [[#5639][121]] Tooltips verdecken nun nicht mehr Hinweisfenster im Spiel
      * [[#5644][122]] Aktive Beschwörungseffekte auf dem Helden führen nicht länger zu Abstürzen während des Spielstarts
      * [[#5656][123]] Charaktere, die sich im Schleichen-Modus befinden, wechseln beim Blocken von Angriffen nun nicht mehr temporär in eine aufrechte Haltung
      * [[#5695][124]] Akteure, die über einen Skriptbefehl zum Wirken eines Fernzaubers auf sich selbst gezwungen werden, zielen nun auf ihre Füße statt in die Richtung ihres aktuellen Ziels
      * [[#5706][125]] KI-Sequenzen (z.B. für patrouillierende NPCs) werden nun beim erneuten Laden eines Spielstandes weiterhin in Endlosschleife ausgeführt
      * [[#5758][126]] Unter Wasser befindliche Akteure treiben nun an die Wasseroberfläche, wenn sie gelähmt sind
      * [[#5758][126]] Akteure unter Einfluss eines Levitationszaubers fallen nun zu Boden, wenn sie gelähmt werden
      * [[#5869][127]] Wachen konfrontieren den Helden nun nur noch dann mit einem Verbrechen, falls sich dieser in Sichtweite befindet
      * [[#5877][128]] Die Transparenz von Symbolen für aktive magische Effekte wird nun korrekt zurückgesetzt, um das Auftauchen &#8220;leerer&#8221; Symbole in bestimmten Situationen zu verhindern
      * [[#5902][129]] &#8216;NiZBufferProperty&#8217; verarbeitet nun &#8216;depth test&#8217;-Flag
      * [[#5995][130]] Der so genannte UV-Versatz (&#8220;UV offset&#8221;) in &#8216;NiUVControllern&#8217; &#8211; der in der Original-Engine zur Simulation der Bewegung von Flüssigkeiten genutzt wird &#8211; wird nun korrekt berechnet
  * _von ccalhoun1999_
      * [[#5101][131]] Feindlich gesonnene Begleiter folgen dem Helden nun nicht mehr durch Türen und nutzen auch keine Reisedienstleistungen mit ihm zusammen
  * _von davidcernat_
      * [[#5422][132]] Euer Held verliert nun nicht mehr alle seine Zaubersprüche, wenn er mit dem &#8216;resurrect&#8217;-Konsolenbefehl wiederbelebt wird
  * _von elsid_
      * [[#4764][133]] Haupt- und Rendering-Thread werden nun synchronisiert, um Fehler beim Erzeugen von Partikeln zu verhindern, z.B. bei Wasserwellen
      * [[#5479][134]] Fehler bei der KI-Wegfindung behoben, der dazu führte, dass NPCs wie angewurzelt herumstanden, statt durch die Gegend zu laufen
      * [[#5507][135]] Alle Lautstärkeeinstellungen sind nun immer auf den Bereich [0,0; 1,0] beschränkt und können diesen nicht mehr potenziell durch Änderungen in den Spieleinstellungen verlassen
      * [[#5531][136]] Fliehende Akteure werden nun korrekt gedreht, um z.B. zu verhindern, dass Klippenläufer auf der Flucht ins Wasser tauchen
      * [[#5914][137]] Der interne &#8216;Navigator&#8217; erstellt nun lokal beschränkte Pfade für Akteure mit weit entfernten Zielen, um sicherzustellen, dass die Wegfindung auch über große Distanzen (außerhalb des Navigationsnetzes) funktioniert
      * [[#6294][138]] Spielabsturz behoben, der von leeren Wegpunkte-Gittern verursacht wurde
  * _von fr3dz10_
      * [[#3372][139]] Magische und nicht-magische Projektile kollidieren nun immer mit sich bewegenden Zielen
      * [[#4083][140]] Sich öffnende oder schließende Türen imitieren beim Zusammenstoßen mit einem Akteur nun das Verhalten der Original-Engine _[teilweise behoben von elsid]_
      * [[#5472][141]] &#8220;Zero-Lifetime&#8221;-Partikel werden nun korrekt verarbeitet und eine zugehörige potenzielle Null-Division im &#8216;NiParticleColorModifier&#8217;, die zu Abstürzen auf Nicht-PC-Betriebssystemen führte, wenn z.B. Melchior Dahrks &#8220;Mistify&#8221;-Mod verwendet wurde, wurde behoben
      * [[#5548][142]] Die &#8216;ClearInfoActor&#8217;-Skriptfunktion löscht nun immer das korrekte Gesprächsthema eines Akteurs
      * [[#5739][143]] Fallschaden kann nun nicht mehr durch Speichern und Neuladen kurz vor dem Aufprall vermieden werden
  * _von glassmancody.info_
      * [[#5899][144]] Noch geöffnete Fenster mit Entscheidungsmöglichkeiten führen bei Beenden des Spiels nun nicht mehr zu Abstürzen
      * [[#6028][145]] NIF-Partikelsysteme erben nun die Partikel-Anzahl aus ihren &#8216;NiParticleData&#8217;-Einträgen auf korrekte Weise, was z.B. Probleme mit der Mod &#8220;I Lava Good Mesh Replacer&#8221; behebt
  * _von kyleshrader_
      * [[#5588][146]] Mausklicks auf leere Seiten im Tagebuch führen nicht länger zum Aufschlagen zufälliger Gesprächsthemen
  * _von madsbuvi_
      * [[#5539][147]] Wechsel von niedrigerer zu Vollbild-Auflösung führen nicht länger zu einem fehlerhaft skalierten Spielfenster
  * _von mp3butcher_
      * [[#1952][148], [#3676][149]] &#8216;NiParticleColorModifier&#8217; in NIF-Dateien werden nun richtig behandelt, was u.a. Probleme mit Feuer- und Rauch-Partikeleffekten behebt
  * _von ptmikheev_
      * [[#5557][150]] Seitwärtsbewegungen mit einem Analog-Stick resultieren nun nicht mehr in einer langsameren Bewegungsgeschwindigkeit verglichen mit der normalen Tastatureingabe
      * [[#5821][151]] NPCs, die von einer Mod hinzugefügt und in eine andere Zelle bewegt worden sind, werden nun korrekt von OpenMW registriert, wenn sich die Position der Mod in der Ladereihenfolge ändert
  * _von SaintMercury_
      * [[#5680][152]] Akteure zielen beim Wirken magischer Projektile nun auf die richtige Höhe, was z.B. verhindert, dass Netchbullen andauernd über den Kopf des Helden hinweg zielen
  * _von Tankinfrank_
      * [[#5800][153]] Das Ausrüsten eines Ringes mit konstanter Verzauberung kann nun nicht länger zum Ablegen eines Ringes mit auslösbarem Zauber führen, falls dieser Zauber aktuell im Zaubermenü ausgewählt ist
  * _von wareya_
      * [[#1901][154]] Das Kollisionsverhalten von Akteuren ähnelt nun noch mehr dem der Original-Engine
      * [[#3137][155]] Laufen auf der Stelle in eine Wand hinein hindert den Helden nun nicht mehr daran, zu springen
      * [[#4247][156]] Dank der Nutzung von AABB-Kollisionskörpern (&#8220;axis-aligned bounding box&#8221;) können Akteure nun bestimmte, ehemals problematische steile Treppen erklimmen
      * [[#4447][157]] Die Kollisionskörper von Akteuren wurden angepasst, um den Helden daran zu hindern, durch bestimmte Wände zu spähen
      * [[#4465][158]] NPCs zucken nun nicht mehr unkontrolliert, falls ihr Kollisionskörper mit einem anderen überlappt
      * [[#4476][159]] Der Held schwebt während Echtzeit-Reisen in abots &#8220;Gondoliers&#8221;-Mod nicht länger in der Luft
      * [[#4568][160]] Akteure können sich nicht mehr gegenseitig durch Levelgrenzen schieben, wenn zu viele von ihnen auf einem Haufen sind
      * [[#5431][161]] Szenen mit sehr, sehr vielen Akteuren können nun nicht mehr zu unaufhaltsamen Abwärtsspiralen in der Physik-Performance führen
      * [[#5681][162]] Helden kollidieren nun korrekt mit Holzbrücken, anstatt steckenzubleiben oder durch sie hindurchzurennen

**Behobene Editor-Bugs:**

  * _von akortunov_
      * [[#1662][163]] OpenMW-CS stürzt nicht länger ab, falls ein Datei- oder ein Konfigurationspfad Nicht-ASCII-Symbole enthalten
  * _von Atahualpa_
      * [[#5473][164]] Zellgrenzen werden nun korrekt eingezeichnet, wenn Änderungen am Terrain rückgängig gemacht oder wiederholt werden
      * [[#6022][165]] Das Terrain-Auswahlgitter wird nun korrekt eingezeichnet, wenn Änderungen am Terrain rückgängig gemacht oder wiederholt werden
      * [[#6023][166]] Objekte in der 3D-Ansicht blockieren nun nicht mehr das Auswählen von Terrain im &#8216;Terrain land editing&#8217;-Modus
      * [[#6035][167]] Kreis-Auswahlwerkzeug wählt nicht länger Terrain außerhalb des Kreisbogens aus
      * [[#6036][168]] Terrain-Auswahlwerkzeuge ignorieren nicht länger Vertices an der Nordost- und an der Südwest-Ecke einer Zelle
  * _von Capostrophic_
      * [[#5400][169]] Der Datenprüfer (&#8216;Verifier&#8217;) prüft Körperteile (&#8216;Body Parts&#8217;) von Kleidung und Rüstung nun nicht mehr auf einen angeblich vorhandenen &#8220;Rasse&#8221;-Eintrag
      * [[#5731][170]] Röcke, die von NPCs getragen werden, werden nun in der 3D-Ansicht korrekt dargestellt
  * _von unelsson_
      * [[#4357][171]] Die Sortierfunktion innerhalb der &#8216;Journal Infos&#8217;- und &#8216;Topic Infos&#8217;-Tabellen ist nun deaktiviert; die Reihenfolge der Einträge kann manuell geändert werden
      * [[#4363][172]] Die Kopierfunkion für &#8216;Journal Infos&#8217;- und &#8216;Topic Infos&#8217;-Einträge erlaubt es nun, die &#8216;ID&#8217; des Eintrags zu bearbeiten
      * [[#5675][173]] Instanzen (Objekte in der Spielwelt) werden nun mit dem richtigen Master-Index geladen und gespeichert, damit überschriebene Objekte nicht im Spiel geladen werden
      * [[#5703][174]] Kein Flackern und keine Abstürze mehr auf XFCE-Desktop-Umgebungen
      * [[#5713][175]] &#8216;Collada&#8217;-Modelle werden nun korrekt dargestellt
      * [[#6235][176]] OpenMW-CS stürzt nun nicht mehr ab, wenn Änderungen am Höhenprofil des Terrains erst rückgängig gemacht und dann wiederholt werden (&#8220;undo&#8221;/&#8221;redo&#8221;)
  * _von Yoae_
      * [[#5384][177]] Das Löschen von Instanzen in der 3D-Ansicht spiegelt sich nun unmittelbar in allen aktiven 3D-Ansichten wider

**Sonstiges:**

  * _von akortunov_
      * [[#5026][178]] Wettlaufsituationen (&#8220;data race&#8221;) bei der Verwendung von &#8216;rain intensity uniforms&#8217; behoben
      * [[#5480][179]] Qt4 wird nun nicht mehr unterstützt: neue Mindestversion: Qt 5.12
  * _von AnyOldName3_
      * [[#4765][180]] OSG-Arrays können nun nicht mehr von unterschiedlichen Threads in OpenMWs &#8216;ChunkManager&#8217; gebunden werden
      * [[#5551][181]] Windows: OpenMW erzwingt nun keinen Neustart mehr während der Installation
      * [[#5904][182]] Mesa: OpenSceneGraph-Problem bzgl. RTT-Methode (&#8220;render-to-texture&#8221;) behoben
  * _von CedricMocquillon_
      * [[#5520][183]] Verbesserte Behandlung des Kombinationsfeldes &#8216;Start default character at&#8217; (&#8220;Starte mit Standard-Charakter in&#8221;) im Launcher, um Warnmeldungen und Speicherlecks zu verhindern
  * _von fr3dz10_
      * [[#5980][184]] Während der Konfiguration von OpenMW wird nun geprüft, ob &#8216;Bullet&#8217; (OpenMWs Physik-Engine) mit doppelter Präzision verwendet wird, und diese Option notfalls erzwungen _[Zusatz von akortunov]_
  * _von glebm_
      * [[#5807][185]] Absturz auf ARM-Systemen behoben, der durch falsche Verarbeitung der Frame-Allokation im &#8216;osg-ffmpeg-videoplayer&#8217; verursacht wurde
      * [[#5897][186]] &#8216;MyGUI&#8217; aktualisiert, um Fehler bei dessen dynamischer Verlinkung zu verhindern

<a href="https://forum.openmw.org/viewtopic.php?f=38&t=7588" data-type="URL" data-id="https://forum.openmw.org/viewtopic.php?f=38&t=7588">Habt Ihr Kommentare oder Anregungen?</a>

 [1]: https://openmw.org/downloads/
 [2]: https://www.youtube.com/watch?v=_9sUhduT-K4
 [3]: https://www.youtube.com/watch?v=9Cwdf7fwO6k
 [4]: https://gitlab.com/OpenMW/openmw/-/issues/5524
 [5]: https://gitlab.com/OpenMW/openmw/-/issues/5580
 [6]: https://gitlab.com/OpenMW/openmw/-/issues/5642
 [7]: https://gitlab.com/OpenMW/openmw/-/issues/5813
 [8]: https://gitlab.com/OpenMW/openmw/-/issues/4899
 [9]: https://gitlab.com/OpenMW/openmw/-/issues/4977
 [10]: https://gitlab.com/OpenMW/openmw/-/issues/2404
 [11]: https://gitlab.com/OpenMW/openmw/-/issues/2798
 [12]: https://gitlab.com/OpenMW/openmw/-/issues/5730
 [13]: https://gitlab.com/OpenMW/openmw/-/issues/5771
 [14]: https://gitlab.com/OpenMW/openmw/-/issues/2386
 [15]: https://gitlab.com/OpenMW/openmw/-/issues/5362
 [16]: https://gitlab.com/OpenMW/openmw/-/issues/5445
 [17]: https://gitlab.com/OpenMW/openmw/-/issues/5545
 [18]: https://gitlab.com/OpenMW/openmw/-/issues/5579
 [19]: https://gitlab.com/OpenMW/openmw/-/issues/5649
 [20]: https://gitlab.com/OpenMW/openmw/-/issues/5672
 [21]: https://gitlab.com/OpenMW/openmw/-/issues/1536
 [22]: https://gitlab.com/OpenMW/openmw/-/issues/4486
 [23]: https://gitlab.com/OpenMW/openmw/-/issues/5297
 [24]: https://gitlab.com/OpenMW/openmw/-/issues/5486
 [25]: https://gitlab.com/OpenMW/openmw/-/issues/5519
 [26]: https://gitlab.com/OpenMW/openmw/-/issues/5814
 [27]: https://gitlab.com/OpenMW/openmw/-/issues/4917
 [28]: https://gitlab.com/OpenMW/openmw/-/issues/5500
 [29]: https://gitlab.com/OpenMW/openmw/-/issues/6033
 [30]: https://gitlab.com/OpenMW/openmw/-/issues/6034
 [31]: https://gitlab.com/OpenMW/openmw/-/issues/2159
 [32]: https://gitlab.com/OpenMW/openmw/-/issues/4201
 [33]: https://gitlab.com/OpenMW/openmw/-/issues/5563
 [34]: https://gitlab.com/OpenMW/openmw/-/issues/5828
 [35]: https://gitlab.com/OpenMW/openmw/-/issues/5692
 [36]: https://gitlab.com/OpenMW/openmw/-/issues/390
 [37]: https://gitlab.com/OpenMW/openmw/-/issues/2686
 [38]: https://gitlab.com/OpenMW/openmw/-/issues/4894
 [39]: https://gitlab.com/OpenMW/openmw/-/issues/5043
 [40]: https://gitlab.com/OpenMW/openmw/-/issues/5457
 [41]: https://gitlab.com/OpenMW/openmw/-/issues/5610
 [42]: https://gitlab.com/OpenMW/openmw/-/issues/5511
 [43]: https://gitlab.com/OpenMW/openmw/-/issues/3983
 [44]: https://gitlab.com/OpenMW/openmw/-/issues/5456
 [45]: https://gitlab.com/OpenMW/openmw/-/issues/5762
 [46]: https://gitlab.com/OpenMW/openmw/-/issues/5910
 [47]: https://gitlab.com/OpenMW/openmw/-/issues/6024
 [48]: https://gitlab.com/OpenMW/openmw/-/issues/3171
 [49]: https://gitlab.com/OpenMW/openmw/-/issues/5199
 [50]: https://gitlab.com/OpenMW/openmw/-/issues/832
 [51]: https://gitlab.com/OpenMW/openmw/-/issues/3714
 [52]: https://gitlab.com/OpenMW/openmw/-/issues/3789
 [53]: https://gitlab.com/OpenMW/openmw/-/issues/4021
 [54]: https://gitlab.com/OpenMW/openmw/-/issues/4623
 [55]: https://gitlab.com/OpenMW/openmw/-/issues/5108
 [56]: https://gitlab.com/OpenMW/openmw/-/issues/5165
 [57]: https://gitlab.com/OpenMW/openmw/-/issues/5387
 [58]: https://gitlab.com/OpenMW/openmw/-/issues/5499
 [59]: https://gitlab.com/OpenMW/openmw/-/issues/5502
 [60]: https://gitlab.com/OpenMW/openmw/-/issues/5619
 [61]: https://gitlab.com/OpenMW/openmw/-/issues/5975
 [62]: https://gitlab.com/OpenMW/openmw/-/issues/6043
 [63]: https://gitlab.com/OpenMW/openmw/-/issues/6047
 [64]: https://gitlab.com/OpenMW/openmw/-/issues/2069
 [65]: https://gitlab.com/OpenMW/openmw/-/issues/2976
 [66]: https://gitlab.com/OpenMW/openmw/-/issues/4631
 [67]: https://gitlab.com/OpenMW/openmw/-/issues/5391
 [68]: https://gitlab.com/OpenMW/openmw/-/issues/5688
 [69]: https://gitlab.com/OpenMW/openmw/-/issues/5906
 [70]: https://gitlab.com/OpenMW/openmw/-/issues/2311
 [71]: https://gitlab.com/OpenMW/openmw/-/issues/2473
 [72]: https://gitlab.com/OpenMW/openmw/-/issues/3862
 [73]: https://gitlab.com/OpenMW/openmw/-/issues/3929
 [74]: https://gitlab.com/OpenMW/openmw/-/issues/4039
 [75]: https://gitlab.com/OpenMW/openmw/-/issues/4055
 [76]: https://gitlab.com/OpenMW/openmw/-/issues/5300
 [77]: https://gitlab.com/OpenMW/openmw/-/issues/5423
 [78]: https://gitlab.com/OpenMW/openmw/-/issues/5469
 [79]: https://gitlab.com/OpenMW/openmw/-/issues/5661
 [80]: https://gitlab.com/OpenMW/openmw/-/issues/5687
 [81]: https://gitlab.com/OpenMW/openmw/-/issues/5835
 [82]: https://gitlab.com/OpenMW/openmw/-/issues/5836
 [83]: https://gitlab.com/OpenMW/openmw/-/issues/5838
 [84]: https://gitlab.com/OpenMW/openmw/-/issues/5840
 [85]: https://gitlab.com/OpenMW/openmw/-/issues/5841
 [86]: https://gitlab.com/OpenMW/openmw/-/issues/5871
 [87]: https://gitlab.com/OpenMW/openmw/-/issues/5912
 [88]: https://gitlab.com/OpenMW/openmw/-/issues/5923
 [89]: https://gitlab.com/OpenMW/openmw/-/issues/5934
 [90]: https://gitlab.com/OpenMW/openmw/-/issues/5991
 [91]: https://gitlab.com/OpenMW/openmw/-/issues/6007
 [92]: https://gitlab.com/OpenMW/openmw/-/issues/6016
 [93]: https://gitlab.com/OpenMW/openmw/-/issues/4774
 [94]: https://gitlab.com/OpenMW/openmw/-/issues/5358
 [95]: https://gitlab.com/OpenMW/openmw/-/issues/5363
 [96]: https://gitlab.com/OpenMW/openmw/-/issues/5364
 [97]: https://gitlab.com/OpenMW/openmw/-/issues/5367
 [98]: https://gitlab.com/OpenMW/openmw/-/issues/5369
 [99]: https://gitlab.com/OpenMW/openmw/-/issues/5370
 [100]: https://gitlab.com/OpenMW/openmw/-/issues/5397
 [101]: https://gitlab.com/OpenMW/openmw/-/issues/5403
 [102]: https://gitlab.com/OpenMW/openmw/-/issues/5415
 [103]: https://gitlab.com/OpenMW/openmw/-/issues/5416
 [104]: https://gitlab.com/OpenMW/openmw/-/issues/5424
 [105]: https://gitlab.com/OpenMW/openmw/-/issues/5425
 [106]: https://gitlab.com/OpenMW/openmw/-/issues/5427
 [107]: https://gitlab.com/OpenMW/openmw/-/issues/5435
 [108]: https://gitlab.com/OpenMW/openmw/-/issues/5441
 [109]: https://gitlab.com/OpenMW/openmw/-/issues/5451
 [110]: https://gitlab.com/OpenMW/openmw/-/issues/5452
 [111]: https://gitlab.com/OpenMW/openmw/-/issues/5484
 [112]: https://gitlab.com/OpenMW/openmw/-/issues/5485
 [113]: https://gitlab.com/OpenMW/openmw/-/issues/5490
 [114]: https://gitlab.com/OpenMW/openmw/-/issues/5525
 [115]: https://gitlab.com/OpenMW/openmw/-/issues/5603
 [116]: https://gitlab.com/OpenMW/openmw/-/issues/5604
 [117]: https://gitlab.com/OpenMW/openmw/-/issues/5611
 [118]: https://gitlab.com/OpenMW/openmw/-/issues/5622
 [119]: https://gitlab.com/OpenMW/openmw/-/issues/5627
 [120]: https://gitlab.com/OpenMW/openmw/-/issues/5633
 [121]: https://gitlab.com/OpenMW/openmw/-/issues/5639
 [122]: https://gitlab.com/OpenMW/openmw/-/issues/5644
 [123]: https://gitlab.com/OpenMW/openmw/-/issues/5656
 [124]: https://gitlab.com/OpenMW/openmw/-/issues/5695
 [125]: https://gitlab.com/OpenMW/openmw/-/issues/5706
 [126]: https://gitlab.com/OpenMW/openmw/-/issues/5758
 [127]: https://gitlab.com/OpenMW/openmw/-/issues/5869
 [128]: https://gitlab.com/OpenMW/openmw/-/issues/5877
 [129]: https://gitlab.com/OpenMW/openmw/-/issues/5902
 [130]: https://gitlab.com/OpenMW/openmw/-/issues/5995
 [131]: https://gitlab.com/OpenMW/openmw/-/issues/5101
 [132]: https://gitlab.com/OpenMW/openmw/-/issues/5422
 [133]: https://gitlab.com/OpenMW/openmw/-/issues/4764
 [134]: https://gitlab.com/OpenMW/openmw/-/issues/5479
 [135]: https://gitlab.com/OpenMW/openmw/-/issues/5507
 [136]: https://gitlab.com/OpenMW/openmw/-/issues/5531
 [137]: https://gitlab.com/OpenMW/openmw/-/issues/5914
 [138]: https://gitlab.com/OpenMW/openmw/-/issues/6294
 [139]: https://gitlab.com/OpenMW/openmw/-/issues/3372
 [140]: https://gitlab.com/OpenMW/openmw/-/issues/4083
 [141]: https://gitlab.com/OpenMW/openmw/-/issues/5472
 [142]: https://gitlab.com/OpenMW/openmw/-/issues/5548
 [143]: https://gitlab.com/OpenMW/openmw/-/issues/5739
 [144]: https://gitlab.com/OpenMW/openmw/-/issues/5899
 [145]: https://gitlab.com/OpenMW/openmw/-/issues/6028
 [146]: https://gitlab.com/OpenMW/openmw/-/issues/5588
 [147]: https://gitlab.com/OpenMW/openmw/-/issues/5539
 [148]: https://gitlab.com/OpenMW/openmw/-/issues/1952
 [149]: https://gitlab.com/OpenMW/openmw/-/issues/3676
 [150]: https://gitlab.com/OpenMW/openmw/-/issues/5557
 [151]: https://gitlab.com/OpenMW/openmw/-/issues/5821
 [152]: https://gitlab.com/OpenMW/openmw/-/issues/5680
 [153]: https://gitlab.com/OpenMW/openmw/-/issues/5800
 [154]: https://gitlab.com/OpenMW/openmw/-/issues/1901
 [155]: https://gitlab.com/OpenMW/openmw/-/issues/3137
 [156]: https://gitlab.com/OpenMW/openmw/-/issues/4247
 [157]: https://gitlab.com/OpenMW/openmw/-/issues/4447
 [158]: https://gitlab.com/OpenMW/openmw/-/issues/4465
 [159]: https://gitlab.com/OpenMW/openmw/-/issues/4476
 [160]: https://gitlab.com/OpenMW/openmw/-/issues/4568
 [161]: https://gitlab.com/OpenMW/openmw/-/issues/5431
 [162]: https://gitlab.com/OpenMW/openmw/-/issues/5681
 [163]: https://gitlab.com/OpenMW/openmw/-/issues/1662
 [164]: https://gitlab.com/OpenMW/openmw/-/issues/5473
 [165]: https://gitlab.com/OpenMW/openmw/-/issues/6022
 [166]: https://gitlab.com/OpenMW/openmw/-/issues/6023
 [167]: https://gitlab.com/OpenMW/openmw/-/issues/6035
 [168]: https://gitlab.com/OpenMW/openmw/-/issues/6036
 [169]: https://gitlab.com/OpenMW/openmw/-/issues/5400
 [170]: https://gitlab.com/OpenMW/openmw/-/issues/5731
 [171]: https://gitlab.com/OpenMW/openmw/-/issues/4357
 [172]: https://gitlab.com/OpenMW/openmw/-/issues/4363
 [173]: https://gitlab.com/OpenMW/openmw/-/issues/5675
 [174]: https://gitlab.com/OpenMW/openmw/-/issues/5703
 [175]: https://gitlab.com/OpenMW/openmw/-/issues/5713
 [176]: https://gitlab.com/OpenMW/openmw/-/issues/6235
 [177]: https://gitlab.com/OpenMW/openmw/-/issues/5384
 [178]: https://gitlab.com/OpenMW/openmw/-/issues/5026
 [179]: https://gitlab.com/OpenMW/openmw/-/issues/5480
 [180]: https://gitlab.com/OpenMW/openmw/-/issues/4765
 [181]: https://gitlab.com/OpenMW/openmw/-/issues/5551
 [182]: https://gitlab.com/OpenMW/openmw/-/issues/5904
 [183]: https://gitlab.com/OpenMW/openmw/-/issues/5520
 [184]: https://gitlab.com/OpenMW/openmw/-/issues/5980
 [185]: https://gitlab.com/OpenMW/openmw/-/issues/5807
 [186]: https://gitlab.com/OpenMW/openmw/-/issues/5897