{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-04-29T17:35:07+00:00",
   "title": "Ostatnie dni kwietnia roku 2012",
   "type": "post",
   "url": "/2012/ostatnie-dnie-kwietnia-roku-2012/"
}
Dosłownie parę dni dzieli nas od wydania openmw 0.14.0. Ostatnie szlify paczek i gotowe!

Kwiecień to chyba najbardziej udany miesiąc w historii openmw: nowe, olśniewające funkcje na które czekaliśmy tak długo w końcu działają, zespół nigdy nie był tak silny, a każda nowa wersja, więcej! każdy nowy dzień! przynosił nam nawet więcej niż moglibyśmy oczekiwać.

W tym tygodniu także mieliśmy wiele interesujących nowinek.

Przede wszystkim należy wspomnieć o tym, że gus w ramach prac nad GUI ekwipunku wprowadził detekcję dla kliknięcia na ikonę przedmiotu. Przenoszenie przedmiotów powinno działać.

Scrawl wciąż kontynuuje pracę nad podpowiedziami. Większość widgetów jest już widoczna, choćby ten odpowiadający za wyświetlania ceny przedmiotu. 

jhooks1 zgodnie ze swymi zapowiedziami implementuje koncepcje wywodzące się z kodu Chrisa do starego systemu animacji. Okazało się, bowiem iż pomimo wysiłków i osiągnięć chris nie jest w stanie zaimplementować animacji nif w systemie ogre3d na tyle dobrze, że działałyby bez zająknięcia w grze.

Zini wciąż pracuje nad nudnymi rzeczami które zrobione być muszą ale jakoś do tej pory nikt się do tego nie kwapił. ;-)