{
   "author": "Thunderforge",
   "categories": [
      "Uncategorized"
   ],
   "date": "2018-06-27T14:21:20+00:00",
   "title": "Help us plan OpenMW’s future",
   "type": "post",
   "url": "/2018/plan-openmws-future/"
}
It&#8217;s hard to believe that OpenMW 0.1.0 was released just over 10 years ago! What began as a barely functional ESM viewer is now a complete replacement for vanilla Morrowind and a complete engine for creating original games. There are still a few features that need to be polished, like [shadows][1] and [AI recastnavigation][2], but the project is almost ready to become version 1.0, signalling to the world that OpenMW has full parity with the original Morrowind engine.

But that doesn&#8217;t mean that our work is complete. OpenMW&#8217;s project leader Zini, with feedback from the project&#8217;s most active contributors, has produced the first draft of [an enormous design document][3] detailing the next steps that they think the project should take. The major focus is on improving modding capabilities (including &#8220;newscript&#8221; support, most likely Lua) and beginning the process of de-hardcoding game mechanics to allow mods to drastically alter gameplay.

Now we need your help. We&#8217;d like for as many people as possible to review this post-1.0 design document and provide their feedback. Have a change that you&#8217;d like to make? Submit either a Merge Request [on GitLab][3] or a Pull Request [on GitHub][4]. Want to talk about it with other contributors? Discuss it [on our forum][5]. This document is far from set in stone and will change as the project evolves.

Let&#8217;s work together to plan how OpenMW will enter into its next ten years!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=5257" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://openmw.org/2018/openmw-launcher-advanced-settings-menu/
 [2]: https://openmw.org/2018/solution-ai-problems/
 [3]: https://gitlab.com/OpenMW/openmw/blob/master/docs/openmw-stage1.md
 [4]: https://github.com/OpenMW/openmw/blob/master/docs/openmw-stage1.md
 [5]: https://forum.openmw.org/viewforum.php?f=20