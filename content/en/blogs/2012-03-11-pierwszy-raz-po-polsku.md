{
   "author": "sir_herrbatka",
   "date": "2012-03-11T21:30:31+00:00",
   "title": "Pierwszy raz po Polsku :-)",
   "type": "post",
   "url": "/2012/pierwszy-raz-po-polsku/"
}
Ahoj! 

OpenMW staje się coraz większym projektem, a rozwój nabrał impetu. Czas więc by nadać stronie odrobinę bardziej profesjonalnego szlifu. Pomysł na wprowadzenie sekcji z newsami również w języku Polskim kołatał się w mojej głowie już od jakiegoś czasu i mam nadzieję, że ta drobna w sumie zmiana nie tylko wpłynie pozytywnie na &#8220;wizerunek&#8221; strony, ale także okaże się przyjemną niespodzianką dla tych którzy preferują czytanie wiadomości w swoim ojczystym języku. :-)

Jak już wszyscy zapewne wiecie, parę dni temu wypuściliśmy najnowsze; wciąż pachnące kawą i potem programistów wydanie OpenMW oznaczone numerem 0.12.0. Zmiany, usprawnienia i nowe funkcje zbliżające nas do mitycznego wydania 1.0.0 oczywiście wszystkich nas cieszą i są powodem do dumy. Wystarczy uświadomić sobie jak wielkiego nakładu pracy wymagało to wydanie i jak sprawnie udało się je przygotować. Co prawda jak zwykle pojawiły się trudności, ale jednak opóźnienie było nie takie znów duże &#8211; zwłaszcza jeśli porówna się je do tego czego byliśmy świadkami w przeszłości.

Ale na pewno wolelibyście przeczytać o tym co nowego wydarzyło się poza wydaniem tego oklepanego 0.12.0 ;-) Otóż wydarzyło się wiele! Tydzień był pełen wytężonej pracy i przyniósł sporo postępu.

Po pierwsze, jak już zauważyliście na stronie pojawiły się newsy także w innych językach. Chciałbym osobiście podziękować naszemu wspaniałmu adminowi za tą nowinkę (bardzo możliwe, że oprócz Polskiego na stronę niedługo zawita także język Francuski). Lgro stwierdził też, że fajnie byłoby mieć link do webirc który pozwoliłby na łatwe zalogowanie się dla odwiedzających stronę na nasz kanał #openmw na freenode i jak pomyślał, tak zrobił. :-)

Werdanith skończył już ze swoim zadaniem i tym samym soundmanager został uprzątnięty, a nowe, konieczne funkcje mogą zostać dodane. Prawdę powiedziawszy to nawet już zostały dodane: scrawl zaimplementował dźwięk wody oraz deszczu. Werdanith ma teraz zamiar popracować nad odgłosami podnoszenia przedmiotów.

Zini sprawił, że skrypt w dialogach działają co otworzyło drogę do dalszego kodzenia dla GUS. Dialogi są kluczową funkcją dla planowanego wydania 0.13.0.

Zini ma zarezerwowane dwa zadania w planie dla 0.13.0 i jednym z nich jest używanie przedmiotów, rozumianych tu głównie jako ubrania i pancerze. Co prawda jhooks1 napracował się mocno by wyświetlać ubrania i pancerze &#8211; szczególnie kłopotliwe okazały się szaty, ale jego praca nie może zostać dostrzeżona w 0.12.0 z tego prostego względu, że wciąż nie ma tam żadnej formy ekwipunku. Tworzenie tymczasowych rozwiązań, tylko po to by robić śliczne screenshooty jest z kolei bez sensu tak więc dopiero po zakończeniu tego zadania openmw będzie faktycznie wspierać wyświetlanie ubranych npc.

Aktualnie jhooks1 wciąż poprawia fizykę w grze, kierując się przy tym wzorem Project Aedra. Gdy skończy będziemy mogli cieszyć się usprawnioną kolizją, i miejmy nadzieję, że nastanie kres ciągłym problemom ze spadaniem przez podłogę. Wyższość konkurencji w tym konkretnym względzie jest aktualnie wręcz przygnębiająca.

Pvdk znalazł przyczynę regresji launchera wersji 0.12.0: &#8220;Scroll and button background graphics in launcher not working in Linux package&#8221;. Problem dotyczy libpng i powinien wkrótce zostać rozwiązany. Ponadto w wersji dla windows launcher już nie wywołuje okienka wierszu poleceń. Ponieważ okno znika po zamknięciu openmw nie można z niego skopiować logów i ośmielę się twierdzić, że jest zupełnie bezużyteczne.

Corristo także pracuje, jednak nie nad funkcją widoczną dla użytkownika końcowego. 

No i na koniec: K1ll wspólnie z BrotherBrick przygotowuje ppa dla ubuntu. Jest z tym trochę roboty, a kiepskie ppa potrafi zrobić sporo złego w systemie, toteż zadanie zajmie trochę czasu. Za to już wkrótce ci spośród nas którzy używają ubuntu będą mogli w łatwy, automatyczny i przyjemny sposób zainstalować openmw.