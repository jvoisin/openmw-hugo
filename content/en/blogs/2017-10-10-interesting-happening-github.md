{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-10-10T20:13:58+00:00",
   "title": "Interesting things are happening on GitHub",
   "type": "post",
   "url": "/2017/interesting-happening-github/"
}
Another month has passed since we last updated you all with OpenMW&#8217;s latest news. Much has happened since, and I&#8217;m not going to let you wait any longer for more news.

As you might remember from the previous news post, the last release (0.42) attracted a couple of new developers to help out with the engine and the editor. We are really starting to see the effect of this now.

### Recently added features & features being worked on right now:

**Akortunov**, a relatively new developer still, is smashing bugs like a pro. He has also been working on quite a few really interesting features. One of them includes an overhaul to the pickpocket system. The old broken vanilla system will still be available for those who want a pure vanilla experience, but with akortunovs new changes, the engine will calculate the success of the pickpocketing based on weight instead of value, and you will also be able to do &#8220;reverse pickpocketing&#8221;, or in other words, place things in a victim&#8217;s inventory. Note that it is not merged into master yet, which means that the feature might end up not exactly as described above.

**Crassel** (or crussel187 on github), another newer developer, has been working on implementing an &#8220;over the shoulder&#8221; 3rd person perspective option. **[He has a successful prototype working][1]**, and only details left to do until it&#8217;s ready to hit master. He is also working on making animation support available to our graphics engine&#8217;s native 3d model format osgb/osgt. This feature might open up a ton of new possibilities for OpenMW. It is a quite big project, so time will tell if he can make it work.

**Drummyfish**, yet another new developer, has improved the water shader by reducing some white line artifacts seen on coastlines and implemented a **[raindrop water ripple effect][2]**. This feature is already merged, so you can try it already with a recent nightly or build from the master branch. And as if this wasn&#8217;t enough, drummyfish is continuing his path on mastering digital water by working on an overhaul of the rain and snow particle effects. As of now, rain in OpenMW has a fixed position above the player&#8217;s camera, like a small cloud that awkwardly follows you. But when drummyfish is done with his new implementation, we will no longer be stalked by rain and snow. There are some technical issues still to be solved until it can be merged however.

**Thunderforge** is no longer a new member here. When he joined the forums, he came like a prophet, carrying uncomfortable truths that needed to be told. Truths about <s>a community needing to repent</s> a launcher that needs to become **[more friendly to new users][3]**. He has begun working on this, and in a not too distant future, we might be directed places to buy Morrowind directly from the launcher if you do not have Morrowind installed already. How and if this should be done is still a matter of debate, so this feature could take some time before it is done.

**AnyOldName3** should no longer be considered new here either. You know him from the last news post. He is still working on his shadows implementation. It may be looking a bit funky still, but he is getting closer and closer. So far, the performance impact is impressively small.

**[Here is a video][4]** showing AnyOldName3&#8217;s shadows in a less-funky situation. With &#8220;less-funky&#8221;, I mean a situation where they work unusually well. In other situations, it can still look rather strange sometimes. But he&#8217;s really getting there, isn&#8217;t he?

**Chris/kcat** is a veteran in the community. He has been working on building further upon the glorious foundation of distant land that scrawl gave us. He wants to add functions to be able to more easily adjust the fog distance and make it look better with distant land. This will hopefully make our already great looking distant land look even better. There are still more to do to this feature, but if we are lucky, we can play with it in the next release.

As mentioned in the last post, **Aesylwinn** is back in action to do work on the CS. He and **PlutonicOverkill** teams up for the ultimate goal of full terrain mesh editing. Although this goal is not fully met yet, Aesylwinn has quite recently gotten his [**land texture editing feature**][5] merged into master, which means it will be available in version 0.43! Great news right? The mesh editing will probably take some time to finish, but hey &#8211; not too long ago, no one was working on this feature at all.

And one last thing. The almighty **scrawl** got so sick of the old implementation of the window manager (controlling the GUI in the game engine) that he completely overhauled it, adding new features as a nice bonus for us mere peasants. What kind of features you say? Well how about being able to navigate more or less all the menus with your keyboard? Bam! Grab the latest nightly or wait until 0.43 to test this out.

### Want to contribute?

Even though we have gotten quite a few more developers recently, it never hurts with even more. If you think you have the programming experience to help out, but don&#8217;t know where to start learning our code base, have a look at some of these wiki pages for some code documentation:

**[Developer reference][6]**

You might be extra interested in the pages about the engine&#8217;s **[architecture][7]**, including info about the [**GUI (MyGUI)**][8] and [**rendering engine (OpenSceneGraph)**][9].

But what if you&#8217;re not a programmer? We always need help with more bug testing. Run the game and [**report bugs**][10].

We also need help with sorting these bug reports, aka &#8220;triaging&#8221; them. This is suitable if you&#8217;ve been here for a while and you&#8217;re familiar with how the project is organized. **[The work of a bug triager is described here][11]**.

That&#8217;s it for today. If all goes well, our eminent video editor will return from doing his exams in a week or so. This means that 0.43 is not too far away now. Stay tuned!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=4688" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://www.youtube.com/watch?v=_7xBR2rQL7s
 [2]: https://www.youtube.com/watch?v=vJMNW58Yhy0
 [3]: https://forum.openmw.org/viewtopic.php?f=20&t=4372
 [4]: https://www.youtube.com/watch?v=aDr7o-Ft4R4
 [5]: https://github.com/OpenMW/openmw/pull/1427
 [6]: https://wiki.openmw.org/index.php?title=Developer_Reference
 [7]: https://wiki.openmw.org/index.php?title=Architecture
 [8]: https://wiki.openmw.org/index.php?title=GUI_Architecture
 [9]: https://wiki.openmw.org/index.php?title=Rendering_Architecture
 [10]: https://wiki.openmw.org/index.php?title=Bug_Reporting_Guidelines
 [11]: https://wiki.openmw.org/index.php?title=Bug_Triaging_Guidelines