{
   "author": "raevol",
   "categories": [
      "Release",
      "Uncategorized"
   ],
   "date": "2012-08-27T13:00:11+00:00",
   "tags": [
      0.17,
      "release",
      "WeirdSexy"
   ],
   "title": "OpenMW 0.17.0 Released!",
   "type": "post",
   "url": "/2012/openmw-0-17-0-released/"
}
The OpenMW team is proud to announce the release of version 0.17.0! Release packages for Ubuntu are now available via our [Launchpad PPA][1]. Release packages for other platforms are available on our [Download page][2]. This version introduces a new player control system, potion usage, a Main Menu, and a multitude of fixes and improvements.

Check out our release video by WeirdSexy:  


**Known Issues:**

  * Activating a bed can lock up the game
  * There is a minor memory leak that will be fixed in 0.18.0

**Changelog:**

  * Fixes for physics shapes and dark textures
  * Fix for character normals
  * Fix for laggy input on OS X
  * Added support for objects crossing cell borders
  * Re-implemented dropping items
  * Implemented a Main Menu
  * Implemented Camera Modes and proper Player control
  * Added support for object rotation and scaling
  * Fix for NIF material sharing
  * Implemented potion usage
  * Implemented skill gain backend
  * Implemented Drain/Fortigy dynamic Stats/Attributes magic effects
  * Fix for various crashes and errors
  * Fixes for memory leaks
  * Various improvements for console scripts
  * Various code cleanup and improvements

 [1]: https://launchpad.net/~openmw/+archive/openmw
 [2]: https://code.google.com/p/openmw/downloads/list