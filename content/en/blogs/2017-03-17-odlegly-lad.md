{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-03-17T14:28:54+00:00",
   "title": "Odległy ląd",
   "type": "post",
   "url": "/2017/odlegly-lad/"
}
[<img loading="lazy" class="wp-image-4922 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2017/03/budmNsK-300x169.jpg?resize=665%2C374&#038;ssl=1" alt="" width="665" height="374" data-recalc-dims="1" />][1]

A cóż to? Upiorną Barierę widać aż z Ald-Ruhn, a fps wciąż jest powyżej 10. To OpenMW, nie MGE XE, prawda?

Prawda, to OpenMW, a to, [co widzicie][2], jest możliwe dzięki najnowszemu dodatkowi od scrawla &#8211; odległemu lądowi. Ta nowa funkcja jest dostępna w najnowszej rewizji OpenMW. Jeśli używasz Windowsa, możesz ją pobrać z [nocnych buildów][3] Ace&#8217;a, by móc ją osobiście przetestować.

Co to w praktyce robi? W skrócie, [upraszcza siatkę terenu][4]. Nic więcej. Nie rusza to skryptów, NPC i&nbsp;obiektów statycznych, ale samo uproszczenie lądu zmniejsza obciążenie na tyle, że można pomanipulować zasięgiem wyświetlania.

Możliwe, że widać, że wykorzystałem (tj. DestinedToDie wykorzystał &#8211; tłumaczę jego opis) powyższą właściwość w niektórych zrzutach ekranu i załadowałem dostatecznie dużo elementów statycznych, by stworzyć iluzję, że obiekty są tak daleko, [jak sięga wzrok][5]. [Pusty ląd][6] można dostrzec wyłącznie, gdy celowo patrzy się na coś bardzo odległego.

Muszę przyznać, że oszukiwałem trochę, by podbić liczbę klatek na sekundę. Jest pewna dodatkowa funkcja służąca do ukrywania małych obiektów (_culling_). Sprawia ona, że zbyt małe obiekty nie są w ogóle renderowane, co odciąża komputer. Używałem tej funkcji dość agresywnie, ponieważ pracuję na kilkuletnim laptopie. Ktoś ze sprzętem przeznaczym do gier może osiągnąć lepsze rezultaty z mniej agresywnym cullingiem.

Chciałbym podzielić się jeszcze jednym [screenshotem][7] oraz opisać, jak osiągnąć taki efekt. Po pobraniu i&nbsp;instalacji ostatniej rewizji (_commita_) znajdź plik settings.cfg i dopisz w nim poniższe linijki (po znaku #&nbsp;znajduje się komentarz):

`[Camera]</p>
<p>viewing distance = 6666.0 # Określa, jak dużo lądu renderować. Można dopisać jeszcze jedną lub dwie szóstki.</p>
<p>small feature culling pixel size = 2.0 # Domyślnie 2, ale można zwiększyć aż do 16. Zwiększa wydajność.</p>
<p>[Cells]</p>
<p>exterior cell load distance = 1 # Wczytuje obiekty statyczne, NPC i skrypty wokół gracza. Można zmienić na 2, 3 lub 4 (wtedy wczytywane są w większym promieniu).</p>
<p>[Terrain]</p>
<p>distant terrain = True # Włącznik funkcji odległego lądu.`

##### Zapraszamy do komentowania [tutaj][8].

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2017/03/budmNsK.jpg?ssl=1
 [2]: http://i.imgur.com/IuRDXv7.jpg
 [3]: https://forum.openmw.org/viewtopic.php?f=20&t=1808#p18839
 [4]: http://i.imgur.com/YklBk3E.jpg
 [5]: http://i.imgur.com/HLZzgDp.jpg
 [6]: http://i.imgur.com/pAg1oKv.jpg
 [7]: http://i.imgur.com/iFuVAdf.jpg
 [8]: https://forum.openmw.org/viewtopic.php?f=38&t=4221