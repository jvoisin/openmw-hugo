{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2015-02-16T10:58:34+00:00",
   "title": "Zmiana silnika na OpenSceneGraph",
   "type": "post",
   "url": "/2015/zmiana-silnika-na-openscenegraph/"
}
Ogre3D to silnik graficzny wykorzystywany przez OpenMW od samego początku. Teraz zespół ogłasza rezygnację z niego.

W okresie naszych wersji alpha, Ogre3D był niezwykle użyteczny i umożliwiał szybki rozwój projektu. Dzięki Ogre3D, możesz już teraz grać w mniej więcej kompletnego Morrowinda poprzez OpenMW, co jest dużym wyczynem.

Jednakże, żeby doprowadzić OpenMW do stanu w pełni wyszlifowanego, i żeby dodać pozostałe kilka funkcji, musielibyśmy albo utworzyć swoją wersję Ogre3D (forka), dopasowaną do naszych potrzeb, albo zmienić silnik. Obie opcje są zadaniami trudnymi. Po długiej dyskusji wybraliśmy drugą opcję.

### Dlaczego zmiana?

Nie ma jednego prostego powodu. Złożyło się na to kilka czynników.

Pierwszą blokadą dla wydania OpenMW 1.0 jest mała liczba klatek na sekundę w porównaniu z oryginalnym Morrowindem. Wynika to z pewnych właściwości Ogre3D 1.x, które stanowiły często wąskie gardło. W&nbsp;Ogre3D 2.x zostało to poprawione, toteż początkowym pomysłem było przeportowanie OpenMW na nowe wydanie tego silnika. Byłoby to duże przedsięwzięcie, ponieważ pod względem interfejsu programistycznego zaszły spore zmiany. Jest jednak kilka problemów z tym związanych:

  * **Tag points (znaczniki):** Używamy funkcji Ogre3D zwanej „Tag points”, żeby łączyć różne części ciała postaci w grze. Tej funkcji nie ma w nowej wersji silnika, więc musielibyśmy stosować skomplikowane obejścia.
  * **Wsparcie dla OpenGL2:** Gałąź 3.1 Ogre3D porzuciła wparcie dla sprzętu OpenGL2. Co prawda OpenMW nigdy nie miał być tworzony pod sprzęt, na którym początkowo działał Morrowind, ale uważamy, że jest za wcześnie na porzucenie wsparcia dla OpenGL2. To wpłynęłoby negatywnie na wielu użytkowników.
  * **System materiałów:** W Ogre 2.1 zmieniono system materiałów. Jest on dobrze zoptymalizowany, ale za to utrudnione jest tworzenie własnych materiałów. Będziemy to robić często w OpenMW, szczególnie jeśli wziąć pod uwagę modyfikacje użytkowników. Preferujemy zatem system bardziej elastyczny i łatwiejszy w użyciu.

Poza tym jest jeszcze kilka spraw, które stanowią problem od dłuższego czasu:

  * **System zasobów:** Ogre3D wymusza użycie globalnego menedżera zasobów, a my potrzebujemy jednego na każdy dokument w OpenCS. Dalej: każdy zasób musi mieć unikalny identyfikator, to prowadzi do dodatkowej pracy po naszej stronie. Może to nawet prowadzić do kolejnych kłopotów, jeśli nazwa zasobu stworzona przez użytkownika konfliktuje z jakąś już istniejącą. Zespół tworzący Ogre3D pracuje nad tym od 2013 roku, ale niestety wciąż nie skończył.
  * **System szablonów materiałów (Material stencil support):** Format NIF używany przez Morrowind ma taką funkcję jak „ustawienia szablonu”, której Ogre3D nie wspiera.
  * **Skalowanie szerokości NPC:** NPC w Morrowindzie mają taką właściwość jak „masa” , która powinna skalować NPC wzdłuż jego lokalnej osi X. OpenMW obecnie ignoruje tę właściwość, bo system szkieletów w Ogre3D nie obsługuje skalowania jednowymiarowego.

### Nowy silnik

W świetle powyższych uwag, rozejrzeliśmy się za alternatywami i znaleźliśmy [OpenSceneGraph][1], które świetnie spełnia nasze oczekwania, oferując do tego wysoką wydajność. Dokładnie sprawdziliśmy, czy ma wymagane funkcje, lub czy da się je łatwo dodać poprzez wtyczkę. W niedalekiej przyszłości przedstawimy harmonogram prac nad przeportowywaniem gry. Będziemy informować na bieżącą o ich postępie.

Dużą różnicą jest to, że OpenSceneGraph (w skróce OSG) wspiera tylko OpenGl. Ogre3D wspierał też DirectX, co jest dużym atutem. W idealnym świecie moglibyśmy zatem oferować wybór między jednym i drugim bez dodatkowej pracy z naszej strony. Niestety świat nie jest idealny. Nastręczyłoby to trudności w&nbsp;pisaniu shaderów (podejście zależy od systemu renderowania). Poza tym wiele błędów ujawniało się tylko przy użyciu Direct3D. Wspieranie tylko OpenGL raczej nas cieszy, bo w tej sytuacji nie musimy wspierać dwóch różnych systemów. Poza tym większość naszych developerów używa Linuksa, co uniemożliwia testowanie Direct3D.

## Wstępne FAQ:

### Q: Jak długo potrwa zmiana?

A: Dobra wiadomość jest taka, że Ogre3D to tylko silnik renderujący, i tylko do tego jest przez nas używany. Kod OpenMW jest podzielony na różne podsystemy, a zmiana silnika wpłynie co najwyżej na jeden nich, to jest „mwrender”. Jest to jakieś 8% kodu z naszej bazy.

### Q: Co się stanie tymczasem z OpenMW, które używa OGRE? Dalszy rozwój (nowe funkcje), tylko poprawki błędów, czy kompletne zignorowanie?

A: Będziemy normalnie dalej rozwijać gałąź OGRE, dodając nowe funkcje i poprawki, tak długo jak port na OSG będzie na etapie rozwoju. Wiemy, że w OpenMW gra wiele osób, pomimo, że to tylko wersja alpha, więc ta ciągłość jest dla nas istotna.

### Q: Co z platformami, na których zadziała OpenMW (Linux, Windows, Mac OS X, Android)?

A: Będziemy dalej wspierać je wszystkie.

### Q: Galeria na stronie OpenSceneGraph jest raczej skromna. Jeśli ten silnik jest taki super, to czemu nie robi się gier z jego użyciem?

A: Pod względem możliwości OSG dorównuje Ogre3D (a nawet go przewyższa), zatem ta „skromność” galerii OSG jest dla nas zaskoczeniem. Twórca tego silnika, Robert Osfield, ma swoją teorię na to (wolne tłumaczenie, oryginalna wypowiedź jest [tutaj][2]: „As a general note”…):

> Generalnie, jeśli chodzi o gry i konflikt OSG vs Ogre, podejrzewam, że to kwestia korzeni każdego projektu i kultury w jakiej się rozwijał. OSG wywodzi się ze świata VisSim i przez lata wyrósł na przenośną bibliotekę graficzną ogólnego przeznaczenia. Społeczność zgromadzona wokół niego zajmowała się profesjonalnymi symulacjami, wizualizacjami, rzeczywistością wirtualną i sprawami naukowymi. Ogre natomiast od początku był tworzony jako API dla gier, choć może być używany do wielu innych rzeczy niż gry. Jednakże to do gier jest wykorzystywany najczęściej.

### Q: Mam obawy co do zakończenia wsparcia dla Direct3D, bo to przy jego użyciu mam więcej klatek na sekundę w porównaniu z OpenGL.

A: Jesteśmy świadomi różnicy w wydajności między jednym i drugim. Wynika ona z kiepskej implementacji OpenGL w Ogre3D 1.x. Valve pokazał ([tutaj][3]), że poprawna jego implementacja gwarantuje wydajność na poziomie Direct3D, a nawet większą.

### Ostatnie podziękowania

Wiele zawdzięczamy Ogre3D i mamy nadzieję, że dobrze się temu projektowi odwdzięczyliśmy przyczyniając się do wielu poprawek.

Chcemy podziękować wszystkim, którzy pracowali nad Ogre3D i jakkolwiek przyczynili się do jego rozwoju, za ich wielki wysiłek w to włożony. Wszystkim, z którymi pisaliśmy na forum, za wsparcie i zachetę z ich strony. Szczególnie dziękujemy Kojackowi za pomoc w debugowaniu tekstur DDS.

Zapraszamy do komentowania [tutaj][4].

 [1]: http://www.openscenegraph.org/
 [2]: http://permalink.gmane.org/gmane.comp.graphics.openscenegraph.user/74376
 [3]: http://blogs.valvesoftware.com/linux/faster-zombies/
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=2754