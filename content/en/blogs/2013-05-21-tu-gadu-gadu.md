{
   "author": "sir_herrbatka",
   "date": "2013-05-21T12:50:20+00:00",
   "title": "My tu gadu-gadu…",
   "type": "post",
   "url": "/2013/tu-gadu-gadu/"
}
… a tymczasem na kanale IRC:

> Honkeh> !  
> gus2> KittyCat: :D  
> riothamus> eeek!!!!!!!!!!!!!!!!!  
> scrawl> weeeee :D  
> Honkeh> KittyCat: Congrats  
> gus2> indeed  
> KittyCat> thanks  
> riothamus> i am squeeling like a girl right now  
> riothamus> how nerdy is that

O tak moi drodzy, tylko jedna rzecz mogła wzbudzić taką reakcję: Zini właśnie zmergował gałąź Chrisa, w której pracował nad warstwowaniem animacji. Oznacza to tym samym, że możemy uznać ten rozdział za zamknięty, odhaczyć zadanie jako wykonane i odpalić fajerwerki.

Oprócz tego, jeszcze ten krótki filmik…



Zarys 0.24.0 jest widoczny jak na dłoni. Możliwość dobycia broni, oraz npc którzy w końcu wykazują oznaki życia. Do tego dochodzą mniejsze, ale wciąż widoczne funkcje, takie jak możliwość otwieranie drzwi. Po prostu rewelacja!

<img src="https://i0.wp.com/img845.imageshack.us/img845/3517/6v9leufx30z5j95szuwn.gif" alt="Lol" data-recalc-dims="1" /> 

Ciekawe czy uda nam się dorwać walkę oraz AI walki (w samą animacje ataku nie wątpię) w wydaniu 0.25.0.

Tymczasem Zini dodaje jedną po drugiej nowe kolumny do edytora. Graffy ostatecznie dodał ustawienia użytkownika. 

Scrawl dodał długo oczekiwany przycisk &#8220;nowa gra&#8221; w menu głównym pozwalający na… eh zaczęcie nowej gry bez konieczności powtórnego uruchomienia OpenMW. Oprócz tego wprowadził również obsługę flagi &#8220;persistence&#8221; npc oraz potworów ─ zapobiega usuwaniu zwłok zabitych.

Jak co tydzień rozwiązaliśmy wiele bugów. Nagrody bugożerców tygodnia dla:

  * riothamus (ten piszczący jak nastolatka gość) za rozwiązanie swojego pierwszego błędu.
  * Glorf za rozwiązanie kolejnego ukrytego błędu. Bethesda uznała, że &#8220;EnableMagicMenu&#8221; powinno także umożliwić korzystanie z dziennika.
Scrawl jest zdyskwalifikowany, bo głupio byłoby dawać mu nagrodę co tydzień.