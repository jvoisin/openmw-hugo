{
   "author": "jvoisin",
   "categories": [
      "Uncategorized"
   ],
   "date": "2012-12-02T17:01:05+00:00",
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-31/"
}
Good news everyone (read this with the voice of the professor Fansworth).

Mark76 is still working and progressing on the support for multiples esm/esp files. The list of changes is quite long and I strongly recommend visiting the forum and reading the post! We can&#8217;t expect to see a lot of progress in this area very soon since Mark76 will be busy this coming week.

Scrawl added some more script instructions (PcExpell, PcExpelled, PcClearExpelled, RaiseRank, LowerRank, GetWerewolfKills, ModScale, SetDelete, GetSquareRoot&#8230;)

gus is working on shields and weapons : you can draw/undraw your weapons, and your shield is visible! Torches aren&#8217;t handled just yet and there is still much to be done, like first person perspective that is currently not handled.

Meanwhile Zini decided to move his focus from the OpenMW engine to the editor. He started a new OpenCS. Until now only Eli2 had worked on the editor while the engine benefited from a larger team. If the situation continued it would lead to the development of OpenMW&#8217;s post 1.0 features stalling. because it&#8217;s almost impossible to work with data files (esp or OpenMW files that are yet to come) without the editor.

Zini decided he <a title="Don't click! This link is EVIL!" href="http://www.youtube.com/watch?v=r3ubLOKbx-Y" target="_blank">wants it all</a> and it&#8217;s time to do something about the editor situation. Hopefully, we will be able to reuse major parts of Eli2&#8217;s code.

PS  
I forgot about trombonecot who solved the problem of long names in tooltips. It looks <a href="http://www.youtube.com/watch?v=ftwThEF09vI&#038;feature=youtu.be" title="Video!" target="_blank">neat</a>!