{
   "author": "sir_herrbatka",
   "date": "2013-02-11T14:51:42+00:00",
   "title": "Są obrazki!",
   "type": "post",
   "url": "/2013/sa-obrazki/"
}
I o to kolejny tydzień z hukiem pociągu ─ tym razem mam dla was coś dużego niczym tyranozaur po zjedzeniu stada brontozaurów. Ale napięcie musi stopniowo rosnąć.

Chris wciąż zajęty jest animacjami, a wymiernym efektem jego wysiłków jest działająca już animacja pływania. Reszta wymagać będzie rozwinięcia możliwości movement solver. Na szczęście kod jest bardzo krótki i prosty, z pewnością o wiele łatwiejszy do zrozumienia niż mastodont zwany pmove. Gdyby któryś z was miał już jakieś doświadczenia z grami 3D i szukał jedynie dobrego momentu by włączyć się do projektu myślę, że nie musi już czekać na nic więcej. Chris z radością przywita pomoc, podobną do tej wywołanej przez następną informację.

Bo o to nastał moment, nie bójmy się tego słowa: historyczny. Wsparcie dla wielu plików gry wylądowało w gałęzi next. Oznacza to, że wsparcie może być obecne w wydaniu 0.22.0, choć będziemy musieli rozwiązać cały szereg błędów, błędzików i bugów. Przykładowo: scrawl zaobserwował znaczną utratę wydajności w wypadku załadowania dodatkowych plików esm.

A teraz mniej istotne nowiny:

PotateosMaster zajął się importowaniem archiwów gry spisanych w pliku openmw.cfg Oczywiście powiązane jest to ze świeżo dodaną opcją wsparcia dla dodatków.

Dougmencken, członek sekty posiadaczy komputerów PowerPC, i osoba która, swojego czasu uruchomił VCMI na swojej ulubionej architekturze niedawno dowiedział się o naszym projekcie i rzecz jasna wyznaczył sobie nowy cel. Dopiął swego co na pewno ucieszy garstkę właścicieli starych komputerów Apple. Wspominam o tym, głównie dla tego by zaprezentować tą unikatową kolekcję bugów powiązanych zarówno z architekturą PowerPC jak „niekoniecznie rewelacyjnymi” otwartymi sterownikami dla kart graficznych nvidii.

Niebieskie ramki okien, kojarzą mi się trochę z rosyjską ludowością. Oprócz tego różowe czcionki. Nie powiem z czym mi się <a title="click me" href="https://dl.dropbox.com/u/2899105/openmwrightmousebuttonm.png" target="_blank">kojarzą</a>.

Ponieważ firma Apple posiada patent na zaokrąglone rogi postanowiliśmy, że nasze słońce zamiast kształtu okrągłego przyjmie nieco bardziej kanciastą <a title="click me" href="https://dl.dropbox.com/u/2899105/quadratishsonne02.png" target="_blank">formę</a>.

Niebo <a title="click me" href="https://dl.dropbox.com/u/2899105/screenshot001zu.png" target="_blank">płonie</a>&#8230;

W zakresie prac nad edytorem również posunęliśmy się do przodu. Zini wprowadził podstawowe wsparcie dla GMST, a prace nad importowaniem plików zostały już zamknięte.