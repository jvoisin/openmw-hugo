{
   "author": "sir_herrbatka",
   "date": "2013-03-10T18:16:05+00:00",
   "title": "Na szybko, niestety.",
   "type": "post",
   "url": "/2013/na-szybko-niestety/"
}
Oj się działo!

Po pierwsze scrawl właśnie wszedł w fazę wzmożonej aktywności co poskutkowało zaimpelemtnowaniem nowych funkcji: przybliżenie obrazu w kamerze bezczynności, animacje skradania się a nawet kradzież kieszonkową (choć nie samą umiejętność skradania się ─ to pozostaje wciąż do zrobienia), podgląd portretu przy wyborze twarzy pokazuje jedynie głowę (akurat ja wolałem formę popiersia, ale no cóż…), dodana została opcja usuwania zwłok, poprawiono renderowanie drzew z bloodmoon. Poza tym rozwiązał zwały rozmiarów gór lodowych błędów, a na dodatek jego gałąź graphics została zmergowana ─ nie pytajcie cóż za zmiany zostały w ten sposób wprowadzone, długo by wymieniać; to przecież aż 75 patchów.

Pozostali programiści również rozwiązali całe chmary błędów.

Gus pracuje aktualnie nad AI. Idzie mu to całkiem nieźle, npc potrafią już przejść do najbliższego punktu sieci ścieżek.

Blunted2night dalej pracuje nad dziennikiem, prace nad edytorem również posuwają się do przodu w zadowalającym tempie.

Pvdk zaprezentował nam odświeżony starter OpenMW z opcją importowania ini i nowym wyglądem. 

A zespół Tamriel Rebuilt właśnie usunął tekstury bmp oraz tag z TRdata. I to bardzo dobra wiadomość!