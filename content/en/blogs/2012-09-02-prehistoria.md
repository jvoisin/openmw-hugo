{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-09-02T17:28:39+00:00",
   "title": "OpenMW 0.17.0 to już prehistoria! Czas na informacje tak gorące, że aż rozpalone do białości!",
   "type": "post",
   "url": "/2012/prehistoria/"
}
Wypróbowaliście już OpenMW 0.17.0? Podobało się wam? Mogło się wam nie podobać z racji na to, że akurat to wydanie nie zawiera zbyt wielu spektakularnych nowych funkcji ale jednak należy podkreślić, że to co wydarzyło się w trzewiach silnika można określić jako przełomowe. 

I to wcale nie jest jakaś tam moja gadanina &#8212; wystarczy spojrzeć na otwarte dla 0.18.0 zadania. Nie przypominam sobie byśmy mieli równie bogate możliwości od czasów&#8230; 

Chyba jednak nigdy nie mieliśmy, aż takiego urodzaju. :-)

Można dodać nowe umiejętności. Wszystko jest gotowe by poprawnie zaimplementować kupiectwo, w pozostałych przypadkach może brakować formuł ale jest z nami Hrnchamd (zawodnik wagi superciężkiej &#8212; to właśnie on stworzył MCP) i pomoże je rozwikłać. Wiele wzorów zdążyło zostać odkrytych i czeka potulnie w cieplutkiej niczym zimowa pierzyna wikipedi.

Gdyby komuś zamarzyło się zaimplementowanie awansu na wyższy poziom doświadczenia to droga jest wolna, wszak umiejętności mogą być już rozwijane &#8212; przynajmniej te spośród nich które są zaimplementowanie, i choć może nie jest ich wiele to sednem sprawy jest sama obecność mechanizmu, a nie skala jego wykorzystania.

Jeśli jeszcze tego mało to co powiecie na animacje? Przykładowo teraz już możemy dodać animację chodzenia, a być może również biegania. 

Oczywiście można również zaimplementować odpoczynek i czekanie, podobnie jak rozmaite elementy GUI który był zaniedbywane zdecydowanie zdecydowanie zbyt często. Okno treningu lub kupowania zaklęć czekają, w przeciwieństwie do okna skrótów klawiszowych bo te zdążył już zaimplementować scrawl.

Oprócz tego scrawl dokonał licznych poprawek naprawiając błędy o zróżnicowanej istotności oraz wprowadził nową funkcję: ukrywanie wszystkich elementów GUI po naciśnięciu klawisza F12.

Greye o którym już wielokrotnie wspominałem, co prawda przedstawił się nam jako specjalista od AI, ale ponieważ OpenMW nie posiada absolutnie niczego związanego ze sztuczną inteligencją zajmował się innymi zadaniami. Do teraz. 

Pierwszym zadaniem związanym z AI jest ładowanie potrzebnych informacji z plików. Dzięki pracy Greye jest to już możliwe.

Dgdiniz to z kolei nowy deweloper, który dopiero co skończył ze swoim pierwszym zadaniem &#8212; już chyba tradycyjnie wszyscy zaczynają od refaktoryzacji jakiegoś kawałka kodu, w tym wypadku chodziło o przeniesienie Sounds do Action. Poza tym programista dał się poznać jako dżentelmen o nienagannej net-etykiecie oraz pochwalił czytelność i jakość naszego kodu. 

Można by to potraktować jako lizusostwo ale nie jest to pierwsza osoba która o tym wspomina. To efekt ciągłej refaktoryzacji oraz pracy ziniego który kontroluje projekt z precyzją zegarmistrza. 

Zadanie zostało ukończone, a Dgdiniz zabrał się za implementację okna treningu. Dokumentacja MyGUI jest daleka od ideału, co jest tym gorsze, że biblioteka ma duże możliwości i wiele funkcji. Z drugiej strony dorobiliśmy się już własnych ekspertów którzy mogą służyć poradą i wskazówką.

Zini robi to samo co zwykle: włącza kolejne gałęzie do main i sprawdza czy po zakończeniu operacji OpenMW dalej działa&#8230; ;-) Znalazł też chwilę by rozwiązać kilka błędów.

By włączyć nową, poprawioną kolizję głównej gałęzi trzeba wpierw usunąć stare rozwiązania (kinematic controller oraz fizyka postaci), więc jhooks1 zajął się naprawą schodów. 

Nie, nie! Nie tych na drugie piętro! 

Schodów w grze. W wielu przypadkach nie sposób na nie wejść &#8212; kłopotliwy jest zwłaszcza ostatni stopień. Bez obaw, wkrótce powinno się to zmienić&#8230;