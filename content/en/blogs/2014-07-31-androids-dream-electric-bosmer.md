{
   "author": "Okulo",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-07-29T11:55:14+00:00",
   "title": "Do Androids Dream of Electric Bosmer?",
   "type": "post",
   "url": "/2014/androids-dream-electric-bosmer/"
}
As mentioned in an earlier newspost, the main quest is now finishable in OpenMW. But there are so many ways to go from here, and one of those is Solstheim. Scrawl pulled a Star Wars and mentioned he had &#8220;a bad feeling about this&#8221;, but it turned out that any bugs that popped up could be fixed pretty easily&#8230; so far. One of our active testers, Bahamut, discovered that [the spawn rate of some wildlife was set just a tad too high][1].

One of the many questions people ask when OpenMW pops up in the conversation is &#8220;what does it provide that Morrowind doesn&#8217;t?&#8221; And yes, of course you can start up the whole spiel about open source software being extremely moddable and whatnot, but here&#8217;s something different: What about more platforms? Not only does the team support OpenMW on Linux and OSX, but one of our community members, Sandstranger, has been working on his own unofficial side-project. OpenMW on Android! [He already has it running][2], and at the time of writing, he&#8217;s working on getting all the terrain to work right. Good luck, sandstranger. We&#8217;re all sure it&#8217;s going to be awesome.

Morrowind is chock full of scripts, but sometimes they aren&#8217;t written as well as they should. Zini gave an example that everyone who has ever used the console in Morrowind should be familiar with. A player can give himself the Fire Bite spell by typing _player -> addSpell &#8220;fire_bite&#8221;_ in the console. Sometimes, in our travels, we encounter interesting variations on that. For example, _player -> addSpell &#8220;fire_bite&#8221;, 654_. What does that 654 mean? Nobody knows. Morrowind would just ignore that unnecessary argument, but OpenMW does read them and says something is wrong with this script. If any of you has an idea what these extra arguments mean, why don&#8217;t you join [the discussion on this topic][3]? For now these &#8220;stray arguments&#8221; are worked around, but it&#8217;s not pretty, so if you have anything to add, please do!

And for a few final tidbits, we&#8217;ve got a new feature added. A difficulty slider! If you thought Morrowind was just a bit too hard or too easy, don&#8217;t worry. We&#8217;ve got you covered in that area too. The NPCs will also now properly greet you. As if Fargoth telling you he had the feeling you would become great friends wasn&#8217;t creepy enough, all NPCs would continue staring at you as you walked past. That&#8217;s no longer the case. Oh yes, and they will blink now, too.

Well, that about wraps it up for this week. See you all next time.

##### [Want to leave a comment?][4]

 [1]: http://forum.openmw.org/download/file.php?id=328&mode=view
 [2]: https://forum.openmw.org/viewtopic.php?p=25795#p25795
 [3]: https://forum.openmw.org/viewtopic.php?f=6&t=2262
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=2288