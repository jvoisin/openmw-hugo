{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-09-04T12:42:08+00:00",
   "tags": [
      "animation",
      "configuration",
      "jhooks",
      "launcher",
      "lgromanowski",
      "mwrender",
      "pvdk",
      "swick"
   ],
   "title": "Success story continues™",
   "type": "post",
   "url": "/2011/success-story-continues™/"
}
Hi!

The good thing happened: mwrender refactoring passed trough bug-hunting phase! Final Now swick can do actual job and hopefully finish his task. As I said, this task is very important since all further rendering stuff requires it completed.

jHooks started to work on beasts. Tails works (well, sort off but there is a progress) and I believe strongly that jhooks will be able to make everything working right.

Pvdk is working on the launcher to get bug fixed. Although it probably will take some time to finish maybe we will be able to release 0.11.0 then! 

Lgro started to improving our configuration system. There are at least few things needed to be done in this area and hopefully when 1.0.0 be released this will prevent massive user headache.

I guess that it&#8217;s all. Not much but in the right direction and soon we will see more.