{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized",
      "Week's review"
   ],
   "date": "2013-04-21T18:46:31+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-48/"
}
Hi,

As you may already know, we are getting ready for **0.23.0**. This is going to be another big version with lots of new features and bugfixes. Unfortunately it also appears to be **more difficult than usual**.

I&#8217;m quite sure that **0.23.0** would be here already, if we wouldn&#8217;t have been stopped by two nasty crash bugs. The first one was hard to find but easy to fix for scrawl, the second one is more cryptic and was experienced only by **Chris**. Finding it proved to be a difficult task but somehow **gus** managed to fix it, saving the AI feature from being cut out of 0.23.0.

But we are already thinking about **0.24.0**. Hopefully this time we will be able to add animation layering and weapon rendering so to-do for **0.25.0** will just contain one line:

  * everything that was left for 1.0.0, with no particular order

or something like that…

Meanwhile **Chris** improves our nif support, especially in the area of particles that still lack many features present in Morrowind&#8217;s engine. **Scrawl** works in multiple parts of the engine fixing glitches in the master branch. Glorf decided to solve a few additional bugs, which is always welcome.

Editor is also progressing, just a little slower this time since **Zini** is busy with merging and getting ready for 0.23.0. At least we have a brand new cell table.