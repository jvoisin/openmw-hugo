{
   "author": "Okulo",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-08-06T19:49:25+00:00",
   "title": "Wake up call",
   "type": "post",
   "url": "/2014/wake-call/"
}
Okay, first off, a message from the voluminously voiced WeirdSexy.



Man, you gotta love that resolved bugs list. Makes me smile whenever I see it, at least. But let&#8217;s move on to what happened in the past week.

Well, last week some websites picked up on the Android port that one of our community members was doing as his own sideproject. The articles written on it mostly (incorrectly) claimed that the port was officially part of OpenMW &#8211; one even said that the Android port _was_ OpenMW. All this attention caused some undue stress for the poor guy in question, so let&#8217;s explain what is happening here, just so everyone is clear on what this Android deal is.

OpenMW is an open-source project. This means that everyone can take the current code and do with it whatever they please. Think of it as if you&#8217;re writing a book and everyone is reading along as you&#8217;re writing. People are free to take the story you have written so far and create their own story or make some changes so that the story is more to their liking. In a way, you have your book (which is the &#8220;main story&#8221;) and the books of others (with their stories that have &#8220;branched off&#8221;). Suppose you would read someone else&#8217;s variation and you liked it. What you then could do is take these changed scenes and incorporate them into your own book (merging the two works, as it were), while still continuing on your own story.

That&#8217;s what happening with OpenMW too. SandStranger took the code of OpenMW and made some changes to be able to port it to Android. From time to time he will show the code to the maintainers of the OpenMW project, and they will incorporate it into the main branch. That doesn&#8217;t mean that OpenMW is officially working on the Android port, but there are people out there making an effort. The work is being done, so why not accept the work into the main project?

So in short, the Android port is not officially OpenMW, but the contributions to that end are very welcome.

Now that we have that out of the way, let&#8217;s move on to other stuff.

A few new features have been added. If you have tried OpenMW, you will undoubtedly have noticed that in a stunning display of Bizarro ventriloquism, everyone in Vvardenfell is capable of talking with their mouths wide open, but without moving their lips. Well, that show is over. Everyone is now properly flapping their gums when they are talking. They also now understand that if they are your follower, you may accidentally hit them in combat. So rather than immediately turning hostile, they will let a certain amount of hits slide. There has been change on the enemy&#8217;s side of combat as well: If you&#8217;re already in combat with an NPC, they won&#8217;t (continuously) report you.

On the editor front, some headway has been made as well. Tooltips have been added to graphical buttons, so if you don&#8217;t know what a button does, you can just hover over it and a short description will be shown. Some discussion is now happening on the forums [concerning icon design][1].

There&#8217;s also a discussion going on about <a href=https://forum.openmw.org/viewtopic.php?f=2&t=2292>the fonts</a> we use in OpenMW. Markelius&#8217;s opening post in the thread should give you a nice and detailed overview on the subject matter, but the long and short of it is that we need a font that (1) has an open license, (2) looks properly like the font that Morrowind uses, (3) has support for international characters and probably some more stuff. Just check out the thread and weigh in if you think you know something useful that we don&#8217;t.

Well, that&#8217;s it for this week. Check back next week for the latest news on the project!

##### [Want to leave a comment?][2]

 [1]: https://forum.openmw.org/viewtopic.php?f=7&t=2064
 [2]: https://forum.openmw.org/viewtopic.php?f=38&t=2308