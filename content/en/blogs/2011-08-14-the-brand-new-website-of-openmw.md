{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-08-14T14:05:59+00:00",
   "tags": [
      "animation",
      "forum",
      "jhooks",
      "lgromanowski",
      "mwrender",
      "refactoring",
      "website"
   ],
   "title": "The brand new website of OpenMW",
   "type": "post",
   "url": "/2011/the-brand-new-website-of-openmw/"
}
<span>Good day and welcome on our brand new website in brand new openmw.org domain! </span>

Since Lordrea went MIA and our wiki was still dead we had no choice but create new site, with new administrator.

Wiki is partly restored and We hope that We will be able to rewrite some parts (that required rewrite anyway).

All credits goes to Lgro who did this excellent job on his own. Currently He is importing all posts from old forum to archive on new forum so He is still busy I guess.

The look of both openmw.org and openmw.org/forum may be altered in future but I guess that you will notice it when the time will come.

The old forum is closed? we can&#8217;t make redirection to openmw.org or ban posting on old forum, so please: just forget about it.

When it comes to coding? As usual jhooks rules the universe: the performance killing bug was fixed and fps are now ok. Actually it took less time than I thought. The hopefully last problems to fix:

quoting jhooks

> NPCs are positioned higher than normal. This has to do with the npc positioning code, which is composed of one track of the bip01 transformations applied to standard ogre animation.
> 
> Gauntlets aren&#8217;t positioned correctly. Normal body part hands are though. If I change the translation and rotation offsets for gauntlets it should fix this.
> 
> &#8230; and robes

I must admit that jhooks is our official MW animations guru.

MWrender refactoring turned into debugging? Since man is not a machine and makes errors every code written by human probably have errors. The real problem is that it&#8217;s easier to look for a bug in smaller portion of code so If bug shows up after making any change in code it&#8217;s better to fix it first to save time later.

And yes: inventory GUI is progressing again. I forgot about it but this is the big thing indeed. It should reach usable state soon.

OpenMW 0.11.0 is coming but I have no idea how much time it may take to finally release this version.

I guess that this is it. See you next week.