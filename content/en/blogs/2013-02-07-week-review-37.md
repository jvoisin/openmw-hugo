{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-02-07T16:59:12+00:00",
   "title": "the week in review",
   "type": "post",
   "url": "/2013/week-review-37/"
}
Welcome once again folks.

Chris is still fighting hard to finally integrate physics and animations subsystems, just like the last week. It&#8217;s really hard to tell how many things need to be done before final success but Chris just won&#8217;t give up; he&#8217;s just not that type of guy.

However, Chris isn&#8217;t the only active developer of OpenMW. For instance, Scrawl works on new water ripples effects that are visible when the character is moving through the water. Here is the test performed by our Master of Shaders and Everything That Is [Pretty][1] (Performance hit is marginal.)

Blunted2night continues to work on the journal while Wheybags implemented additional missing variables (like OnPCEquip, that is used in the keening and sunder scripts).

And as usual we had some bug fixes. There is also a small amount of work on the editor. Mike-sc started implementing &#8220;open file&#8221; option.

We are getting ready to publish the new 0.21.0 quite soon. It won&#8217;t have great new features but it will definitely be worth a try!

 [1]: http://scrawl.bplaced.net/perm/water.png "water"