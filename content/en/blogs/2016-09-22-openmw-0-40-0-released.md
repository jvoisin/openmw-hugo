{
   "author": "raevol",
   "categories": [
      "Release"
   ],
   "date": "2016-09-22T21:05:40+00:00",
   "title": "OpenMW 0.40.0 Released!",
   "type": "post",
   "url": "/2016/openmw-0-40-0-released/"
}
The OpenMW team is proud to announce the release of version 0.40.0! Grab it from our [Downloads Page][1] for all operating systems. This release brings some small new features, but mainly bug fixes and improvements. We&#8217;re winding down on implementable features, these are exciting times for OpenMW, even if releases have a little less user-visible changes. The game is basically fully playable, and future releases will bring mostly more fixes and optimizations.

Check out the [release video][2] and the [OpenMW-CS release video][3] by the prodigious Atahualpa, and see below for the full list of changes.





**Known Issues:**

  * Shadows are not re-implemented at this time, as well as distant land
  * To use the Linux targz package binaries Qt4 and libpng12 must be installed on your system

**Changelog:**

  * Implemented NPC &#8220;Face&#8221; function
  * Implemented weather dependent sun/moon reflections
  * Implemented effect removal for abilities, fixes locked player movement upon exiting portable house mods
  * Implemented deletion of moved references
  * Fixed possible reference duplication when a mod modifies the corresponding refID
  * Fixed creatures in cells without pathgrids not wandering
  * Fixed death events to trigger at the end of the death animation instead of the beginning
  * Fixed timed effects not to be interrupted when resting
  * Fixed constant effects not to be put on hold when resting
  * Fixed some issues with how Magicka is calculated in regard to Fortify Magicka effect and Intelligence
  * Fixed several issues with telekinesis interaction
  * Fixed issues with how the &#8220;#&#8221; character was handled in directory names in the config
  * Fixed Slowfall spell to now stop forward momentum when jumping
  * Fixed not being able to replace parent cell references with a cell reference of different type
  * Fixed Cliff Racers not being able to attack targets below them
  * Fixed creatures and NPCs not being able to aim ranged attacks vertically
  * Fixed the player spawning at the origin if the cell they saved in doesn&#8217;t exist anymore
  * Fixed crash that could occur when using teleportation spells
  * Fixed ESS-Importer not separating item stacks
  * Fixed NPCs forgetting they had talked to the player before for their voiced dialog
  * Fixed NPCs not looping their AiTravel assignments
  * Fixed NPCs wandering to invalid locations after training
  * Fixed NPCs wandering off their pathgrid when player waits
  * Fixed &#8220;StopCombat&#8221; AI command not functioning correctly
  * Fixed Larienna Macrina not correctly stopping combat after killing Hrelvesuu in the &#8220;Battle at Nchurdamz&#8221; quest
  * Fixed monsters respawning when loading a quicksave
  * Fixed a visual effect playing in Dagoth Ur&#8217;s chamber that shouldn&#8217;t be there
  * Fixed blight weather still occurring after killing Dagoth Ur
  * Fixed new dialog in a mod inheriting some data from the dialog it replaces
  * Fixed actors that start the game dead always using the same death pose
  * Fixed NPCs to not auto-equip ammunition
  * Fixed NPCs not favoring ranged weapons
  * Fixed NPCs detecting the player when they don&#8217;t have line of sight
  * Fixed ashmire particles rendering outside of their pool
  * Fixed AiWander start time resetting when saving/loading
  * Fixed 1st and 3rd person camera not converting from .ess correctly with ESS-Importer
  * Fixed issues caused by the idle camera when the player is paralyzed
  * Fixed nearby monsters who have not detected the player not preventing the player from resting
  * Fixed an issue with the victim of a pickpocket not always being alerted
  * Fixed dead NPCs and creatures contributing to Sneak skill increases
  * Fixed weather dependent dialog playing in interior cells
  * Fixed effects cast by summons persisting after their death
  * Fixed an issue with parallax map rendering
  * Fixed level up graphic to match vanilla game behavior
  * Fixed segfault in Atrayonis&#8217;s &#8220;Anthology Solstheim: Tomb of the Snow Prince&#8221; mod
  * Fixed issues with invisibility not dispelling correctly
  * Fixed the first couple of seconds of NPC speech being muted on some PCs
  * Fixed being able to stack effects from identical magic scrolls
  * Fixed 3rd person camera distance getting stuck
  * Fixed AiFollow and AiEscort durations to be handled as in-game hours instead of seconds, as per the vanilla engine
  * Fixed a couple of issues with how fatigue is displayed when the player has fortified strength
  * OpenMW-CS: Implemented multiple deletion of subrecords
  * OpenMW-CS: Reimplemented pathgrid rendering
  * OpenMW-CS: Implemented configurable key bindings
  * OpenMW-CS: Implemented pathgrid editing
  * OpenMW-CS: Fixed Start Scripts table not updating when a script is added
  * OpenMW-CS: Fixed tooltips not showing in scene view unless holding mouse button on them
  * OpenMW-CS: Fixed tab width in Script Editor to be 4 characters instead of 4 pixels
  * OpenMW-CS: Fixed being able to create duplicate pathgrid records
  * OpenMW-CS: Fixed data in Weapon records not being properly set
  * OpenMW-CS: Fixed Regions table to show weather options and allow editing them

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=3754" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://openmw.org/downloads/
 [2]: http://www.youtube.com/watch?v=avPbctre9Qw
 [3]: http://www.youtube.com/watch?v=izlm2CAnCpY