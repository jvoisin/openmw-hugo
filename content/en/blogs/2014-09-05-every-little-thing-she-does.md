{
   "author": "Okulo",
   "categories": [
      "Uncategorized",
      "Week's review"
   ],
   "date": "2014-09-05T14:22:42+00:00",
   "title": "Every little thing she does",
   "type": "post",
   "url": "/2014/every-little-thing-she-does/"
}
We&#8217;re in lock for version 0.32.0. That means that release candidate packages are being built for each platform and any bugs and other problems are cleaned up. 

So what got in, last-minute? The most significant news is that the Light spell now works, creating light as it should. This in and of itself isn&#8217;t very exciting, but what is worth mentioning about it is that it was the last magic spell to be implemented. That&#8217;s right &#8211; all magic effects are in! This feature was the second oldest issue on the tracker (#47, right between Creature Animation and NPC Dialogue System &#8211; the newest issue is numbered #1857, just to give you an idea), marking another big leap in OpenMW&#8217;s completion.

In fact, feature-wise there isn&#8217;t all that much left. If we take our list of open features and filter out the editor jobs and everything post-1.0, [we can see][1] that this project has less than thirty features left to implement.

Of course, that doesn&#8217;t mean that 1.0 is right around the corner. There are over 200 issues to be worked through before that, after all. But those 200 issues are mostly bugs and if you&#8217;ve seen our last release video, you know that our development team has people on board that squish bugs with an impressive pace. (In fact, personally I am quite convinced that Scrawl in particular is [secretly a robot][2].)

So when will 0.32.0 arrive? Very soon. The team is in the final stages of pushing the release out, so hang in there. Soon you&#8217;ll be wizarding like Myrddin.

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2393" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://bugs.openmw.org/projects/openmw/issues?c[]=project&c[]=tracker&c[]=status&c[]=priority&c[]=subject&c[]=assigned_to&c[]=updated_on&f[]=status_id&f[]=tracker_id&f[]=fixed_version_id&f[]=subject&f[]=&group_by=&op[fixed_version_id]=!&op[status_id]=o&op[subject]=!~&op[tracker_id]=%3D&per_page=50&set_filter=1&utf8=%E2%9C%93&v[fixed_version_id][]=3&v[subject][]=editor&v[tracker_id][]=2
 [2]: http://www.impactlab.net/wp-content/uploads/2011/03/robot.jpg