{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-11-15T19:27:41+00:00",
   "title": "Błyszczące skały",
   "type": "post",
   "url": "/2016/blyszczace-skaly/"
}
Po ogłoszeniu OpenMW w wersji 0.39 jakieś 5-6 miesięcy temu, garstka użytkowników została powitana przez grę niemiłą niespodzianką – błyszczącymi skałami. Ale powiedziano im, że nie jest to tak do końca błąd. Wszystko działało zgodnie z zamierzeniami.

[<img loading="lazy" class="wp-image-4759 alignnone" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/WAaXCcy-300x188.jpg?resize=699%2C438&#038;ssl=1" alt="waaxccy" width="699" height="438" data-recalc-dims="1" />][1]

Co do tego doprowadziło? Morrowind został wydany w roku 2002, kiedy to techniki teksturowania nie były tak zaawansowane jak dzisiaj, i nie było normalnych map. Lata mijały, a społeczność gry tworzyła coraz więcej złożonych modów na silniku, który był zamrożony w stanie sprzed dziesięciu lat. W pewnym momencie granice możliwości silnika zostały osiągnięte i nie można już było stosować nowszych technologii.

Jako że silnik Morrowinda nie ma otwartych źródeł, nie można było po prostu unowocześnić jego kodu, by potrafił sprostać nowoczesnym standardom. Możliwe natomiast było zhakowanie silnika poprzez pozyskiwanie z niego informacji i tworzenie wstrzykiwaczy kodu (code injectors). Tak powstał uwielbiany przez wielu MGE XE distant lands, Morrowind Script Extender, oraz nie mniej ważny Morrowind Code Patch. Mody te bardzo dobrze służyły społeczności przez długi czas, ale miały też swoje skutki uboczne, takie jak crashe, niestabilność, wycieki pamięci i dziwne metody teksturowania.

Do tego ostatniego przyczynił się w szczególności Morrowind Code Patch, który jest także powodem naszych błyszczących skał. MCP wprowadza rodzaj „fałszywego” mapowania wypukłości poprzez używanie mapy otoczenia, po to by dać teksturom wrażenie zmapowanych wypukłości. Żaden inny silnik nie robi tego w ten sposób – gdyby ktoś chciał przeportować w ten sposób „fałszywie” zmapowanego mesha na silnik Unity, Skyrima lub OpenMW, to otrzymałby właśnie takie błyszczące dziwactwo.

Co więc zrobimy z tymi błyszczącymi skałami? Wyrzucimy je? Nie! Na szczęście jest przed nimi druga szansa. Mamy szczegółowy [poradnik][2] autorem którego jest Lysol, opisujący proces ich naprawy. Większość map wypukłości da się naprawić w kilku prostych krokach, które powinien być w stanie wykonać nawet przeciętny gracz bez doświadczenie moderskiego.

[<img loading="lazy" class="wp-image-4758 alignnone" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/mAjCbzS-300x188.jpg?resize=703%2C440&#038;ssl=1" alt="majcbzs" width="703" height="440" data-recalc-dims="1" />][3]

Jedynie autor moda ma prawo do uploadu swojej własności, tak więc każdy użytkownik, który chciałby używać Twojego moda musi powtórzyć ten proces. Jeśli jesteś autorem, zachęcamy do poświęcenia paru chwil na aktualizację moda, by działał poprawnie z OpenMW. Jeśli masz mało czasu, możesz napisać post na naszym forum i pozwolić użytkownikom na aktualizację – a nuż ktoś się tym zajmie. Przegońmy te błyszczące skały z Vvardenfell!

##### Zapraszamy do komentowania [tutaj][4].

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/WAaXCcy.jpg?ssl=1
 [2]: https://forum.openmw.org/viewtopic.php?f=40&t=3922
 [3]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/mAjCbzS.jpg?ssl=1
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=3935