{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-02-24T10:50:26+00:00",
   "title": "Oda do testów",
   "type": "post",
   "url": "/2014/oda-testow/"
}
Nie ma OpenMW 0.29.0. Problemem są błędy, które (tradycyjnie) wyszły na jaw dopiero przy okazji wydania. Nie chcę nawet zagłębiać się w szczegóły, ale jeśli ktoś potrzebuje przykładu na poparcie tezy o kluczowej roli testów ─ oczywiście OpenMW stanowi ten przykład.

Na szczęście poza wspomnianym, feralnym wydaniem 0.29.0, sprawy idą w dobrym kierunku. Scrawl przykładowo, próbuje ugłaskać twierdzących, iż w trakcie przerw na ładowanie przeczytali &#8220;Wojnę i Pokój&#8221;. Najnowszy pomysł na usprawnienie procesu polega na wielowątkowym ładowaniu terenu ─ oczywiście, gdy tylko dokonamy odpowiednich pomiarów, przedstawię rozkład prawdopodobieństwa przeczytania &#8220;Braci Karamazow&#8221; w trakcie przerw na ładowanie OpenMW do roku 2020. Z racji jednak na wciąż znaczący okres oddzielający nas do końca dekady, należy zaznaczyć możliwość dalszych zmian szybkości ładowania w przeciągu kolejnych lat rozwoju projektu. Zmiany te mogą podważyć wiarygodność naszych wyliczeń na bazie stanu obecnego.

Pvdk z kolei, próbuje dogodzić innym malkontentom i abstynentom nie tolerujących wine w systemie. Wine co prawda umożliwia instalację Morrowinda, jednak trzeba przyznać, że instalowania go tylko w tym celu zdaje się być przesadą. Wszak wystarczy wypakować archiwa gry ─ istnieje nawet do tego użyteczna biblioteka. Jak wspominałem w minionych tygodniach, Pvdk tworzy przyjazny, graficzny instalator plików Morrowinda. Instalator ten wzbogacił się w nowe funkcje: nie pozwoli zainstalować bloodmoon przed tribunalem, prawidłowo zapisze plik morrowind.ini, a także wzbogacił się o inne, pomniejsze ulepszenia.

OpenCS z kolei doczekał się nowej funkcji. Wprowadziłem możliwość automatycznego tworzenia filtrów metodą przeciągnij i upuść. Wyrażenie jest tworzone tak, by można je było dopasować do każdej kolumny mogącej zawierać odpowiedni rekord. Przykładowo, jeśli przeciągnięcie rekord frakcji, dajmy na to gildii wojowników, do pola filtru tabeli &#8220;Referencables&#8221; automatycznie pojawi się filtr dzięki któremu tabela będzie wyświetlać wyłącznie bohaterów niezależnych (NPC) należących do gildii wojowników. Przyśpiesza to dość wyraźnie pracę z edytorem, nawet jeśli dość szybko piszecie na klawiaturze; nowa metoda tworzenia filtrów jest sprawniejsza. Możliwości tej funkcji są nieco szersze niż przedstawiony przykład, i z czasem zostaną opisane w tekście naszej instrukcji.

PS  
Zapomniałem wspomnieć o tym, że byliśmy (a przynajmiej raevol był) na SCaLE, zapraszam do [forum][1] gdzie znalazł się krótki opis całego wydarzenia.

 [1]: https://forum.openmw.org/viewtopic.php?f=4&t=2053