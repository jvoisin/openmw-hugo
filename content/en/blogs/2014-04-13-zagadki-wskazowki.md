{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-04-13T18:34:28+00:00",
   "title": "Zagadki, wskazówki…",
   "type": "post",
   "url": "/2014/zagadki-wskazowki/"
}
Witajcie ponownie!

Jak zapewne już zauważyliście, ostatnio na blogu dość regularnie pojawiają się doniesienia o kolejnych zmianach w zakresie obsługi sztucznej inteligencji. Nie powinno to nikogo dziwić, bowiem zapewniam: wciąż jest co poprawiać i ulepszać. Zwłaszcza algorytmy wyszukiwania trasy (tak zwany &#8220;pathfinding&#8221;) potrafią zachowywać się w sposób wręcz przezabawny – wierzcie lub nie: npc w OpenMW zachowują się nieraz nawet dziwniej niż w oryginalnym silniku. Jeśli gustujecie w tego typu błędach, polecam pośpieszyć się z testowaniem – z tygodnia na tydzień jest coraz mniej sytuacji gdy boki można zrywać, a wkrótce najpewniej ledwie okazjonalnie drgnie nam ku górze kącik ust. Tym razem, do wprowadzenia nudy i przewidywalności we wspomnianej dziedzinie przyczynił się cc9cii, za co (mimo wszystko) należą mu się brawa.

Poza tym, w OpenMW nie dzieje się oszałamiająco wiele. Naprawionych zostało sporo błędów, ale jeśli mam być szczery, a chcę być szczery – to nigdy nie wydawało mi się to naprawdę interesujące. Za to OpenCS rozwija się ciekawie.

Zacznijmy może od cellselect: jest to gałąź zawierający kod powiązany częściowo z funkcją okna renderowania komórki. Oczywiście całość jest daleka od stanu gotowości bojowej, ale ostatnio zmiany zostały już włączone do gałęzi main, zatem można je wypróbować. Do czego, rzecz jasna, zachęcam. Brak listy zmian możecie potraktować jako dodatkową sugestię…

Oprócz tego w panelu mapy regionów pojawiło się zupełnie nowe menu kontekstowe pozwalające na… 

Tak, to też potraktujcie jako wskazówkę dotyczącą moich oczekiwań wobec was, moi drodzy czytelnicy. Pamiętajcie jednak, że nowa mapa regionów nie jest jeszcze w main.