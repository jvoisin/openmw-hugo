{
   "author": "raevol",
   "categories": [
      "Release"
   ],
   "date": "2013-08-05T09:55:12+00:00",
   "tags": [
      "release"
   ],
   "title": "OpenMW 0.25.0 Released!",
   "type": "post",
   "url": "/2013/openmw-0-25-0-released/"
}
The OpenMW team is proud to announce the release of version 0.25.0! Grab it from our [Downloads Page][1] for all operating systems. With the implementation of SoundGen records and the Wander AI Package, this release introduces stomping NPCs. Note that AI Wander must still be called from the console. SoundGen implementation also means that Creatures make noises. Prepare for squealing scrib nostalgia. SDL2 has also been implemented, which stomps out a lot of mouse focus and other UI bugs. Check over the changelog for the rest of the changes in this release.

**Known Issues:**

  * Extreme shaking may occur during cell transitions for some users (enable anti-aliasing as a possible workaround)
  * Polish version of Morrowind.esm makes OpenMW crash
  * Pressing &#8216;j&#8217; key during startup freezes the game

Check out the release video</a> by the spectacular WeirdSexy:  


**Changelog:**

  * Implemented SoundGen for NPCs and creatures
  * Implemented AI Package: Wander
  * Implemented 64-bit compatibility for OS X
  * Implemented Hardware mouse cursors
  * Implemented first person animations
  * Implemented mouse wheel zoom in third person mode
  * Implemented Autorepeat for slider buttons
  * Launcher will not start OpenMW when no esm/esp files are selected
  * Fixed launcher crash on OS X < 10.8
  * Fixed a performance drop in the Census and Excise Office
  * Fixed SSCR records not starting correctly
  * Fixed handling script names with hyphen characters
  * Fixed handling the repeat parameter for AIWander
  * Fixed enchanting window to include weapons
  * Fixed character being able to move while over-encumbered
  * Fixed dead keys
  * Fixed mouse not being confined to window when the game is un-tabbed, and other mouse focus issues
  * Fixed ini Importer aborting when there is no existing cfg file
  * Fixed Dead NPC and Creature collision boxes
  * Fixed incorrect sorting of answers in dialogue
  * Fixed ini Importrt core dump when given an unknown parameter
  * Fixed getting stuck in some doors
  * Fix for journal/book rendering
  * Fix for draw weapon sound playing when weapon is readied and cell border is crossed

 [1]: https://openmw.org/downloads/