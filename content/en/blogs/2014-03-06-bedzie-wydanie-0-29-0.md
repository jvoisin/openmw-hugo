{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-03-06T13:12:57+00:00",
   "title": "Będzie wydanie 0.29.0!",
   "type": "post",
   "url": "/2014/bedzie-wydanie-0-29-0/"
}
Ten tekst rozpocznę cytatem:

> The first iteration of the navigation system is now in master. Please test!
> 
> A few points to consider:  
> &#8211; This is only the first iteration. We will most definitely improve the system further.  
> &#8211; Other navigation modes can be added later. We should even consider allowing editor plugins to add navigation modes.  
> &#8211; Input focus handling is a bit wonky at the moment. When opening the scene sub view and after clicking outside the actual rendered scene you will have to click on the scene again, before it accepts keyboard input. Needs further work, but not my priority now.  
> &#8211; Navigation modes can be selected via the button on the left bar. This buttons pops up another bar with 3 buttons, one for each mode. The icons on these buttons are placeholders (I just used some random icons that are already in use elsewhere).  
> &#8211; There are currently several hardcoded values that influence how the navigation system works (e.g. mouse sensitivity). These will be made configurable, once we have a properly working user settings implementation (\*cough\*). Likewise all key-bindings (including mouse buttons) will be made configurable eventually.
> 
> The navigation modes are (from left to right):
> 
> 1. First Person
> 
> &#8211; Mouse-Look while holding the left mouse button  
> &#8211; WASD as usual  
> &#8211; Mouse wheel moves the camera forward/backward  
> &#8211; Holding the left mouse button and control, let&#8217;s you strafe (also vertically)  
> &#8211; Camera is held upright (you can&#8217;t do a backflip or a headstand) 
> 
> 2. Free Camera
> 
> &#8211; Mouse-Look while holding the left mouse button  
> &#8211; WASD let&#8217;s you strafe (also vertically)  
> &#8211; Holding the left mouse button and control, let&#8217;s you strafe (also vertically)  
> &#8211; Mouse wheel moves the camera forward/backward  
> &#8211; Q and E keys makes the camera roll
> 
> 3. Orbiting
> 
> &#8211; You are always facing the centre point.  
> &#8211; The centre point is 0, 0, 0 by default (we will add other methods to set it eventually)  
> &#8211; Holding the left mouse button makes you rotate around the centre point while keeping the distance.  
> &#8211; WASD also makes you rotate around the centre point while keeping the distance.  
> &#8211; The mouse wheel moves you towards or away from the centre. But you can not pass through the centre.  
> &#8211; Q and E keys makes the camera roll  
> &#8211; Holding the left mouse button and control let&#8217;s you strafe and moves the centre point in the same direction and by the same distance.
> 
> In all three modes you can hold down the shift key to perform actions at an accelerated speed.

Innymi słowy, OpenCS właśnie wzbogacił się o możliwość zmiany orientacji kamery. Oczywiście panel renderowania nadal nie jest w stanie wyświetlić zawartości komórki gry – ta funkcja przyjdzie z czasem, ale już teraz widzimy jak bogate możliwości będą w dyspozycji użytkowników.

Tymczasem scrawl, niczym opętany, wrzuca na githuba poprawki powiązane z obsługą terenu. Czyżby ostatnie prace przy ładowaniu ujawniły jakieś niedoskonałości? Brzmi to na tyle podejrzanie, że na wszelki wypadek nie będę drążyć tematu. Podobnie wolę uniknąć znęcania się nad biednym pvdk, który z niezłomną wolą godną mitycznych herosów dąży do obsługi wszystkich możliwych nośników instalacyjnych morrowinda. Grupa ta obejmuje płytki z czasopism jak również wszystkie możliwe wydania pudełkowe – a każda z tych edycja może minimalnie różnić się szczegółami rozmieszczenia plików. Dużo, dość w sumie nudnej; pracy.

cc9cii (wbrew pozorom nie jest to oznaczenie Borga, a jedynie nick jednego z programistów – owszem, jest to istotna różnica) wprowadził szereg poprawek do projektu. Między innymi, dzięki tym dokonaniom…

…**nadchodzi wydanie 0.29.0!** W przeciwieństwie do lgro nie uprawiam propagandy sukcesu więc możecie mi zaufać; nowa wersja jest bliska. Może nawet jeszcze przed początkiem następnego tygodnia, w dziale &#8220;do ściągnięcia&#8221; pojawią się pliki binarne nowego wydania.

Do przeczytania!