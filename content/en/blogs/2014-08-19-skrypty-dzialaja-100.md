{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-08-19T10:07:11+00:00",
   "title": "Skrypty działają w 100%!",
   "type": "post",
   "url": "/2014/skrypty-dzialaja-100/"
}
Osiągnęliśmy kolejny kamień milowy w rozwoju OpenMW. Obsługa skryptów jest już w 100% sprawna!

Skrypty są narzędziem, dzięki któremu mogą być uruchamiane specjalne wydarzenia (np. pojawienie się kogoś/czegoś w konkretnym miejscu, zmiana statystyk postaci, i inne). Jeśli któryś ze skryptów by nie zadziałał, coś mogłoby się nie wydarzyć. Mogłoby to zaskutkować niemożnością ukończenia zadania lub nie działaniem magicznych efektów. Czasami skrypty Morrowinda nie mogą być odczytane przez OpenMW bezpośrednio, na przykład z powodu bezdomnych argumentów („przybłęd”), o których niedawno wspominaliśmy. Na szczęście nie musimy się tym dłużej przejmować, bo obecnie wszystkie 1204(!) skrypty mogą zostać odczytane i wykonane. Jest to ogromny skok naprzód w kierunku działającej implementacji wszystkich funkcji gry. Nie jest więc chyba zdziwieniem, że każdy każdemu przybijał piątkę w atmosferze ogromnej radości ([link][1]) :)

Rozmawialiśmy trochę o sposobach optymalizacji kodu. Jednym z pomysłów jest naprawienie tak zwanych „wycieków pamięci”. Wyciek pamięci polega na zajęciu przez program pamięci w celu wykonania określonych operacji, i nie zwolnieniu jej po użyciu. Jest ona wtedy niedostępna. Program, który „przecieka”, zajmuje coraz więcej pamięci, co prowadzi do spadku wydajności. Niedawno Scrawl znalazł jeden z wycieków, a konkretniej w części interfejsu. Używamy MyGUI jako interfejsu, bo to naprawdę świetny soft. Dużo się mówi, że dzięki MyGUI można przy niskim nakładzie pracy zrobić wspaniały interfejs. Niemniej jednak, wyciek musi zostać zatkany.

I oto dlaczego projekty open-source wymiatają. Scrawl rozmawiał z Altrenem, głównym człowiekiem od MyGUI. Altren zaszczycił nas nawet swoją obecnością. Wygląda na to, że Scrawl będzie pracował trochę bliżej z MyGUI, po to żeby je usprawnić w szybszym tempie, niż miałoby to miejsce normalnie. To powinno zakończyć nie tylko problemy jakie napotyka projekt OpenMW, ale także inne projekty korzystające z tego oprogramowania. W tym celu MyGUI opuściło SourceForge i przeniosło się na Githuba ([link][2]). Jeśli jesteś programistą i chcesz mieć oko na projekt, może nawet przyczynić się do jego rozwoju, to czuj się zaproszony!

W zeszłym tygodniu mówiliśmy o fontach. Może nie jest to jedna najbardziej efektowych rzeczy w OpenMW, ale jest istotna, gdyż pismo jest głównym kanałem komunikacji gry z graczem. Markelius pracuje nad fontem zwanym Open Magicka, który ma wyglądać ładnie i ostro w każdym rozmiarze. Po pierwsze, każdy znak w oryginalnym foncie Magic Cards musi zostać wykonany ponownie. Następnym krokiem jest dodanie wsparcia dla znaków narodowych, jak np. polskie ogonki lub cyrylica. Markelius potrzebuje nieco informacji, szczególnie o językach nie używających cyrylicy ani alfabetu łacińskiego. (Przykładowo, gdyby była wersja japońska gry, a OpenMW miałby ją wspierać, font musiałby obsługiwać japońskie znaki.) Jeśli przypadkiem coś wiesz, prosimy o post [tutaj][3].

Jeszcze jedna nowinka. Jeden z fanów OpenMW, Deonsion, rozpoczał pracę nad pewnym projektem ([link][4]), mającym na celu poprawienie atmosfery gry poprzez utworzenie lepszych modeli i animacji. Obecnie pracuje on nad nowymi modelami skał i drzew, żeby od czegoś zacząć, ale pragnie także pracować nad animacjami, gdy tylko nauczy się używać stosownego oprogramowania. Wiadomo, że domyślne animacje i grafika gry były bardzo modne w roku 2002. Było to dwanaście lat temu, więc przedsięwzięcie jest naprawdę wielkie. Jeśli jest ktoś chętny do pomocy Deonsionowi, prosimy o kontakt! Napisz posta w wątku dotyczącym projektu (link powyżej), i napisz, co możesz zrobić. Każda para rąk chętna do pomocy jest potrzebna. OpenMW jest najlepszym sposobem, byśmy mogli grać w grę wszech czasów ze współczesną grafiką. Razem możemy sprawić, że tak właśnie się stanie!

Zapraszamy do komentowania [tutaj][5]!

 [1]: https://forum.openmw.org/viewtopic.php?p=26476#p26476
 [2]: https://github.com/MyGUI/mygui
 [3]: https://forum.openmw.org/viewtopic.php?f=2&t=2292&start=20
 [4]: https://forum.openmw.org/viewtopic.php?t=2318
 [5]: https://forum.openmw.org/viewtopic.php?f=38&t=2326