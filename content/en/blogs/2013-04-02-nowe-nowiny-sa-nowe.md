{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2013-04-02T16:43:41+00:00",
   "title": "Nowe nowiny są nowe…",
   "type": "post",
   "url": "/2013/nowe-nowiny-sa-nowe/"
}
Mimo, że Natka Natura przygotowała nam niespodziankę na Wielkanoc OpenMW ma się nieźle. 

Przede wszystkim prace nad zaklinaniem przedmiotów postępują naprzód dzięki wysiłkom Glorfa, scrawl zaś pracował odrobinę nad światłami dzięki czemu wyglądają jeszcze lepiej niż wcześniej.

Tak naprawdę scrawla zajmuje jego biblioteka shiny, stworzona by ułatwić i przyśpieszyć tworzenie shaderów. Wraz z wydaniem wersji 1.0 dodana zostanie możliwość uzyskania podglądu shaderów bez konieczności restartowania gry co powinno znacząco umilić testowanie naszego dzieła.

Niestety prace nad AI zostały chwilowo zatrzymane, ponieważ laptop gusa właśnie nieodwołalnie umarł. Co prawda gus kupił już nowy sprzęt ale ustawienie wszystkich bibliotek na windowsie raczej nie należy do kategorii „zadań prostych”, za to z całą pewnością do „grupy czasochłonnych”.

Zini do spółki z graffym tak jak każdego tygodnia pracują nad edytorem.

Tydzień pewnie należało by uznać za jeden z tych mizernych, gdyby nie kolejna chmara bugów poskromiona przez odważnych deweloperów OpenMW. Aktualnie liczba otwartych raportów wciąż wzrasta, więc pracy nie powinno zabraknąć.