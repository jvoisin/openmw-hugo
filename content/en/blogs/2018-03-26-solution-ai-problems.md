{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2018-03-26T17:34:58+00:00",
   "title": "A solution to the AI problems?",
   "type": "post",
   "url": "/2018/solution-ai-problems/"
}
Ever since the AI was implemented, OpenMW has been plagued with a few irritating bugs. [Guards running against walls][1] and [people randomly walking off cliffs][2], among other things. All of these seem to boil down to this: The pathfinding AI needs to be [improved][3]. There has been some attempts to do this, but none of them has come very far. Until now.

Elsid, who previously had just started on a few pull requests in november last year and then wasn’t really heard from, suddenly posted [this][4] massive pull request. Why is it massive, you say?

Well basically, elsid is implementing Detour from a navigation mesh toolset called [recastnavigation][5]. You can read details about it in the link, but to summarize, recastnavigation calculates where the AI can and can’t walk, and then creates a navigation mesh for the AI to use.  
People have already built OpenMW with elsid’s latest commits to try it out, and it seems to work great. 



In the above video, user Gluka tried it out in the Tamriel Rebuilt town of Kragen Mar because that town does not have a path grid yet. The NPC’s are still able to navigate the town without problems.

So _if_ elsid manages to get this to work well without too much of a performance hit (remember, this feature isn’t done and there is still a chance that there are problems that he can’t solve), then this will surely revolutionize OpenMW.

In other news, the shadow branch is still moving forward in a steady pace and there has been a lot of small neat bug fixes and feature implementations merged that will further enhance the experience in the upcoming version 0.44.

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=5058" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://bugs.openmw.org/issues/3524
 [2]: https://bugs.openmw.org/issues/1997
 [3]: https://bugs.openmw.org/issues/2229
 [4]: https://github.com/OpenMW/openmw/pull/1633
 [5]: https://github.com/recastnavigation/recastnavigation