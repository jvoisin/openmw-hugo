{
   "author": "greye",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-07-10T13:27:39+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-59/"
}
Greetings.

&#8220;So &#8230; (almost) everybody on summer vacation already?&#8221; **Zini** asked.  
&#8220;_Already_, hahaha. Its already July.&#8221; was the answer.

As you may know, developers are soooo lazy people and use any opportunity not to work. But few of them still wander here, let&#8217;s praise them.

First, some bug fixes (surprise!): **swick** improved scrolling in gui, **scrawl** fixed nasty X11-related crash.

Next, coc (CenterOnCell) command is pretend to be sane now: character appears near doors or on walkable surface. That also means we can start engine with collisions enabled.

**Zini** still working on editor, and added functionality to work with references.

And the most recent news are **Chris** got first-person animations working! Seems like after that he [decided to hunt a bit][1]. Well, actually we know little about his background&#8230;

 [1]: http://i.imgur.com/aEToYFq.jpg