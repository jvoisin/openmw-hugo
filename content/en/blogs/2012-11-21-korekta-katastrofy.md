{
   "author": "sir_herrbatka",
   "date": "2012-11-21T15:18:18+00:00",
   "title": "korekta katastrofy",
   "type": "post",
   "url": "/2012/korekta-katastrofy/"
}
Ponieważ przez przypadek, w ostatnią niedzielę, wkleiłem tekst w niewłaściwy języku na bloga wywołałem w konsternację, zamęt i dezorientację zarówno czytelników jak i swoją własną. 

Nie wiem jak to się mogło stać. Nie wiem, więc nie pytajcie.

Ale nie ma tego złego co by na dobre nie wyszło: dzięki tej drobnej, miniaturowej niczym piesek w damskiej torebce porażce otrzymacie już teraz zrzut ekranu na który reszta świata poczeka do niedzieli.

http://scrawl.bplaced.net/perm/map.jpg

Widać oczywiście, że wiele pozostało do zrobienia ale spójrzcie tylko jakie to ładne i szczegółowe. Oczywiście metoda jest inna niż w poczciwym Morrowind, zamiast starego mazania teksturami zastosowaliśmy tworzenie obrazu z „lotu ptaka”. Widać nawet budynki w Vivec.

Gus tymczasem zajął się renderowaniem broni. Jest to funkcja której brak nie jest usprawiedliwiony aktualnym stanem OpenMW, tak więc nic też nie stoi na przeszkodzie dla implementacji. Po pierwszych zabawnych pomyłkach mogę powiedzieć, że oręż jest już widoczny w grze… i tylko tyle jak na razie. Wciąż pozostaje do rozwiązania kwestia animacji.

Zini zakończył implementowanie filtrów dialogów których brakowało do tej pory. Oznacza to, że większość z zadań powinna być możliwa do wykonania.

Mark76 powrócił do prac nad naszym wspólnym projektem i ma całkiem niezłe osiągnięcia. Prawdę powiedziawszy to wysyła kod na githuba już od dwóch tygodni ale początkowo obawiałem się, że mam halucynacje. Na szczęście coś faktycznie się dzieje w temacie wsparcia dla wielu plików esm/esp.

Greye z kolei właśnie zakończył prace nad zmianą rasy. Od teraz wszystko ma swoje odbicie w grze, a oprócz tego wiele innych zadań możliwym jest już do rozpoczęcia.

lgro pracował nad książkami. Co prawda zdawać wam się może, że książki już działają w zasadzie bez zarzutu, ale jest to prawdą jedynie w odniesieniu do języka polskiego i angielskiego, niemiecki; rosyjski; litewski; estoński; i takie których nazwy nie sposób wymówić mogą działać lub nie. Tak czy owak na razie tkwimy w miejscu.

Dołączyli do nas nowi programiści: Gohan i trombonecot (co za nick…) zajęli się swoimi zadaniami by mieli szansę zapoznać się z kodem. Powodzenia!