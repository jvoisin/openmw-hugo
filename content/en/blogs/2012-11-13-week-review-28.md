{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized",
      "Week's review"
   ],
   "date": "2012-11-13T18:34:33+00:00",
   "tags": [
      "death"
   ],
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-28/"
}
Voila, OpenMW 0.19.0 is at your fingertips.

Death was a major feature addition in 0.19.0, but it isn&#8217;t fully implemented yet with many missing things in the dialogue code. These will be fixed in time for version 0.20.0. We&#8217;ve got a two point plan to do it:

1) Zini will work on it.  
2) Zini will fix everything.

As you can see it simply must work. ;-)

But other things are working with the death, including the kill counter. So it is possible to complete quests involving the killing of people and or things.

Zini recently added the bounty feature to the player&#8217;s stats. This is similar to disease feature. Now npcs will be able to recognize if you are a wanted criminal and use different greetings accordingly.

Gus worked on bartering, which has been present in past versions but with limited functionality. Previously, the player was not able to bargain for a better price. This is now implemented. Also, thanks to scrawl, now rejected offers will hurt an NPC&#8217;s disposition towards the player character.

Scrawl is working on the persuasion feature. The menu is already done and you can get a disposition bonus after a successful attempt.

Greye is busy with the character model task. Get ready to finally see your character&#8217;s race change! Before even being able to start this feature, Greye had to add some other features in &#8220;player-dynamics&#8221; branch. We love you Greye!