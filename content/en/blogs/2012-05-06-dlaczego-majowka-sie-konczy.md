{
   "author": "sir_herrbatka",
   "date": "2012-05-06T17:29:35+00:00",
   "title": "Dlaczego majówka się kończy? :(",
   "type": "post",
   "url": "/2012/dlaczego-majowka-sie-konczy/"
}
No i po majówce. Zapewne w kraju za jakiś czas odczytają pik wzrostu ilości samobójstw jaki dziś wystąpił, ale będę się nogami zapierał, że to nie nasza wina! My zrobiliśmy swoje!

Przykładowo duet jhooks1/chris osiągnął to do czego dążyli tak długo: system animacji OGRE3d w końcu potrafi obsłużyć animacje nif. W teorii powinno oznaczać to skok wydajności i ułatwienie dla deweloperów, ale w praktyce nie sposób nawet powiedzieć czy są szanse, by te bezdyskusyjne usprawnienie trafiło do nowego wydania OpenMW.

Do nowego wydania trafi za to na pewno nowa fizyka. Zadanie zostało ostatecznie zakończone i tak do OpenMW tylnymi drzwiami wkradł się kod pochodzący z (sic!) Quake 3. Tak tak, to prawda: pmove został napisany w ubiegłym wieku dla Quake 3 i teraz jest częścią implementacji fizyki w OpenMW. Jak na razie nowa fizyka wciąż nie działa idealnie ale znacznie, znacznie lepiej niż stara, a dzięki kolejnym bugfixom i ulepszeniom w końcu powinna bez najdrobniejszego nawet zarzutu. Oprócz tego, że nie zdarzają się teraz przypadki spadania przez podłogę należy wspomnieć o tym, że oprócz tego możemy już przejść się po ziemi bo kolizja z terenem także działa.

Scrawl pracuje nad wieloma zagadnieniami. GUI, bugfixy, fizyka, podpowiedzi, przyśpieszył także ruch w trybie no-clip. 

Gus zaś wciąż pracuje nad ekwipunkiem, a konkretnie nad upuszczaniem (to coś nowego!) przedmiotów.

I na koniec coś specjalnego: jeśli jesteś programistą i znasz Qt4 (albo i Qt5 ;-)) i jak do tej pory nie widziałeś dla siebie miejsca w projekcie to teraz nastała pora byś zmienił swoje nastawienie. Ponieważ wszystkie znaki na ziemi i niebie wskazują na to, że wydamy OpenMW 1.0 szybciej niż mogłoby się wydawać jeszcze pół roku roku temu czas zacząć myśleć nad edytorem. Bez własnego edytora nie sposób wprowadzić nowych funkcji do formatów esp i esm, a bez rozszerzenia możliwości tych plików nie możemy dodać zbyt wielu nowych i długo oczekiwanych opcji na użytek moddingu. Co prawda nie powstrzyma to nas przed ulepszeniem grafiki ale wydanie OpenMW 1.1 wzbogacone względem 1.0 tylko nowymi efektami wizualnymi nikomu nie byłoby w smak.

Tak więc nastał moment w którym możesz dołączyć do naszego zespołu i pomóc nie tylko osobom oczekującym na OpenMW, ale także całej społeczności moderskiej bo nowy edytor, nawet bez OpenMW jako takiego, byłby sam w sobie potężnym narzędziem nie do przeceniania przez osoby które potrafiłyby go użyć.