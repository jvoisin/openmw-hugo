{
   "author": "Okulo",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-12-02T12:39:23+00:00",
   "title": "Wikification",
   "type": "post",
   "url": "/2014/wikification/"
}
Hey people, hope you enjoyed those (belated) release videos. Without WeirdSexy&#8217;s sweet smooth ambrosia voice a release just isn&#8217;t the same. As 0.33.0 was gearing up for release, the OpenMW devteam couldn&#8217;t just leave well enough alone for a few days and started working on 0.34.0. Oh, those workaholics. Not that I&#8217;m complaining, mind you. More fixes for us fans. Let&#8217;s look at a few of them.

You&#8217;d think that with the life many Argonians have in Vvardenfell they wouldn&#8217;t be too unhappy getting rid of their shackles. Apparently they were still quite possessive of them even after you freed them, though. Pick up their bracers from the floor and they&#8217;d act all indignant. Not anymore. Starting 0.34.0 they will give you gratitude rather than attitude.

Some issues have popped up with the Fair Magicka Regen mod. OpenMW successfully reads the script, but the mod contains a bug. [So paging dr. GlassBoy and dr Smoke, there might be an issue with this great thing you guys have got going on][1]. It&#8217;d be appreciated if you could update your mod and push it to the Nexus. :)

Speaking of compatability, my fellow Archonaut Tinker has [added a page to the OpenMW Wiki that is all about which mods work with OpenMW to what extent][2]. Most Morrowind mods _should_ work &#8220;out of the box&#8221; with OpenMW, but it&#8217;d be great to have some confirmation on this in a central spot. So if you can&#8217;t program, but are still enthusiastic about helping out, you can join this burgeoning project and help us build a great big repository of knowledge.

In other news, since our in-house programming machine, Scrawl, needs his daily oil change as well, he has been working on stuff outside of OpenMW. Real life has caught up with him, but if everything checks out, he might well spend a significant amount of time on OpenMW again very soon. That&#8217;d be the best. Scrawl has been a recurring character in the development of OpenMW, so you might imagine how much we miss his presence here.

Another familiar name to our followers (and especially our friends on /r/morrowind &#8211; hi guys and gals!) is Raevol, and he has come up with the idea of getting all features out of the way so we can leave the alpha stage behind us and move on to the beta phase. Currently the team is discussing the upsides and downsides of this plan. You&#8217;ll might well hear more on this subject soon, so stay tuned!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2612" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://bugs.openmw.org/issues/2080
 [2]: https://wiki.openmw.org/index.php?title=Mod_status