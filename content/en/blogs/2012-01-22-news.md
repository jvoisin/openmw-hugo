{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-01-22T16:50:22+00:00",
   "tags": [
      "configuration",
      "Gus",
      "jhooks",
      "journal",
      "lgromanowski",
      "rendering",
      "terrain",
      "water",
      "yacoby"
   ],
   "title": "News!",
   "type": "post",
   "url": "/2012/news/"
}
Journal task is finished for now (It&#8217;s very basic but We don&#8217;t need anything more than that at the moment), GUS is free to focus on the other frontiers, like dialog GUI task, or meshes orientation bug. Good. Very good! Many have fallen in the battle with GUI tasks, so developer who survived and is somewhat experienced with mygui is even more important as He can do not only tasks on his own but also support other developers.

Developers! Developers! Developers!

lgro continues his work on configuration handling. Task is not that small as It seems but It&#8217;s only one left for release and It doesn&#8217;t interact with other tasks like water or terrain rendering.

Terrain task is going well and smoothly, and that&#8217;s not surprising since Yacoby knows very well what to do. Once again experience proves to be useful.

jHooks is doing fine with water rendering as well. In interior cells it works mostly well, while work on rendering water in exterior cells had only began.

One last thing: lgro (our beloved admin) upgraded wiki and forum recently. Forum members recevied PM but I will quote It for those who are not forum members yet:

> Hi,  
> forum and wiki upgrade is done. If you encounter any strange bugs or something, please let me know (send me e-mail or pm, or just write a post in &#8220;Discuss the site&#8221; subforum), and I will try to fix it.