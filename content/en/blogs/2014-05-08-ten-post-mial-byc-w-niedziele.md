{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-05-08T08:33:41+00:00",
   "title": "Ten post miał być w niedzielę…",
   "type": "post",
   "url": "/2014/ten-post-mial-byc-w-niedziele/"
}
Gdzie news? Gdzie wpis? Wpis jest tu, choć z opóźnieniem – do czego za chwilę wrócę.

OpenMW jest projektem miesiąca według Linux Games Award – zważywszy na to, że dostaliśmy zdecydowanie więcej głosów niż poprzedni zwycięzcy, możemy bezpiecznie założyć, że konkurs nie jest szczególnie dobrze znany. A szkoda, bo pomysł wydaje się ciekawy: pozwala wszak dowiedzieć się o pozycjach pozbawionych większej promocji. Przeglądając propozycje natrafiłem choćby na grę Tales of Maj&#8217;Eyal. Wciąga. Wciąga jak bagno. I stąd mam opóźnienie.

Ale bez obaw, czytelnicy anglojęzyczni będą mieli ten post nawet później – nekochan nie będzie mogła go przetłumaczyć natychmiast. Wracając jednak do tematu OpenMW…

Gdy tylko udało mi się wyzdrowieć, zabrałem się za OpenCS, w związku z czym od teraz możecie swobodnie przeciągać komórki gry na widget renderowania, wliczając w to także komórki widoczne na mapie eksteriorów. Na mapę można z kolei przeciągać rekordy regionów by zmienić region danej komórki.

Zini tymczasem ciężko pracował nad ładowaniem dokumentów w OpenCS. Szczegóły podam niebawem.

Prace nad samym OpenMW również idą do przodu. Scrawl wprowadził istotne poprawki dla SI (npc stają się teraz agresywni, nawet jeśli twój atak chybił) oraz naprawił dość znaczną liczbę innych problemów,

Nasz plik Cmake został oczyszczony z niepotrzebnych odniesień do od dawna zapomnianych bibliotek (audiare? już nawet nie pamiętam kiedy to porzuciliśmy).

Przestępczość w OpenMW staje się coraz to bardziej zbliżona do tej znanej z Morrowind. Od teraz potwory nie mogą być świadkami (choć myślę, że w przyszłości pojawi się możliwość ustawienia flagi by umożliwić niektórym stworzeniom bycie świadkami – deadry-strażnicy w posiadłości maga to chyba dobry przykład).

I deser: OpenMW 0.30.0 zbliża się wielkimi krokami – i zawiera dość imponującą ilość poprawek. Spodoba się wam!