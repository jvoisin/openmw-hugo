{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-10-21T20:33:41+00:00",
   "tags": [
      "alchemy",
      "animations",
      "books",
      "cell loading",
      "Chris",
      "death",
      "editor",
      "Eli2",
      "GUI",
      "lgromanowski",
      "performance",
      "scrawl",
      "Zini"
   ],
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-25/"
}
Welcome! All of you!

The OpenMW editor, which some of you might not even be aware of,  has been progressing stably for quite some time now. Eli2 is currently focusing on loading and saving projects. Once this groundwork is finished, other features can be edited more quickly.

Scrawl is not working on shaders at the moment (mark this historic date on your calenders). Instead he&#8217;s working with zini on the alchemy implementation. The new code needs to be integrated into the game engine, and also requires its own GUI. He made the original alchemy window, so clearly he&#8217;s fit for the current job. Scrawl is becoming a true MyGUI adept. When the GUI first appeared bugs appeared with it, but these are getting squashed now.

BTW, compile time of OpenMW on Scrawl&#8217;s computer is about 2 minutes.

Zini assigned the &#8220;Death&#8221; task to himself. I wouldn&#8217;t be surprised if WeirdSexy&#8217;s videos get a little more of thriller/horror feel or maybe something ala [Overgrowth][1]. Hopefully WeirdSexy will also be able to show some more animations as Chris is working on this feature. Although getting animations to behave correctly is not trivial, the concept has already taken shape.

lgro, the fearless spambot slayer, holy guardian of&#8230; eh never mind. Lgro is working on books. The task proved to be harder then it first appeared to be. The only current benefit from Lgros effort is realizing there are problems that need to be solved in the parser. Lgro promises to do the trick, but it will take quite some time.

Another careful operation in the works is on the performance of cell loading. For some users loading is very slow. To fix this, Zini introduced changes, and the testing brought some surprising results. Unfortunately surprising in the negative sense. Honestly, nobody knows what to do! But the team will figure this out, eventually.

&nbsp;

 [1]: http://blog.wolfire.com/